@extends('Client.layouts.app')

@section('css')
<link rel="stylesheet" href="{{ asset('public/src/css/acc.css') }}">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
@endsection
@section('content')

<div class="l-znurzd">
        <div class="l-7uxhm3">
            <div class="l-1m2qlr5--3">
                <nav class="l-1kiw45x--3">
                    <ul class="l-1aslb2z--3--3--3">
                        <li data-selected="false"> <a style="padding:0px;color:black" href="{{url('/account')}}">Account</a> </li>
                        <li data-selected="false"><a style="padding:0px;color:black" href="{{url('/purchase_history')}}">Purchase History</a></li>
                        <li data-selected="true"><a style="padding:0px;color:black" href="{{url('/affiliate')}}">Affiliation</a></li>
                        <li data-selected="false"><a style="padding:0px;color:black" href="{{url('/withdrawRequests')}}">Withdraw Requests </a></li>                       
                        <li>
                            <form action="{{url('/logout')}}" method="POST">
                            @csrf
                           <input style = "background:transparent;border:none;padding:0px;font-size:16px" type="submit" value="Sign Out">
                        </form> 
                            
                         </li>
                    </ul>
                </nav>
                <main class="l-1vsbrpk--3">
                    @if($data['check_link'])
                    <section class="affiliation">
                    @if(\Session::has('success'))
                       <h3 style="background:green;color: white;padding:10px;text-align: center;">{{\Session::get('success')}}</h3>

                    @endif

                    @if(\Session::has('error'))
                       <h3 style="background:red;color: white;padding:10px;text-align: center;">{{\Session::get('error')}}</h3>

                    @endif

                    <div class="container">
                            <div class="link-section">
                                <h4>Affiliation Link</h4>
                                <input type="text" class="form-control mt-3" value="{{url('/registerAff')}}/{{$data['check_link']}}" readonly name="" id="">
                            </div>

                            <div class="stats-section mt-5">
                                <h4>Statistics</h4>
                                <table class="table table-striped table-bordered mt-3">
                                    <tr>
                                        <th>Total Users Referred</th>
                                        <td>{{$data['users_referred']}}</td>
                                    </tr>
                                    <tr>
                                        <th>Total Earned</th>
                                        <td ><span id = "total_earned">{{$data['total_earned']}}</span> = <span id = "total_earned_usd"></span></td>
                                    </tr>
                                    <tr>
                                        <th>Current Amount</th>
                                        <td><span id = "total_commission">{{$data['current_amount']}}</span> = <span id = "total_commission_usd"></span></td>
                                    </tr>
                                </table>
                            </div>
                            
                            <section class="amount_transfer">
                                <div class="wrap-buttons text-center">
                                    <a href="{{ url('/transferCommission') }}" class="btn btn-primary">Transfer Amount In Balance</a>
                                    <button type="button" class="btn btn-warning text-white" onclick="toggleBtcAddressForm()">Withdraw Amount</button>
                                    <button type="button" class="btn btn-info text-white" onclick="toggleBtcAddressForm2()">Set Auto Transfer</button>
                                </div>
                                <p for="" class="bg-danger text-center text-white mt-2" id = "min_amount">Minimum Commission To Proceed Request Is 0.0020 BTC = <span id = "usd_btc">20$</span></p>
                                @if($data['mode'] != 0)
                                <p for="" class="bg-success text-center text-white mt-2" id = "min_auto_amount"> Auto Trnasfer Option Is Set <span id = ""></span></p>
                                @endif
                                <form class="mt-4 text-center p-5 d-none" id = "btcAddressForm" action="{{ url('/withdrawCommission') }}" method="post" style="border: 1px solid black;">
                                    <h2>Withdraw Request</h2>
                                    @csrf
                                    <div class="form-group">
                                    <input class="form-control" type="text" name="btc_address" id="" placeholder="Enter Your BTC Address" required>
                                    </div>
                                    <div class="form-group">
                                    <input class="form-control" type="text" name="amount" id="" value="{{$data['current_amount']}}" readonly>
                                    </div>

                                    <input class="btn btn-success" type="submit" value="Send Request">
                                </form>
                                <form class="mt-4 text-center p-5 d-none" id = "autoSetCommission" action="{{ url('/autoSetCommission') }}" method="post" style="border: 1px solid black;">
                                    <h2>Set Amount(in BTC) To Auto Transfer</h2>
                                    @csrf
                                    <div class="form-group">
                                    <input step="any" class="form-control" type="number" name="auto_commission_amount" id="" placeholder="Enter Amount To Withdraw Or Transfer Amount Automatically" required>
                                    </div>
                                    <div class="form-group">
                                        <select name="auto_request_mode" id="" class="form-control" onchange="checkMode(this)" required>
                                            <option value="">Select Mode</option>
                                            <option value=1>Transfer</option>
                                            <option value=2>Withdraw Request</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                    <input step="" class="form-control d-none" id = "btc_address_field" type="text" name="btc_address" id="" placeholder="Enter BTC Address">
                                    </div>

                                    <input class="btn btn-success" type="submit" value="Set">
                                </form>

                            </section>

                            <div class="stats-section mt-5">
                                <h4>Total Earnings</h4>
                                <table class="table table-striped table-bordered mt-3">
                                    <thead>
                                        <th>Date</th>
                                        <th>Order Value</th>
                                        <th>Percentage</th>
                                        <th>Your Profit</th>
                                    </thead>
                                    <tbody>
                                    @foreach($data['affiliation_data'] as $data)
                                        <tr>
                                            <td>{{$data->created_at}}</td>
                                            <td name = "order_value">{{$data->order_value}}</td>
                                            <td>10%</td>
                                            <td name = "order_commission">{{$data->commission}}</td>
                                        </tr>
                                    @endforeach    
                                    </tbody>
                                </table>
                            </div>


                        </div>
                    </section>
                    @else
                    <section class="not_started_affliate">
                        <div class="container text-center">
                            <p>You Did'nt Start Affiliation Yet </p>
                            <form action="{{ url('/generateLink') }}" method="POST">
                                @csrf 
                                <input type="submit" class = "btn btn-lg btn-success" value="Start Affiliation">
                            </form>
                            <p class="mt-2">You Will Get Upto 20% Of First Order Of New User Reached By Your Link </p>

                        </div>
                    </section>
                    @endif
                </main>
            </div>
        </div>
    </div>


@endsection

@section('js')
<script src="{{asset('public/js/affiliation.js')}}"></script>
@endsection