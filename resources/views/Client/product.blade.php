@extends('Client.layouts.app')

@section('css')
<link rel="stylesheet" href="{{ asset('public/src/css/buy-a.css') }}">
@endsection
@section('content')




<div class="l-lqxlj7">
  <section class="l-17wxq0i-2">
    <div class="l-1wt8vbo">
      <div style="color:#1D232B" class="l-layjw8-2-2">
        <div style="background-color:#449bf7;color:#ffffff" class="l-d6m5lc">
          <div data-fill="true" class="l-cokf6j-2-2">
            <img id = "main_image" src="{{ asset('public/product_images/'.$data['product']->image) }}" loading="lazy" class="l-xieeun-2" width="350" height="212" alt="{{$data['product']->name}}" layout="responsive" decoding="async" style="background:#449bf7">
          </div>
        </div>
      </div>
    </div>
    <div class="l-wenihk-2">
      <div class="l-liirey">
      </div>
      <div class="l-liirey">
        <a href="">
          {{$data['cat_name']}}
        </a>
        <svg width="6" height="8" viewBox="0 0 6 8">
          <polyline fill="none" stroke="#939393" stroke-linecap="round" stroke-linejoin="round" points="716 118 720 122 724 118" transform="rotate(-90 303.5 420.5)"></polyline>
        </svg>
      </div>
      <div class="l-hdpqtb"><a href="">{{$data['product']->name}}</a>
      </div>
      <div class="l-b27mxe">
        <h1>{{$data['product']->name}}</h1>
      </div>
      <!-- <button class="l-1k51syl-2"><div class="l-1wl7hen">
                  <div class="l-cedl3k-2"><span class="l-v3ntxh-2"></span><span class="l-156c610"></span></div>
                  <p>Rating:  reviews</p></div>
                </button>
                   -->
      <div class="l-vo3cl1"></div>
     
      <form id="form" onsubmit="event.preventDefault();">
        
          <div style="display:flex;flex-direction:row;justify-content:space-between;margin-bottom:6px;height:44px">
          <p class="l-tih06m-2 eqn5xv80">Select amount</p>
          <div style="display: flex; flex-direction: row; align-items: center;">
            Currency:
            <div class="l-1fjca0k-2">
              <p> BTC</p>
            </div>
          </div>
        </div>
        <!-- princing area -->

        <div class="l-1qidl6o-2">

          @foreach($data['product']->price as $index=>$price)
          <label class="l-1b4gzgt">
            <input type="radio"  name="value"  delivery_status ={{$data['product']->delivery_statuss[$index]}}  value="{{$price}}" class="l-g5cvxc price">
            <div class="radio__fake"></div><span class="l-10zozy1">{{$data['product']->currency}} {{$price}}<br>
              <div >       </span>
             {{$data['product']->comment[$index]}}
              </div>
</span> 

          </label>

          @endforeach

        </div>
        <!-- princing area -->

        <!-- <div class="l-w5evj6"> -->
          <!-- <label>Amount to buy<label class="l-77mxvo">
                               <input type="number" name="ranged_value" step="1" min="1" max="2000" value="2000" required="" class="l-132hm4i-2 e1yfu4cn0"></label></label><span class="l-99kpxf-2 eqn5xv88"><strong>Min:</strong> 1 USD</span><span class="l-99kpxf-2 eqn5xv88"><strong>Max:</strong> 2000 USD</span><span class="l-j4uldl-2 eqn5xv88"><strong>Price:</strong> 0.21302800 BTC</span></div> -->
          <div class="l-1a2lyks">
            <!-- add to cart button -->
            <div class="l-ns4038">
              <input type="hidden" class="main_title" value="{{$data['product']->name}}">
              <button type="button"  onclick="dicrease()"><span>-</span></button>
              <button type="submit" currency = "{{$data['product']->currency}}" value = "{{$data['product']->id}}" onclick="addToCart(this)">
                <svg stroke-width="2.5" class="l-no8bmp-2" fill="none" height="24" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" viewBox="0 0 24 24" width="24">
                  <circle cx="9" cy="21" r="1"></circle>
                  <circle cx="20" cy="21" r="1"></circle>
                  <path d="M1 1h4l2.68 13.39a2 2 0 0 0 2 1.61h9.72a2 2 0 0 0 2-1.61L23 6H6"></path>
                </svg>Add 
                <span id = "counter_to_show"> 1 </span>
                  to cart
              </button>
              <button type="button" onclick="increase()"><span>+</span></button>
            </div>
            <!-- add to cart button -->

            <!-- add to cart button -->
            <div class="l-ns4038">
              <input type="hidden" class="main_title" value="{{$data['product']->name}}">

              <!-- this is to check if it has redeem codes or not -->
              <input type="hidden" id = "redeem_status" class="redeem_status" value="{{$data['actual_redeems']}}">
              <!-- this is to check if it has redeem codes or not -->

              <button style="background: orange;" type="button" onclick="dicrease1()"><span>-</span></button>
              <button style="background: #f9b02b;" type="submit" currency = "{{$data['product']->currency}}" value = "{{$data['product']->id}}" onclick="instantBuy(this)">
                <svg stroke-width="2.5" class="l-no8bmp-2" fill="none" height="24" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" viewBox="0 0 24 24" width="24">
                  <circle cx="9" cy="21" r="1"></circle>
                  <circle cx="20" cy="21" r="1"></circle>
                  <path d="M1 1h4l2.68 13.39a2 2 0 0 0 2 1.61h9.72a2 2 0 0 0 2-1.61L23 6H6"></path>
                </svg>Buy  
                <span id = "counter_to_show1"> 1 </span>
                  Now
              </button>
              <button type="button" style="background: orange;" onclick="increase1()"><span>+</span></button>
            </div>
            <!-- add to cart button -->




          </div>
      </form>
      <ul id="product-tabs" class="l-1jhjtzn">
    @if($data['redeem_length'] > 0)
      <li class="l-k4g1no"><svg width="19" height="20" viewBox="0 0 19 20">
            <g fill="none" fill-rule="evenodd" stroke="#13CE66" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" transform="translate(1 1)">
              <path d="M17,8.568 L17,9.35 C16.9978476,13.114383 14.5199499,16.428981 10.9100496,17.4963158 C7.30014923,18.5636506 3.41818451,17.1294653 1.36931182,13.9715064 C-0.679560869,10.8135475 -0.407266071,6.68409313 2.03853307,3.82251132 C4.4843322,0.960929515 8.52100575,0.0488964916 11.9595,1.581"></path>
              <polyline points="17 2.55 8.5 11.059 5.95 8.508"></polyline>
            </g>
          </svg>
          <p>Instant delivery</p>
        </li>
     @endif
        <li class="l-k4g1no"><svg width="19" height="20" viewBox="0 0 19 20">
            <g fill="none" fill-rule="evenodd" stroke="#13CE66" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" transform="translate(1 1)">
              <path d="M17,8.568 L17,9.35 C16.9978476,13.114383 14.5199499,16.428981 10.9100496,17.4963158 C7.30014923,18.5636506 3.41818451,17.1294653 1.36931182,13.9715064 C-0.679560869,10.8135475 -0.407266071,6.68409313 2.03853307,3.82251132 C4.4843322,0.960929515 8.52100575,0.0488964916 11.9595,1.581"></path>
              <polyline points="17 2.55 8.5 11.059 5.95 8.508"></polyline>
            </g>
          </svg>
          <p>Fast, Private, Safe</p>
        </li>
      </ul>
      <div class="l-1rwcu4i">
        <div role="tablist">




          <div id="producttabs-tab0" role="tab" aria-controls="producttabs-tabpanel0" option="Description" data-selected="true" onclick="change_attribute('producttabs-tab0')"> Product Description </div>
          <div id="producttabs-tabpanel0" role="tabpanel" aria-labelledby="producttabs-tab0">
            <div class="l-47cdza">
              <div class="l-1lrewpk-2">
                   <pre style = "background:transparent!important;width:100%;font-size: 12px;letter-spacing: 1px;white-space: pre-wrap;white-space: -moz-pre-wrap;white-space: -pre-wrap;white-space: -o-pre-wrap;word-wrap: break-word; hyphens: auto;font-weight: 700;font-size:12pxmargin-top: 0;padding: 8px;">{{$data['product']->description}}</pre>
                  <!-- <h2 class="l-civ7qo-2 eqn5xv82"> {{$data['product']->description}}</h2> -->

              </div>
            </div>
          </div>

        </div>
      </div>
    </div>
  </section>
</div>
<section class="l-dg8599">



  <div class="l-lqxlj7">
    <h2 class="l-12tsu90">Similar Products There</h2>
    <div class="l-tikki8-2">

   @foreach($data['products'] as $product)
      <!-- product   -->
       @if (    ($loop->iteration) <= 5)

      <a style="color:#1D232B" class="l-1mtjfgl-2" href="{{ url('/product/'.$product->id) }}">
        <div style="background-color:#ffffff;color:black" class="l-d6m5lc">
          <div data-fill="false" class="l-cokf6j-2-2">
            <img src="{{ asset('public/product_images/'.$product->image) }}" loading="lazy" class="l-xieeun-2" width="350" height="212" layout="responsive" decoding="async" style="background:#ffffff">
          </div>
        </div>
        <p class="l-166cjrn-2">{{ $product->name }}</p>
      </a>
      @endif
      <!-- product   -->
      @endforeach

    </div>
    <a class="l-8brkfy" href="{{ url('/home/category/'.$data['product']->cat_id) }}">See all</a>
  </div>


</section>




</div>
</div>

<!-- <script src="./Buy Bitrefill Balance Card (USD) with Bitcoin - Bitrefill_files/en.1660f44e.js.download" nonce=""></script> -->











<script>
window.onload = function(){
  let product_btc_rates = document.querySelectorAll('.product_btc_rates');
  let currency  = product_btc_rates[0].getAttribute('currency');
  let btc_rate = 0;
  let btc_api  = JSON.parse(localStorage.getItem('egify_BTC_rate'));
   
  btc_api.forEach(x=>{
    if(x.code === currency ){
      btc_exc_rate = x.rate_float;
      btc_exc_rate = parseFloat(1/btc_exc_rate);
    }
  });
  // alert(btc_rate);
  product_btc_rates.forEach(product_btc_rate=>{
    let price = product_btc_rate.getAttribute('rate');
    let btc_rate = price*btc_exc_rate;
    btc_rate = btc_rate.toFixed(10)
    product_btc_rate.innerHTML = btc_rate;   
  });
}
</script>


@endsection

@section('js')
<script src="{{asset('public/js/custom.js')}}"></script>
@endsection