@extends('Client.layouts.app')

@section('css')
<link rel="stylesheet" href="{{ asset('public/src/css/acc.css') }}">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<style>
.blur_back{
    width:100%!important;
    height:100vh!important;
    background:rgb(0,0,0,0.8);
    position:absolute!important;
    display:flex;
    justify-content:center;
    align-items:center;
    display:none
    
    
}

.blur_back_active{
    width:100%!important;
    height:100vh!important;
    position:absolute!important;
    z-index:999999999!important;
    background:rgb(0,0,0,0.8);
    display:flex;
    justify-content:center;
    align-items:center;

    
}

.popupBox{
    padding:40px 100px;
    background:white!important;
    text-align:center;
}

</style>

@endsection
@section('content')

<div class = "blur_back" id = "blur_back">
    <div class = "popupBox">
       <!-- <img style = "width:150px" src = "{{asset('public/src/site_images/gs.webp ')}}"> --> 
        <h4 style = "color:red">Updating Your transactions <br>Please Wait .... <br> page will refresh automatically in <span id = "refresh_countdown"></span></h4>
        <!-- <h4>ThankYou For Shopping</h4> -->
    </div>
</div>
<div class="l-znurzd">
        <div class="l-7uxhm3">
            <div class="l-1m2qlr5--3">
                <nav class="l-1kiw45x--3">
                    <ul class="l-1aslb2z--3--3--3">
                        <li data-selected="true"> <a style="padding:0px;color:black" href="{{url('/account')}}">Account</a> </li>
                        <li data-selected="false"><a style="padding:0px;color:black" href="{{url('/purchase_history')}}">Purchase History</a></li>
                        <li data-selected="false"><a style="padding:0px;color:black" href="{{url('/affiliate')}}">Affiliation</a></li>
                        <li data-selected="false"><a style="padding:0px;color:black" href="{{url('/withdrawRequests')}}">Withdraw Requests </a></li> 
                        <li>
                            <form action="{{url('/logout')}}" method="POST">
                            @csrf
                           <input style = "background:transparent;border:none;padding:0px;font-size:16px" type="submit" value="Sign Out">
                        </form> 
                            
                         </li>
                    </ul>
                </nav>
                <main class="l-1vsbrpk--3">
                    <div>
                        <div class="l-1nbtjcd--3">
                            <div class="l-961w8f">
                                <div class="l-klmojb">
                                    <h1 class="l-jojkln--3 eqn5xv81">Balances</h1><svg width="22" height="22"
                                        viewBox="0 0 56 56" fill="none" style="margin-left: 8px;">
                                        <path
                                            d="M31.0472 9.4136C29.5542 9.96998 28.6375 11.5269 28.6375 13.5129C28.6375 14.9537 28.9675 15.8909 29.7192 16.5469C30.5423 17.2657 31.4062 17.3488 32.4748 16.8241C33.5255 16.3001 34.1327 15.479 34.4911 14.1016C35.2448 11.1692 33.3427 8.59454 31.0472 9.4136Z"
                                            fill="#449BF7"></path>
                                        <path
                                            d="M28.491 19.35C26.6707 19.7262 24.6709 21.2667 22.8664 23.6948C21.5887 25.4003 21.0323 26.5949 21.2772 27.0562C21.3755 27.2371 21.555 27.3836 21.689 27.3836C21.8045 27.3836 22.4751 26.5309 23.1799 25.4828C23.8842 24.4485 24.6709 23.417 24.9336 23.186C25.917 22.3517 26.6892 22.9081 26.3268 24.2202C26.2463 24.5654 25.3593 27.4991 24.3917 30.745C22.2929 37.7166 21.8659 39.5481 21.8514 41.5651C21.8514 42.827 21.8995 43.122 22.2269 43.6309C22.9833 44.8585 24.6386 45.009 26.6212 44.0447C29.3272 42.6976 33.21 38.0591 33.0457 36.3543C33.016 36.0243 32.9176 35.829 32.7711 35.829C32.5223 35.829 32.2953 36.1088 30.5898 38.471C29.1134 40.5018 28.1656 40.8351 28.1656 39.3079C28.1656 38.9468 29.017 35.6343 30.0479 31.9277C31.0802 28.2244 32.0167 24.735 32.1157 24.1443C32.3652 22.7847 32.2332 21.1604 31.8194 20.472C31.2459 19.4814 29.9826 19.0543 28.491 19.35Z"
                                            fill="#449BF7"></path>
                                        <path
                                            d="M27.9993 55.3413C12.9235 55.3413 0.660004 43.0745 0.660004 27.9987C0.660004 12.9235 12.9235 0.660004 27.9993 0.660004C43.0732 0.660004 55.34 12.9235 55.34 27.9987C55.34 43.0745 43.0732 55.3413 27.9993 55.3413ZM27.9993 2.62681C14.0099 2.62681 2.62681 14.0099 2.62681 27.9987C2.62681 41.9875 14.0079 53.3719 27.9993 53.3719C41.9894 53.3719 53.3705 41.9908 53.3705 27.9987C53.3705 14.0079 41.9894 2.62681 27.9993 2.62681Z"
                                            fill="#449BF7"></path>
                                        <path
                                            d="M27.9993 56.0013C12.5605 56.0013 0 43.4388 0 27.9987C0 12.5599 12.5605 0 27.9993 0C43.4395 0 56 12.5599 56 27.9987C56 43.4388 43.4388 56.0013 27.9993 56.0013ZM27.9993 1.32001C13.2885 1.32001 1.32001 13.2879 1.32001 27.9987C1.32001 42.7115 13.2885 54.6813 27.9993 54.6813C42.7108 54.6813 54.68 42.7115 54.68 27.9987C54.68 13.2879 42.7108 1.32001 27.9993 1.32001ZM27.9993 54.0319C13.6449 54.0319 1.96681 42.3538 1.96681 27.9987C1.96681 13.6443 13.6449 1.96681 27.9993 1.96681C42.3531 1.96681 54.0305 13.6443 54.0305 27.9987C54.0305 42.3538 42.3531 54.0319 27.9993 54.0319ZM27.9993 3.28682C14.3729 3.28682 3.28682 14.3722 3.28682 27.9987C3.28682 41.6251 14.3729 52.7119 27.9993 52.7119C41.6258 52.7119 52.7105 41.6251 52.7105 27.9987C52.7105 14.3722 41.6251 3.28682 27.9993 3.28682Z"
                                            fill="#449BF7"></path>
                                    </svg>
                                </div><span>
                                    <p class="l-11xabtj--3 eqn5xv80">What are Balances ?</p>
                                    <p class="l-10g8w73--3 eqn5xv80">Each card denotes your current balance in that
                                        currency. You may withdraw from your <i>Bitcoin Balance</i>, while others are
                                        only spendable balances. <br> <br> The Primary balance is used by default to
                                        make purchases faster than before. Pending balances will be displayed
                                        explicitly.</p>
                                </span>
                            </div>
                            <a class="l-18qdhk2--3 e8fmg4a0" href="{{ url('/addFund') }}">Add Funds</a>
                            <button class = "btn btn-success ml-3" id = "pending_trans_button2">Update Balance</button>
                        
                        </div>
                        <div class="l-kc5mhc--3">
                            <div class="l-1wr7qrj">
                                
                                <div disabled="" class="l-ps739y--3" ><svg width="32" height="32"
                                        class="l-ubm37l--3" viewBox="0 0 60 60" fill="none">
                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                            d="M59.103 37.258c-4.01 16.038-20.324 25.855-36.361 21.845C6.704 55.093-3.113 38.848.897 22.742 4.907 6.704 21.152-3.113 37.258.897c16.038 4.01 25.855 20.324 21.845 36.361z"
                                            fill="#F7931A"></path>
                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                            d="M43.207 25.71c.623-4.01-2.42-6.153-6.567-7.536l1.314-5.46-3.25-.761-1.313 5.253a153.44 153.44 0 01-2.627-.622l1.314-5.323-3.319-.83-1.313 5.393c-.76-.138-1.452-.277-2.143-.484l-4.493-1.106-.899 3.525 2.42.554c1.313.345 1.52 1.244 1.52 1.935l-1.59 6.153c.139 0 .277.069.346.138-.07-.07-.207-.07-.346-.138l-2.143 8.64c-.138.416-.553 1.038-1.52.761.069.07-2.35-.553-2.35-.553l-1.66 3.733 4.286 1.106c.83.208 1.59.415 2.35.622l-1.382 5.462 3.318.76 1.314-5.392c.898.276 1.797.484 2.626.691l-1.313 5.392 3.318.83 1.314-5.461c5.599 1.037 9.885.622 11.613-4.425 1.452-4.147-.069-6.498-3.041-8.019 2.212-.483 3.802-1.935 4.216-4.839zm-7.535 10.576c-.967 4.079-7.88 1.867-10.092 1.313l1.797-7.258c2.212.553 9.401 1.66 8.295 5.945zM36.71 25.64c-.898 3.733-6.636 1.867-8.503 1.383l1.66-6.567c1.866.483 7.811 1.313 6.843 5.184z"
                                            fill="#fff"></path>
                                    </svg>
                                    <p class="l-19hjj3d--3 eqn5xv80">Bitcoin</p><strong class="l-g8eivf--3--3"><span
                                            class="amount l-gc66f9">{{$data['balance']}}</span><span
                                            class="suffix l-1nlr9kf">BTC</span></strong>
                                    <div class="hover-display l-3zuepu--3">
                                        <p class="l-10g8w73--3 eqn5xv80"></p>
                                    </div>
                                    <div class="l-fj1mq8"></div>
                                </div>
                                
                            </div>
                        </div>
                        <h2 class="l-ezexrc--3--3 eqn5xv82">History</h2>
                        @if(sizeof($data['transactions']) > 0)
                        <table style="width: 100%;">
                        <thead>
                            <tr style="text-align: left;"> 
                          <th >Address</th>
                          <th>Amount</th>
                          <th>Status</th>
                          </tr>
                        </thead>

                        <tbody>
                        @foreach($data['transactions'] as $transaction)
                        <tr>
                          <td>{{$transaction->btc_address}}</td>
                          <td>{{$transaction->amount}}</td>
                          
                            @if($transaction->status == 1)
                            <td style="color:orangered">Pending</td>
                            @else
                            <td style="color:green">Confirmed</td>

                            @endif
                          
                        </tr>

                        @endforeach
                        </tbody>
                      <h5 id = "transaction_message" style="background:red;color:white;padding:10px">Please Wait A Little If You have any Transaction Pending Or Not Showing In Transaction 
                      <br>
                      To Check And Update transaction Take A Little Bit time You Just Need To Wait A Little On this Page</h5>
                        
                        </table>
                        <br>
                        <button class = "btn btn-block btn-success" id = "pending_trans_button">Click here If Payment is still Pending or not showing</button>

                        @endif



                        <p class="l-10g8w73--3 eqn5xv80"> 
                            <a class="l-a0vwlq" href="{{ url('/addFund') }}">Add funds</a> to order instantly next time</p>
                    </div>
                </main>
            </div>
        </div>
    </div>


@endsection

@section('js')
<script src="{{asset('public/js/pendingPayment.js')}}"></script>
@endsection