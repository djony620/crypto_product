@extends('Client.layouts.app')

@section('css')
<link rel="stylesheet" href="{{ asset('public/src/css/acc.css') }}">
<style>
    .l-x10c0s--3 {
    display: flex;
    -webkit-box-align: center;
    align-items: center;
    -webkit-box-pack: center;
    justify-content: center;
    min-height: 40px;
    font-size: 14px;
    color: rgb(255, 255, 255);
    text-align: center;
    text-rendering: optimizelegibility;
    -webkit-font-smoothing: antialiased;
    font-weight: 600;
    line-height: 24px;
    cursor: pointer;
    background-color: rgb(69, 155, 247);
    border-radius: 4px;
    margin: 8px 0px;
    padding: 6px 14px;
    text-decoration: none;
    border-width: 2px;
    border-style: solid;
    border-color: transparent;
    border-image: initial;
    transition: background-color 0.1s ease 0s, color 0.1s ease 0s;
    }
    </style>
@endsection
@section('content')


<div class="l-znurzd">
        <div class="l-7uxhm3">
            <div class="l-1m2qlr5--3">
            <nav class="l-1kiw45x--3">
                    <ul class="l-1aslb2z--3--3--3">
                        <li data-selected="false"> <a style="padding:0px;color:black" href="{{url('/account')}}">Account</a> </li>
                        <li data-selected="true"><a style="padding:0px;color:black" href="{{url('/myproducts')}}">My Products</a></li>
                        <li data-selected="false"><a style="padding:0px;color:black" href="{{url('/purchase_history')}}">Purchase History</a></li>
                        <li>
                            <form action="{{url('/logout')}}" method="POST">
                            @csrf
                           <input style = "background:transparent;border:none;padding:0px;font-size:16px" type="submit" value="Sign Out">
                        </form> 
                            
                         </li>
                    </ul>
                </nav>
               <main class="l-1vsbrpk--3">
                    <div class="l-1ddlsfu--3">
                        <h1 class="l-13ii16a--3--3 eqn5xv81">My Products</h1>
                        <div>
                            <div class="l-8ztxda--3">
                            </div>
                        </div>
                    </div>
                    <p class="l-10g8w73--3 eqn5xv80"> <a class="l-a0vwlq"
                            href="{{ url('/addFund') }}">Add funds</a> to order instantly next time</p>
                            @foreach($orders as $index=>$order)
                               
                            <table style="width: 100%;box-shadow:10px 10px 10px silver;margin-top:50px" id = "order_table">
                            @if($index == 0)
                            <tr>
                                <td>
                                
                                <h2>Most Recent</h2>
                                
                                </td>
                            </tr>
                            @endif   
                            @foreach($order->cart as $product)
                                        <tr>
                                            <td style = "width:40%">
                                            <img style = "width:95%" src="{{$product['image']}}" alt="">
                                            </td>
                                            <td>
                                                 <h2>{{$product['name']}}</h2>                                                      
                                                 <h5>{{$product['rate']}} {{$product['currency']}}</h5>
                                                 <a href = "{{$product['redeem_link']}}" class="l-x10c0s--3 e8fmg4a1" >Redeem</a>
                                                 @foreach($product['redeem_code'] as $index=>$code)
                                                 <input type="text" readonly value="{{$code}}" style="width: 100%;height:30px;background:#f0f0f0;border:none;text-align:center">   
                                                @endforeach
                                                </td>
                                        </tr>
                                        @endforeach
                                </table>
                            @endforeach
                        </main>
            </div>
        </div>
    </div>


@endsection

@section('js')
<!-- <script src="{{asset('js/custom.js')}}"></script> -->
@endsection