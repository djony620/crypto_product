@extends('Client.layouts.app')

@section('css')
<link rel="stylesheet" href="{{ asset('public/src/css/acc.css') }}">
<style>
    .l-x10c0s--3 {
        display: flex;
        -webkit-box-align: center;
        align-items: center;
        -webkit-box-pack: center;
        justify-content: center;
        min-height: 40px;
        font-size: 14px;
        color: rgb(255, 255, 255);
        text-align: center;
        text-rendering: optimizelegibility;
        -webkit-font-smoothing: antialiased;
        font-weight: 600;
        line-height: 24px;
        cursor: pointer;
        background-color: rgb(69, 155, 247);
        border-radius: 4px;
        margin: 8px 0px;
        padding: 6px 14px;
        text-decoration: none;
        border-width: 2px;
        border-style: solid;
        border-color: transparent;
        border-image: initial;
        transition: background-color 0.1s ease 0s, color 0.1s ease 0s;
    }
</style>
@endsection
@section('content')


<div class="l-znurzd">
    <div class="l-7uxhm3">
        <div class="l-1m2qlr5--3">
            <nav class="l-1kiw45x--3">
                <ul class="l-1aslb2z--3--3--3">
                    <li data-selected="false"> <a style="padding:0px;color:black" href="{{url('/account')}}">Account</a> </li>
                    <li data-selected="false"><a style="padding:0px;color:black" href="{{url('/purchase_history')}}">Purchase History</a></li>
                    <li>
                        <form action="{{url('/logout')}}" method="POST">
                            @csrf
                            <input style="background:transparent;border:none;padding:0px;font-size:16px" type="submit" value="Sign Out">
                        </form>

                    </li>
                </ul>
            </nav>
            <main class="l-1vsbrpk--3">
                <div class="l-1ddlsfu--3">
                    <h1 class="l-13ii16a--3--3 eqn5xv81">Support &nbsp; <img src="{{ asset('public/src/site_images/mail.png') }}" alt=""></h1>
                    <div>
                        <div class="l-8ztxda--3">
                        </div>
                    </div>
                </div>
                <p class="l-10g8w73--3 eqn5xv80"> <a class="l-a0vwlq" href="{{ url('/addFund') }}">Add funds</a> to order instantly next time</p>

                <div class="support">
                    <div class="create_message">
                        <form action="{{ url('/sendMessage') }}" method="POST">
                            @csrf
                            <textarea name="message" style="width: 100%;" id="" rows="10" placeholder="Write Message To Support"></textarea>
                            <input style="padding: 10px 20px;border:none" type="submit" value="Submit Message">
                        </form>
                    </div>
                    <div style="margin-top: 60px;border:1px solid silver;padding: 10px;" class="messages">
                    @if($messages)
                    @foreach($messages as $message)
                <!-- message wrap -->
                        <div style="border:1px solid silver;padding:5px;margin-bottom:50px" class="message-wrap">
                            <div class="message">
                                <h4>You Date:{{ $message->created_at }}
                                @if($message->status == 0)
                                 <span style="color: red;">( Unseen )</span>
                                 @else
                                 <span style="color: green;">( Seen )</span>
                                 @endif
                                 </h4>
                                {{$message->message}}
                            </div>
                    <hr>
                            <div class="reply">
                                @if($message->reply == NULL)
                                <h4>Support Team Will Response Within 12-24 hours</h4>    
                                @else
                                <h4>Admin (Reply) Date:8-6-2020</h4>
                                {{$message->reply}}
                                @endif
                            </div>
                        </div>

                <!-- message wrap -->
                    @endforeach
                    @else
                <!-- message wrap -->
                <div style="border:1px solid silver;padding:5px;margin-bottom:50px;text-align:center" class="message-wrap">
                            No Messages Found
                        </div>

                <!-- message wrap -->

                   @endif 


                    </div>

                </div>



            </main>
        </div>
    </div>
</div>


@endsection

@section('js')
<!-- <script src="{{asset('js/custom.js')}}"></script> -->
@endsection