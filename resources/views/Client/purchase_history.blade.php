@extends('Client.layouts.app')

@section('css')
<link rel="stylesheet" href="{{ asset('public/src/css/acc.css') }}">
<style>
    .l-x10c0s--3 {
        display: flex;
        -webkit-box-align: center;
        align-items: center;
        -webkit-box-pack: center;
        justify-content: center;
        min-height: 40px;
        font-size: 14px;
        color: rgb(255, 255, 255);
        text-align: center;
        text-rendering: optimizelegibility;
        -webkit-font-smoothing: antialiased;
        font-weight: 600;
        line-height: 24px;
        cursor: pointer;
        background-color: rgb(69, 155, 247);
        border-radius: 4px;
        margin: 8px 0px;
        padding: 6px 14px;
        text-decoration: none;
        border-width: 2px;
        border-style: solid;
        border-color: transparent;
        border-image: initial;
        transition: background-color 0.1s ease 0s, color 0.1s ease 0s;
    }

    .wrap {
        width: 100%;
    }

    .wrap-cont {
        display: flex;
        justify-content: space-between;
    }

    .wrap-cont a {
        width: 40%;
    }

    .table {
        width: 80%;
        display: block;
        box-shadow: 0px 0px 15px silver;
        margin-top: 50px
    }

    .order_details {
        display: none;
    }

    .order_details_active {
        display: block;
    }


    .left {
        width: 40%;
        float: left;
    }

    .right {
        width: 40%;
        float: right;
    }

    .refund_box {
        display: none;
    }

    .refund_box_active {
        display: block;
    }
    
    pre{
width: 100%;
font-size: 12px;
letter-spacing: 1px;
white-space: pre-wrap;
white-space: -moz-pre-wrap;
white-space: -pre-wrap;
white-space: -o-pre-wrap;
word-wrap: break-word;
hyphens: auto;
font-weight: 700;
font-size: 12pxmargin-top: 0;
padding: 8px;
    }
</style>
@endsection
@section('content')


<div class="l-znurzd">
    <div class="l-7uxhm3">
        <div class="l-1m2qlr5--3">
            <nav class="l-1kiw45x--3">
                <ul class="l-1aslb2z--3--3--3">
                    <li data-selected="false"> <a style="padding:0px;color:black" href="{{url('/account')}}">Account</a> </li>
                    <li data-selected="true"><a style="padding:0px;color:black" href="{{url('/purchase_history')}}">Purchase History</a></li>
                        <li data-selected="false"><a style="padding:0px;color:black" href="{{url('/affiliate')}}">Affiliation</a></li>
                        <li data-selected="false"><a style="padding:0px;color:black" href="{{url('/withdrawRequests')}}">Withdraw Requests </a></li> 
                    <li>
                        <form action="{{url('/logout')}}" method="POST">
                            @csrf
                            <input style="background:transparent;border:none;padding:0px;font-size:16px" type="submit" value="Sign Out">
                        </form>

                    </li>
                </ul>
            </nav>
            <main class="l-1vsbrpk--3">
                <div class="l-1ddlsfu--3">
                    <h1 class="l-13ii16a--3--3 eqn5xv81">Purchase History</h1>
                    <div>
                        <div class="l-8ztxda--3">
                        </div>
                    </div>
                </div>
                <p class="l-10g8w73--3 eqn5xv80"> <a class="l-a0vwlq" href="{{ url('/addFund') }}">Add funds</a> to order instantly next time</p>
                @if($errors->any())
                <h3 style="background:red;color: white;padding:10px;text-align: center;">{{$errors->first()}}</h3>

                @endif

                @if(\Session::has('msg'))
                <h3 style="background:green;color: white;padding:10px;text-align: center;">{{\Session::get('msg')}}</h3>

                @endif


                @foreach($orders as $index=>$order)

                <table class="table">
                    @foreach($order->cart as $key=>$product)
                   

                  @if($product['status'] == 0 || $product['status'] == 1)
                  <?php
                    $order->invoice_total += $product['rate'] * $product['btc_rate'] * $product['qty'];
                    $product_total = $product['rate'] * $product['btc_rate'] * $product['qty'];
                    ?>



                    <tr>
                        <td style="width:40%">
                            <img style="width:95%" src="{{$product['image']}}" alt="">
                        </td>
                        <td>
                            <h2>{{$product['name']}}</h2>
                            <h5>Price : {{$product['rate']}} {{$product['currency']}}</h5>
<h5> Quantity : {{$product['qty']}}</h5>
                            @if($product['status'] == 0)
                            <div class="wrap-cont">
                            </div>

                            <h4 style="background: skyblue!important;text-align: center;color: white; padding: 10px;">In Progress</h4>

                            <a href="{{url('/orderCencel')}}/{{$order->invoice}}/{{$product['slug']}}/{{$key}}" class="l-x10c0s--3 e8fmg4a1" style="background: red!important;">Cencel</a>

                            @else
                            <h4 class="delivered" style="background: green!important;text-align: center;color: white; padding: 10px;">Delivered</h4>

                            @if($order['refund_status'][$key] == 0)

                            <button class="l-x10c0s--3 e8fmg4a1" invoice="{{$order->invoice}}" slug="{{$product['slug']}}" index={{$key}} style="background: green!important;width: 100%;" onclick="seeOrder(this)" >See Order</button>

                            <section class="order_details">
                                @foreach($product['redeem_code'] as $index=>$code) 
                                                 @if($code != 'In Progress')
<pre style = "width:100%;background:#f0f0f0;border:none;font-size:15px;padding:10px;letter-spacing: 1px;white-space: pre-wrap;white-space: -moz-pre-wrap;white-space: -pre-wrap;white-space: -o-pre-wrap;word-wrap: break-word; hyphens: auto;font-weight: 700;font-size:12pxmargin-top: 0;padding: 8px;"  name="" id="" readonly>{{$code}}</pre>                                
    @else
<pre style = "width:100%;background:#f0f0f0;border:none;font-size:15px;padding:10px;overflow-x:scroll"  name="" id="" readonly></pre>
                                   @endif
           <!-- <input type="text" readonly value="{{$code}}" style="width: 100%;height:30px;background:#f0f0f0;border:none;text-align:center">-->
                                @endforeach

                            </section>

                             @else

                            <section class="order_details_active">
                                @foreach($product['redeem_code'] as $index=>$code)
                                   @if($code != 'In Progress')
<pre style = "width:100%;background:#f0f0f0;border:none;font-size:15px;padding:10px;overflow-wrap: anywhere;"  name="" id="" readonly>{{$code}}</pre>                                
    @else
<pre style = "width:100%;background:#f0f0f0;border:none;font-size:15px;padding:10px;overflow-wrap: anywhere;"  name="" id="" readonly></pre>
                                   @endif
                                

                               <!-- <input type="text" readonly value="{{$code}}" style="width: 100%;height:30px;background:#f0f0f0;border:none;text-align:center"> -->
                                @endforeach

                            </section>


                             @endif                           

                            @if($order['refund_status'][$key] == 0)
                            <button class="l-x10c0s--3 e8fmg4a1 refund" time={{$order['times'][$key]}} style="background:red!important;width:100%" onclick="seeRefundBox(this)" disabled>Ask Refund</button>
                            @elseif($order['refund_status'][$key] == 1)
                            <button class="l-x10c0s--3 e8fmg4a1 refund countdown_activate" time={{$order['times'][$key]}} style="background:red!important;width:100%" onclick="seeRefundBox(this)">Ask Refund</button>
                            @elseif($order['refund_status'][$key] == 2)
                            <button class="l-x10c0s--3 e8fmg4a1" time={{$order['times'][$key]}} style="background:skyblue!important;width:100%">Request Sent</button>
                            <p style="font-weight:bold;color: skyblue;">Reason To Ask Refund</p> 
                            <p style="padding: 10px;border: 1px solid skyblue;">
                                {{$order['refund_comment'][$key]}}
                            </p>   
                            @elseif($order['refund_status'][$key] == 3)
                            <button class="l-x10c0s--3 e8fmg4a1" time={{$order['times'][$key]}} style="background:green!important;width:100%">Refunded</button>
                            @elseif($order['refund_status'][$key] == 4)
                            <button class="l-x10c0s--3 e8fmg4a1" time={{$order['times'][$key]}} style="background:#ffc107!important;width:100%">Decline</button>
                            <p style="font-weight:bold;color: #ffc107;">Reason To Deny Refund</p> 
                            <p style="padding: 10px;border: 1px solid #ffc107;">
                                {{$order['refund_comment'][$key]}}
                            </p>   
                    
                            @elseif($order['refund_status'][$key] == 5)
                            <button class="l-x10c0s--3 e8fmg4a1" time={{$order['times'][$key]}} style="background:red!important;width:100%">Refunded Time Expired</button>

   
                  
                            @endif
                          
                            <form action="{{url('/refundRequestUpdate')}}" method="post" class="refund_box">
                            @csrf
                                <input type="hidden" name="invoice" value="{{$order->invoice}}">
                                <input type="hidden" name="index" value={{$key}}>

                                <textarea name="comment" style="width:100%" id="" rows="10" placeholder="Please Enter Rreason To Ask Refund"></textarea>
                                <button type="submit" class="l-x10c0s--3 e8fmg4a1" style="background:red!important;width:100%">Submit</button>
                            </form>
                            @if($product['guide'] != null && $product['guide'] != "")
                            <div class="guide">
                                <label for="">Guide</label>
                                <p style="color:green">
                                    {{$product['guide']}}
                                </p>
                            </div>
                            @endif
                            <!-- guide if  -->
                            @endif
                            <!-- status if else -->

                        </td>
                    </tr>
                    @endif
                    @endforeach
<!--                    <tr>
                        <td colspan="2" style="text-align: center;border:1px solid silver;padding:10px">
                            <span style="font-weight: bold;border-bottom:1px solid green;">Order Comment</span> <br><br> {{$order->comment}}
                        </td>
                    </tr> -->
                    <tr>
                        <th>
                            <p>Total : {{$order->invoice_total}} BTC</p>
                        </th>
                    </tr>
                </table>
                @endforeach
            </main>
        </div>
    </div>
</div>


@endsection

@section('js')
<script>
    let seeOrder = (e) => {
        let orderTable = e.parentNode.getElementsByTagName('section')[0];
        let invoice = e.getAttribute('invoice');
        let index = e.getAttribute('index');
        let slug = e.getAttribute('slug');
        // to check countdown is activated or not/
        let countdown_activate = e.parentNode.querySelectorAll('.countdown_activate');
        if (countdown_activate.length > 0) {

        } else {
            let refund = e.parentNode.querySelectorAll('.refund')[0];
            if (refund !== undefined) {
                refund.classList.add('countdown_activate');
                refund.disabled = false;
                //entry
                $.ajax({
                    url: `http://${window.location.hostname}/refundRequest/${invoice}/${slug}/${index}`,
                    type: 'get',
                    success: function(response) {
                        console.log(response);
                    },
                    error: function(error) {
                        console.log(error);

                    }
                });
                //entry

            }//undefined if

        }//else
        // to check countdown is activated or not/

        let attribute = orderTable.getAttribute('class');
        // alert(attribute);
        if (attribute == "order_details") {
            orderTable.setAttribute('class', 'order_details_active');
        } else {
            orderTable.setAttribute('class', 'order_details');
        }

    }


    let seeRefundBox = (e) => {
        let form = e.parentNode.getElementsByTagName('form')[0];
        let attribute = form.getAttribute('class');
        // alert(attribute);
        if (attribute == "refund_box") {
            form.setAttribute('class', 'refund_box_active');
        } else {
            form.setAttribute('class', 'refund_box');
        }

    }



    setInterval(() => {
        let countdowns = document.querySelectorAll('.countdown_activate');
        countdowns.forEach(element => {
            let time = element.getAttribute('time');
            time = parseFloat(time * 60)
            if (time <= 0) {
                element.innerHTML = "Refund Time Expired";
                element.disabled = true;

            } else {
                time--
                time = parseFloat(time / 60).toFixed(2);
                element.setAttribute('time', time);
                element.innerHTML = time + " Minutes Remainning To Ask Refund";

            }
        });

    }, 1000);
</script>
<!-- <script src="{{asset('js/custom.js')}}"></script> -->
@endsection