@extends('Client.layouts.app')

@section('css')
<link rel="stylesheet" href="{{ asset('public/src/css/acc.css') }}">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
@endsection
@section('content')

<div class="l-znurzd">
        <div class="l-7uxhm3">
            <div class="l-1m2qlr5--3">
                <nav class="l-1kiw45x--3">
                    <ul class="l-1aslb2z--3--3--3">
                        <li data-selected="false"> <a style="padding:0px;color:black" href="{{url('/account')}}">Account</a> </li>
                        <li data-selected="false"><a style="padding:0px;color:black" href="{{url('/purchase_history')}}">Purchase History</a></li>
                        <li data-selected="false"><a style="padding:0px;color:black" href="{{url('/affiliate')}}">Affiliation</a></li>
                        <li data-selected="true"><a style="padding:0px;color:black" href="{{url('/withdrawRequests')}}">Withdraw Requests </a></li>
                       
                        <li>
                            <form action="{{url('/logout')}}" method="POST">
                            @csrf
                           <input style = "background:transparent;border:none;padding:0px;font-size:16px" type="submit" value="Sign Out">
                        </form> 
                            
                         </li>
                    </ul>
                </nav>
                <main class="l-1vsbrpk--3">
                    @if($data['check_link'])
                    <section class="affiliation">
                    @if(\Session::has('success'))
                       <h3 style="background:green;color: white;padding:10px;text-align: center;">{{\Session::get('success')}}</h3>

                    @endif

                    @if(\Session::has('error'))
                       <h3 style="background:red;color: white;padding:10px;text-align: center;">{{\Session::get('error')}}</h3>

                    @endif

                    <div class="container">
                            <div class="link-section">
                                <h4>Affiliation Link</h4>
                                <input type="text" class="form-control mt-3" value="{{url('/registerAffiliation')}}/{{$data['check_link']}}" readonly name="" id="">
                            </div>

                            <div class="stats-section mt-5">
                                <h4>Withdraw Requests</h4>
                                <table class="table table-striped table-bordered mt-3">
                                    <thead>
                                        <th>Date</th>
                                        <th>BTC Address</th>
                                        <th>Amount</th>
                                        <th>Status</th>
                                    </thead>
                                    <tbody>
                                    @foreach($data['withdrawrequest_data'] as $data)
                                        <tr>
                                            <td>{{$data->created_at}}</td>
                                            <td >{{$data->btc_address}}</td>
                                            <td name = "order_commission">{{$data->amount}}</td>
                                            <td>
                                               @if($data->status == 0) 
                                                    <label for="" class="text-danger">Pending</label>
                                                @else
                                                     <label for="" class="text-success">Paid</label>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach    
                                    </tbody>
                                </table>
                            </div>


                        </div>
                    </section>
                    @else
                    <section class="not_started_affliate">
                        <div class="container text-center">
                            <p>You Did'nt Start Affiliation Yet </p>
                            <form action="{{ url('/generateLink') }}" method="POST">
                                @csrf 
                                <input type="submit" class = "btn btn-lg btn-success" value="Start Affiliation">
                            </form>
                            <p class="mt-2">You Will Get Upto 20% Of First Order Of New User Reached By Your Link </p>

                        </div>
                    </section>
                    @endif
                </main>
            </div>
        </div>
    </div>


@endsection

@section('js')
<script src="{{asset('public/js/affiliation2.js')}}"></script>
@endsection