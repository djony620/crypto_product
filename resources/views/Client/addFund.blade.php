@extends('Client.layouts.app')

@section('css')
<link rel="stylesheet" href="{{ asset('public/src/css/acc.css') }}">
@endsection
@section('content')

<div class="l-znurzd">
    <div class="l-7uxhm3">
        <div class="l-1m2qlr5--3">

            <nav class="l-1kiw45x--3">
                <ul class="l-1aslb2z--3--3--3">
                    <li data-selected="true"> <a style="padding:0px;color:black" href="{{url('/account')}}">Account</a> </li>
                    <li data-selected="false"><a style="padding:0px;color:black" href="{{url('/purchase_history')}}">Purchase History</a></li>
                        <li data-selected="false"><a style="padding:0px;color:black" href="{{url('/affiliate')}}">Affiliation</a></li>
                        <li data-selected="false"><a style="padding:0px;color:black" href="{{url('/withdrawRequests')}}">Withdraw Requests </a></li> 
                    <li>
                        <form action="{{url('/logout')}}" method="POST">
                            @csrf
                            <input style="background:transparent;border:none;padding:0px;font-size:16px" type="submit" value="Sign Out">
                        </form>

                    </li>
                </ul>
            </nav>

            <main class="l-1vsbrpk--3">
                <div>
                    <div class="l-1stf43z--3">
                        <h2 class="l-1cdi7op--3 eqn5xv82">Add Funds using</h2>

                    </div>
                    <div class="l-1nocmay">
                        <div class="l-1hzp5ku--3">
                            <div style="display: unset;">
                                <div class="l-90le0k--3">
                                    <img id="qr_code" class="l-gukyn8--3" src="https://chart.googleapis.com/chart?chs=300x300&cht=qr&chl=bitcoin:" crossorigin="anonymous" style="padding: 17px 17px 17px 17px;min-width:256px;min-height:256px;"><svg width="64" height="64" class="l-1njhkq1--3" viewBox="0 0 60 60" fill="none">
                                        <path fill-rule="evenodd" clip-rule="evenodd" d="M59.103 37.258c-4.01 16.038-20.324 25.855-36.361 21.845C6.704 55.093-3.113 38.848.897 22.742 4.907 6.704 21.152-3.113 37.258.897c16.038 4.01 25.855 20.324 21.845 36.361z" fill="#F7931A"></path>
                                        <path fill-rule="evenodd" clip-rule="evenodd" d="M43.207 25.71c.623-4.01-2.42-6.153-6.567-7.536l1.314-5.46-3.25-.761-1.313 5.253a153.44 153.44 0 01-2.627-.622l1.314-5.323-3.319-.83-1.313 5.393c-.76-.138-1.452-.277-2.143-.484l-4.493-1.106-.899 3.525 2.42.554c1.313.345 1.52 1.244 1.52 1.935l-1.59 6.153c.139 0 .277.069.346.138-.07-.07-.207-.07-.346-.138l-2.143 8.64c-.138.416-.553 1.038-1.52.761.069.07-2.35-.553-2.35-.553l-1.66 3.733 4.286 1.106c.83.208 1.59.415 2.35.622l-1.382 5.462 3.318.76 1.314-5.392c.898.276 1.797.484 2.626.691l-1.313 5.392 3.318.83 1.314-5.461c5.599 1.037 9.885.622 11.613-4.425 1.452-4.147-.069-6.498-3.041-8.019 2.212-.483 3.802-1.935 4.216-4.839zm-7.535 10.576c-.967 4.079-7.88 1.867-10.092 1.313l1.797-7.258c2.212.553 9.401 1.66 8.295 5.945zM36.71 25.64c-.898 3.733-6.636 1.867-8.503 1.383l1.66-6.567c1.866.483 7.811 1.313 6.843 5.184z" fill="#fff"></path>
                                    </svg>
                                </div>
                                <a target="" id="openInWallet" href="bitcoin:" class="l-ifhvsj--3 e8fmg4a1"><svg width="16" height="16" viewBox="0 0 16 16" style="margin-right: 8px;">
                                        <path fill="#FFF" d="M13.1893398,1.75 L10.3333333,1.75 C9.91911977,1.75 9.58333333,1.41421356 9.58333333,1 C9.58333333,0.585786438 9.91911977,0.25 10.3333333,0.25 L15,0.25 C15.4142136,0.25 15.75,0.585786438 15.75,1 L15.75,5.66666667 C15.75,6.08088023 15.4142136,6.41666667 15,6.41666667 C14.5857864,6.41666667 14.25,6.08088023 14.25,5.66666667 L14.25,2.81066017 L6.97477453,10.0858856 C6.68188131,10.3787789 6.20700758,10.3787789 5.91411436,10.0858856 C5.62122114,9.79299242 5.62122114,9.31811869 5.91411436,9.02522547 L13.1893398,1.75 Z M11.9166667,8.77777778 C11.9166667,8.36356422 12.2524531,8.02777778 12.6666667,8.02777778 C13.0808802,8.02777778 13.4166667,8.36356422 13.4166667,8.77777778 L13.4166667,13.4444444 C13.4166667,14.7177676 12.3844343,15.75 11.1111111,15.75 L2.55555556,15.75 C1.28223238,15.75 0.25,14.7177676 0.25,13.4444444 L0.25,4.88888889 C0.25,3.61556572 1.28223238,2.58333333 2.55555556,2.58333333 L7.22222222,2.58333333 C7.63643578,2.58333333 7.97222222,2.91911977 7.97222222,3.33333333 C7.97222222,3.7475469 7.63643578,4.08333333 7.22222222,4.08333333 L2.55555556,4.08333333 C2.11065951,4.08333333 1.75,4.44399284 1.75,4.88888889 L1.75,13.4444444 C1.75,13.8893405 2.11065951,14.25 2.55555556,14.25 L11.1111111,14.25 C11.5560072,14.25 11.9166667,13.8893405 11.9166667,13.4444444 L11.9166667,8.77777778 Z">
                                        </path>
                                    </svg>Open in Wallet</a>
                            </div>
                        </div>
                        <div class="l-eb3agv--3"><span class="l-grzrk7">Send Bitcoin to this address</span><span class="l-1oupn8s">Copied!</span>
                        <input readonly="" id="onlyAddress" class="l-sp1jao" value="">
                        <input type="hidden" readonly="" id="onlyInvoice" class="l-sp1jao" value="">
                        <input type="hidden" readonly="" id="onlyCode" class="l-sp1jao" value="">
                        
                        </div>

                        <div id = "notifications">

                        </div>
                        <p class="l-10g8w73--3 eqn5xv80">Top-ups under 0.002 BTC incur a fee of 0.00001 BTC</p>
                        <div class="l-yqzhoz"><svg width="56" height="56" viewBox="0 0 56 56" fill="none">
                                <path d="M31.0472 9.4136C29.5542 9.96998 28.6375 11.5269 28.6375 13.5129C28.6375 14.9537 28.9675 15.8909 29.7192 16.5469C30.5423 17.2657 31.4062 17.3488 32.4748 16.8241C33.5255 16.3001 34.1327 15.479 34.4911 14.1016C35.2448 11.1692 33.3427 8.59454 31.0472 9.4136Z" fill="#449BF7"></path>
                                <path d="M28.491 19.35C26.6707 19.7262 24.6709 21.2667 22.8664 23.6948C21.5887 25.4003 21.0323 26.5949 21.2772 27.0562C21.3755 27.2371 21.555 27.3836 21.689 27.3836C21.8045 27.3836 22.4751 26.5309 23.1799 25.4828C23.8842 24.4485 24.6709 23.417 24.9336 23.186C25.917 22.3517 26.6892 22.9081 26.3268 24.2202C26.2463 24.5654 25.3593 27.4991 24.3917 30.745C22.2929 37.7166 21.8659 39.5481 21.8514 41.5651C21.8514 42.827 21.8995 43.122 22.2269 43.6309C22.9833 44.8585 24.6386 45.009 26.6212 44.0447C29.3272 42.6976 33.21 38.0591 33.0457 36.3543C33.016 36.0243 32.9176 35.829 32.7711 35.829C32.5223 35.829 32.2953 36.1088 30.5898 38.471C29.1134 40.5018 28.1656 40.8351 28.1656 39.3079C28.1656 38.9468 29.017 35.6343 30.0479 31.9277C31.0802 28.2244 32.0167 24.735 32.1157 24.1443C32.3652 22.7847 32.2332 21.1604 31.8194 20.472C31.2459 19.4814 29.9826 19.0543 28.491 19.35Z" fill="#449BF7"></path>
                                <path d="M27.9993 55.3413C12.9235 55.3413 0.660004 43.0745 0.660004 27.9987C0.660004 12.9235 12.9235 0.660004 27.9993 0.660004C43.0732 0.660004 55.34 12.9235 55.34 27.9987C55.34 43.0745 43.0732 55.3413 27.9993 55.3413ZM27.9993 2.62681C14.0099 2.62681 2.62681 14.0099 2.62681 27.9987C2.62681 41.9875 14.0079 53.3719 27.9993 53.3719C41.9894 53.3719 53.3705 41.9908 53.3705 27.9987C53.3705 14.0079 41.9894 2.62681 27.9993 2.62681Z" fill="#449BF7"></path>
                                <path d="M27.9993 56.0013C12.5605 56.0013 0 43.4388 0 27.9987C0 12.5599 12.5605 0 27.9993 0C43.4395 0 56 12.5599 56 27.9987C56 43.4388 43.4388 56.0013 27.9993 56.0013ZM27.9993 1.32001C13.2885 1.32001 1.32001 13.2879 1.32001 27.9987C1.32001 42.7115 13.2885 54.6813 27.9993 54.6813C42.7108 54.6813 54.68 42.7115 54.68 27.9987C54.68 13.2879 42.7108 1.32001 27.9993 1.32001ZM27.9993 54.0319C13.6449 54.0319 1.96681 42.3538 1.96681 27.9987C1.96681 13.6443 13.6449 1.96681 27.9993 1.96681C42.3531 1.96681 54.0305 13.6443 54.0305 27.9987C54.0305 42.3538 42.3531 54.0319 27.9993 54.0319ZM27.9993 3.28682C14.3729 3.28682 3.28682 14.3722 3.28682 27.9987C3.28682 41.6251 14.3729 52.7119 27.9993 52.7119C41.6258 52.7119 52.7105 41.6251 52.7105 27.9987C52.7105 14.3722 41.6251 3.28682 27.9993 3.28682Z" fill="#449BF7"></path>
                            </svg>
                            <p class="l-10g8w73--3 eqn5xv80">Note: <b>This address is only valid for one top-up.</b>
                                <br>For your next top-up a new address will be displayed here.</p>
                        </div>
                        <div class="l-gx3uvr--3"><button class="l-sv66i2"><svg width="14" height="14" viewBox="0 0 14 14" style="margin-right: 8px;">
                                    <path fill="#3290F6" d="M6.125,1 L0.4375,1 C0.196,1 0,1.1955625 0,1.4375 L0,7.125 C0,7.3669375 0.196,7.5625 0.4375,7.5625 L6.125,7.5625 C6.3665,7.5625 6.5625,7.3669375 6.5625,7.125 L6.5625,1.4375 C6.5625,1.1955625 6.3665,1 6.125,1 Z M5.6875,6.6875 L0.875,6.6875 L0.875,1.875 L5.6875,1.875 L5.6875,6.6875 Z M2.1875,5.8125 L4.375,5.8125 C4.6165,5.8125 4.8125,5.6169375 4.8125,5.375 L4.8125,3.1875 C4.8125,2.9455625 4.6165,2.75 4.375,2.75 L2.1875,2.75 C1.946,2.75 1.75,2.9455625 1.75,3.1875 L1.75,5.375 C1.75,5.6169375 1.946,5.8125 2.1875,5.8125 Z M2.625,3.625 L3.9375,3.625 L3.9375,4.9375 L2.625,4.9375 L2.625,3.625 Z M13.5625,1 L7.875,1 C7.6335,1 7.4375,1.1955625 7.4375,1.4375 L7.4375,7.125 C7.4375,7.3669375 7.6335,7.5625 7.875,7.5625 L13.5625,7.5625 C13.804,7.5625 14,7.3669375 14,7.125 L14,1.4375 C14,1.1955625 13.804,1 13.5625,1 Z M13.125,6.6875 L8.3125,6.6875 L8.3125,1.875 L13.125,1.875 L13.125,6.6875 Z M9.625,5.8125 L11.8125,5.8125 C12.054,5.8125 12.25,5.6169375 12.25,5.375 L12.25,3.1875 C12.25,2.9455625 12.054,2.75 11.8125,2.75 L9.625,2.75 C9.3835,2.75 9.1875,2.9455625 9.1875,3.1875 L9.1875,5.375 C9.1875,5.6169375 9.3835,5.8125 9.625,5.8125 Z M10.0625,3.625 L11.375,3.625 L11.375,4.9375 L10.0625,4.9375 L10.0625,3.625 Z M13.5625,8.4375 L7.875,8.4375 C7.6335,8.4375 7.4375,8.6330625 7.4375,8.875 L7.4375,14.5625 C7.4375,14.8044375 7.6335,15 7.875,15 L13.5625,15 C13.804,15 14,14.8044375 14,14.5625 L14,8.875 C14,8.6330625 13.804,8.4375 13.5625,8.4375 Z M13.125,14.125 L8.3125,14.125 L8.3125,9.3125 L13.125,9.3125 L13.125,14.125 Z M9.625,13.25 L11.8125,13.25 C12.054,13.25 12.25,13.0544375 12.25,12.8125 L12.25,10.625 C12.25,10.3830625 12.054,10.1875 11.8125,10.1875 L9.625,10.1875 C9.3835,10.1875 9.1875,10.3830625 9.1875,10.625 L9.1875,12.8125 C9.1875,13.0544375 9.3835,13.25 9.625,13.25 Z M10.0625,11.0625 L11.375,11.0625 L11.375,12.375 L10.0625,12.375 L10.0625,11.0625 Z M6.125,8.4375 L3.9375,8.4375 C3.696,8.4375 3.5,8.6330625 3.5,8.875 L3.5,10.625 L0.4375,10.625 C0.196,10.625 0,10.8205625 0,11.0625 L0,14.5625 C0,14.8044375 0.196,15 0.4375,15 L3.9375,15 C4.179,15 4.375,14.8044375 4.375,14.5625 L4.375,11.5 L6.125,11.5 C6.3665,11.5 6.5625,11.3044375 6.5625,11.0625 L6.5625,8.875 C6.5625,8.6330625 6.3665,8.4375 6.125,8.4375 Z M3.5,14.125 L0.875,14.125 L0.875,11.5 L3.5,11.5 L3.5,14.125 Z M5.6875,10.625 L4.375,10.625 L4.375,9.3125 L5.6875,9.3125 L5.6875,10.625 Z M0,8.875 C0,8.6330625 0.196,8.4375 0.4375,8.4375 L2.1875,8.4375 C2.429,8.4375 2.625,8.6330625 2.625,8.875 C2.625,9.1169375 2.429,9.3125 2.1875,9.3125 L0.4375,9.3125 C0.196,9.3125 0,9.1169375 0,8.875 Z M2.625,13.25 L1.75,13.25 L1.75,12.375 L2.625,12.375 L2.625,13.25 Z M6.5625,12.375 L6.5625,14.5625 C6.5625,14.8044375 6.3665,15 6.125,15 L5.25,15 C5.0085,15 4.8125,14.8044375 4.8125,14.5625 C4.8125,14.3205625 5.0085,14.125 5.25,14.125 L5.6875,14.125 L5.6875,12.375 C5.6875,12.1330625 5.8835,11.9375 6.125,11.9375 C6.3665,11.9375 6.5625,12.1330625 6.5625,12.375 Z" transform="translate(0 -1)"></path>
                                </svg>Display QR Code</button></div><a target="" id="openInWallet2" href="bitcoin:" class="l-108dmce--3--3 e8fmg4a1"><svg width="16" height="16" viewBox="0 0 16 16" style="margin-right: 8px;">
                                <path fill="#FFF" d="M13.1893398,1.75 L10.3333333,1.75 C9.91911977,1.75 9.58333333,1.41421356 9.58333333,1 C9.58333333,0.585786438 9.91911977,0.25 10.3333333,0.25 L15,0.25 C15.4142136,0.25 15.75,0.585786438 15.75,1 L15.75,5.66666667 C15.75,6.08088023 15.4142136,6.41666667 15,6.41666667 C14.5857864,6.41666667 14.25,6.08088023 14.25,5.66666667 L14.25,2.81066017 L6.97477453,10.0858856 C6.68188131,10.3787789 6.20700758,10.3787789 5.91411436,10.0858856 C5.62122114,9.79299242 5.62122114,9.31811869 5.91411436,9.02522547 L13.1893398,1.75 Z M11.9166667,8.77777778 C11.9166667,8.36356422 12.2524531,8.02777778 12.6666667,8.02777778 C13.0808802,8.02777778 13.4166667,8.36356422 13.4166667,8.77777778 L13.4166667,13.4444444 C13.4166667,14.7177676 12.3844343,15.75 11.1111111,15.75 L2.55555556,15.75 C1.28223238,15.75 0.25,14.7177676 0.25,13.4444444 L0.25,4.88888889 C0.25,3.61556572 1.28223238,2.58333333 2.55555556,2.58333333 L7.22222222,2.58333333 C7.63643578,2.58333333 7.97222222,2.91911977 7.97222222,3.33333333 C7.97222222,3.7475469 7.63643578,4.08333333 7.22222222,4.08333333 L2.55555556,4.08333333 C2.11065951,4.08333333 1.75,4.44399284 1.75,4.88888889 L1.75,13.4444444 C1.75,13.8893405 2.11065951,14.25 2.55555556,14.25 L11.1111111,14.25 C11.5560072,14.25 11.9166667,13.8893405 11.9166667,13.4444444 L11.9166667,8.77777778 Z">
                                </path>
                            </svg>Open in Wallet</a>
                    </div>
                </div>
            </main>
        </div>
    </div>
</div>

@endsection

@section('js')
<script>

    let updateTransaction = (address,amount,status)=>{
        let csrf = document.getElementsByTagName('meta')[2].getAttribute('content');

        $.ajax({
            url: `http://${window.location.hostname}/btcCallback`,
            type: 'POST',
            data:{
            '_token':csrf,
            'address':address,
            'amount':amount,
            'status':status            

            },
            success: function(response) {
                console.log(response);
                
            },
            error: function(error) {
                console.log(error)
            }

        });



    }

    let btcPaymentResponse = () => {
        let address = document.getElementById('onlyAddress').value;
        let notifications = document.getElementById('notifications');
        $.ajax({
            url: `https://api.bitaps.com/btc/v1/payment/address/state/${address}`,
            type: 'GET',
            success: function(response) {
                console.log(response);
                if(response.transaction_count == 0 && response.pending_transaction_count > 0){
                    notifications.innerHTML = `
                            <h2 style="background: green;padding:10px;color:white">Payment Detected</h2>
                            <h2 style="background: green;padding:10px;color:white ">Amount : ${response.pending_received*0.00000001}</h2>

                            <h2 style="background: orangered;padding:10px;color:white">Unconfirmed (Please dont Leave Page Untill Transaction is confirmed) </h2>
                    
                    `;
                    let amount = response.pending_received*0.00000001;

                    updateTransaction(address,amount,1);
                }
                else
                if(response.transaction_count > 0 && response.pending_transaction_count == 0){
                    notifications.innerHTML = `
                            <h2 style="background: green;padding:10px;color:white">Payment Detected</h2>
                            <h2 style="background: green;padding:10px;color:white ">Amount : ${response.received*0.00000001}</h2>

                            <h2 style="background: green;padding:10px;color:white">Confirmed </h2>
                    
                    `;

                    let amount = response.received*0.00000001;

                    updateTransaction(address,amount,2);

                }
            },
            error: function(error) {
                console.log(error)
            }

        });

    }


    let createAddress = ()=>{
        let csrf = document.getElementsByTagName('meta')[2].getAttribute('content');
        let address = document.getElementById('onlyAddress').value;
        let invoice = document.getElementById('onlyInvoice').value;
        let payment_code = document.getElementById('onlyCode').value;

        $.ajax({
            url: `http://${window.location.hostname}/createAddress`,
            type: 'POST',
            data:{
            '_token':csrf,
            'invoice':invoice,
            'address':address,
            'payment_code':payment_code,            

            },
            success: function(response) {
                console.log(response);
                
            },
            error: function(error) {
                console.log(error)
            }

        });



  }


    

    let getAddress = () => {
        let qr_code = document.getElementById('qr_code');
        let openInWallet = document.getElementById('openInWallet');
        let onlyAddress = document.getElementById('onlyAddress');
        let onlyInvoice = document.getElementById('onlyInvoice');
        let onlyCode = document.getElementById('onlyCode');
        $.ajax({
            url: `https://api.bitaps.com/btc/v1/create/payment/address`,
            type: 'POST',
            datatType: "application/json",
            data: JSON.stringify({

                // "callback_link" : `http://${window.location.hostname}/btcCallback` ,
                "forwarding_address": "bc1qvtt33533wgrd66uh7578kqsek7d4sqe0nd8f6l",
                "confirmations": 3

            }),
            success: function(response) {
                console.log(response);
                qr_code.setAttribute('src', `https://chart.googleapis.com/chart?chs=300x300&cht=qr&chl=bitcoin:${response.address}`);
                openInWallet.setAttribute('href', `bitcoin:${response.address}`);
                onlyAddress.value = response.address;
                onlyInvoice.value = response.invoice;
                onlyCode.value = response.payment_code;
                createAddress()
                // createAddressDb();                    
            },
            error: function(error) {
                console.log(error)
            }

        });

    }

  
    getAddress();
    
    setInterval(btcPaymentResponse,5000);
</script>
@endsection