@extends('Client.layouts.app')

@section('content')
<!-- hero carousel     -->
<?php
use App\Http\Controllers\ClientHomeController;
?>
<div class="carousel-root l-1be3reo" tabindex="0">
  <div class="carousel carousel-slider" style="width: 100%;">
    <button type="button" aria-label="previous slide / item" class="control-arrow control-prev control-disabled"></button>
    <div class="slider-wrapper axis-horizontal">
      <ul class="slider animated transition" style="">
        <li class="slide ">
          <a rel="noopener" target="_blank" href="" title="Rewards program" class="l-iigu7d" style="background-image:url('/public/banners/<?php echo ClientHomeController::homeBanners()['banner1']->image?>');background-size: 1783px 230px;
background-repeat: no-repeat;background-position: center center;">
            <div class="l-lqxlj7">
              <div title="Rewards program" class="l-12hu1ls" >
                <div class="l-u8d87p-3">
                 @if(ClientHomeController::homeBanners()['banner1']->heading != NULL)
                 <h1>{{ClientHomeController::homeBanners()['banner1']->heading}} </h1>                 
                 @endif
                 @if(ClientHomeController::homeBanners()['banner1']->text != NULL)
                 <h4>{{ClientHomeController::homeBanners()['banner1']->text}} </h4>
                 @endif
                </div>
              </div>
            </div>
          </a></li>
        <li class="slide selected"><a rel="noopener" target="_blank" href="/usd-account" title="USD Account" class="l-101use2" style="background-image:url('/public/banners/<?php echo ClientHomeController::homeBanners()['banner2']->image?>');
background-size: 1783px 230px;background-repeat: no-repeat;background-position: center center;">
            <div class="l-lqxlj7">
              <div title="USD Account" class="l-8e1aqe" >
                <div class="l-u8d87p-3">
                @if(ClientHomeController::homeBanners()['banner2']->heading != NULL)
                 <h1>{{ClientHomeController::homeBanners()['banner2']->heading}} </h1>                 
                 @endif
                 @if(ClientHomeController::homeBanners()['banner2']->text != NULL)
                 <h4>{{ClientHomeController::homeBanners()['banner2']->text}} </h4>
                 @endif
                </div>
              </div>
            </div>
          </a></li>
        <li class="slide "><a rel="noopener" target="_blank" href="/rewards" title="Rewards program" class="l-iigu7d" style="background-image:url('/public/banners/<?php echo ClientHomeController::homeBanners()['banner1']->image?>');
background-size: 1783px 230px;background-repeat: no-repeat;background-position: center center;">
            <div class="l-lqxlj7">
              <div title="Rewards program" class="l-12hu1ls"  >
                <div class="l-u8d87p-3">
                @if(ClientHomeController::homeBanners()['banner1']->heading != NULL)
                 <h1>{{ClientHomeController::homeBanners()['banner1']->heading}} </h1>                 
                 @endif
                 @if(ClientHomeController::homeBanners()['banner1']->text != NULL)
                 <h4>{{ClientHomeController::homeBanners()['banner1']->text}} </h4>
                 @endif

                </div>
              </div>
            </div>
          </a></li>
        <li class="slide selected"><a rel="noopener" target="_blank" href="/usd-account" title="USD Account" class="l-101use2" style="background-image:url('/public/banners/<?php echo ClientHomeController::homeBanners()['banner2']->image?>');
background-size: 1783px 230px;background-repeat: no-repeat;background-position: center center;">
            <div class="l-lqxlj7">
              <div title="USD Account" class="l-8e1aqe" >
                <div class="l-u8d87p-3">
                @if(ClientHomeController::homeBanners()['banner2']->heading != NULL)
                 <h1>{{ClientHomeController::homeBanners()['banner2']->heading}} </h1>                 
                 @endif
                 @if(ClientHomeController::homeBanners()['banner2']->text != NULL)
                 <h4>{{ClientHomeController::homeBanners()['banner2']->text}} </h4>
                 @endif

                </div>
              </div>
            </div>
          </a></li>
      </ul>
    </div>
    <button type="button" aria-label="next slide / item" class="control-arrow control-next control-disabled"></button>
    <!-- <ul class="control-dots">
        <li class="dot " value="0" role="button" tabindex="0" aria-label="slide item 1"></li>
        <li class="dot selected" value="1" role="button" tabindex="0" aria-label="slide item 2"></li>
      </ul> -->
  </div>
</div>
<!-- /.hero carousel     -->

<!-- content-section -->
<div class="l-znurzd">
  <div class="l-lqxlj7">
    <div class="l-7ow0xr-3">
      <!-- sidebar  -->
      <div class="l-h528vh">
        <p class="l-1k3m6oj-3" style="color: rgb(29, 35, 43);">Country</p>
        <select name="country select2" onchange="filter1(this)" style="background:white;padding:10px;border:none;width:100%;box-shadow:2px 2px 2px silver" id="">
          <option value=0>Select Country</option>
          @foreach($data['countries'] as $country)
          <option cat_id={{$data['cat_id']}} value={{$country->id}}>{{$country->name}}</option>
          @endforeach
        </select>
        <!-- <div class="l-h3lxuk-3"><button role="combobox" aria-haspopup="listbox" aria-owns="downshift-1-menu"
                aria-expanded="false" class="l-13ypejg"><span class="l-1ba8w0v-3">🇺🇸</span><input
                  id="downshift-0-input" tabindex="-1" aria-autocomplete="list" aria-controls="downshift-1-menu"
                  aria-labelledby="downshift-1-label" autocomplete="off" type="text" class="l-krszde" value="USA"
                  placeholder="USA" style="padding: 12px 40px;"><svg id="downshift-1-toggle-button" tabindex="-1"
                  class="l-1f7m35t" width="24" height="24" viewBox="0 0 24 24">
                  <path d="M16.59 8.59L12 13.17 7.41 8.59 6 10l6 6 6-6z"></path>
                  <path d="M0 0h24v24H0z" fill="none"></path>
                </svg></button>
              <div id="downshift-1-menu" role="listbox" aria-labelledby="downshift-1-label"></div>
              <div tabindex="0"></div>
            </div> -->
        <ol class="l-1wg98qd">
          <li>
            <!-- geftcard categories   -->
            <ul>
              <p class="separator l-1k3m6oj-3" style="color: rgb(29, 35, 43);"> All Categories </p>
              <li><a href="{{ url('/home') }}" data-selected=""> All Categories </a></li>
              @foreach($data['categories'] as $category)
              <li>
                <a href="{{ url('/home/category/'.$category->id) }}"><img style="width:20px;height:20px;border-radius:50%;margin-right:10px" src="{{ asset('public/uploads/'.$category->icon_url) }}" alt=""> {{$category->name}} </a>
              </li>
              @endforeach
            </ul>
            <!-- /.geftcard categories   -->

          </li>
          <!-- <li> -->
          <!-- /.Refill categories   -->
          <!-- <ul>
                  <p class="separator l-1k3m6oj-3" style="color: rgb(29, 35, 43);">Refills</p>
                  <li><a href="/buy/usa/refill?hl=en">📱 Prepaid phones</a></li>
                </ul> -->
          <!-- /.Refill categories   -->

          <!-- </li> -->
          <!-- <li> -->
          <!-- /.Lightning categories   -->
          <!-- <ul>
                  <p class="separator l-1k3m6oj-3" style="color: rgb(29, 35, 43);">Lightning Utilities</p>
                  <li><a href="/buy/usa/lightning?hl=en">⚡ Lightning</a></li>
                </ul> -->
          <!-- /.Lightning categories   -->

          <!-- </li> -->
        </ol>
      </div>
      <!-- sidebar  -->

      <!-- /right-col -->
      <div class="l-rkk3yt-3">

        <!-- catgory title and sort section -->
        <div class="l-17ytp44-3">
          <h1 style="color: rgb(29, 35, 43);">
            @if($data['cat_name'])
            {{$data['cat_name']}}
            @else
            Gift Cards / Amazon Gift Cards  demo stuff
            @endif

          </h1>
<!--removed area start-->

  
        </div>
        <!-- /.catgory title and sort section -->

        <!-- filter variables -->
        <input type="hidden" id="country_id" value={{$data['country_id']}}>
        <input type="hidden" id="sorting_id" value={{$data['sorting_id']}}>
        <!-- filter variables -->

        <!-- products section -->
        <div class="l-eh9u44-3" id="products_section">

          @foreach($data['products'] as $product)

          <!-- product  -->
          <a class="l-13vjo0e-3-3" href="{{ url('/product/'.$product->id) }}" style="color: rgb(29, 35, 43);">
            <div class="l-d6m5lc" style="background-color: rgb(255, 255, 255); color: black;">
              <div data-fill="true" class="l-xyawq0-3-3">
                <img src="{{ asset('public/product_images/'.$product->image) }}" loading="lazy" class="l-1uv64rf-3" width="350" height="212" alt="{{$product->name}}" layout="responsive" decoding="async" style="background: rgb(255, 255, 255);">
              </div>
              <!-- badge -->
              <!-- <div class="l-7f0b15-3-3">Featured</div> -->
              <!-- badge -->

            </div>
            <p class="l-nfokyf-3" style = "margin-bottom:5px">{{$product->name}}</p>
            <h5 class="" style="color: red;margin-top:0px;font-weight:bold">{{$product->min_price}} {{$product->currency}}</h5>

          </a>
          <!-- product  -->
          @endforeach
        </div>
        <!-- /.products section -->
      </div>
      <!-- /right-col -->
    </div>
  </div>
</div>
<!-- /.content-section -->







@endsection

@section('js')
<!-- <script src="{{asset('js/cart.js')}}"></script> -->
<script src="{{asset('public/js/sortProducts.js')}}"></script>

<!-- <script src="{{asset('js/getCart.js')}}"></script> -->
@endsection