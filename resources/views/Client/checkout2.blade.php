<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name = "_token" content={{csrf_token()}}>

    <title>Checkout</title>


<!-- CSS -->
<link rel="stylesheet" href="{{asset('/public/src/css/alertify.min.css')}}"/>
<link rel="stylesheet" href="{{asset('/public/src/css/default.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('/public/src/css/chkout2.css')}}">
<style>
.blur_back{
    width:100%!important;
    height:100vh!important;
    background:rgb(0,0,0,0.8);
    display:flex;
    justify-content:center;
    align-items:center;
    display:none
    
    
}

.blur_back_active{
    width:100%!important;
    height:100vh!important;
    background:rgb(0,0,0,0.8);
    display:flex;
    justify-content:center;
    align-items:center;

    
}

.popupBox{
    padding:40px 100px;
    background:white!important;
    text-align:center;
}

</style>
</head>

<body>
    
<div class = "blur_back" id = "blur_back">
    <div class = "popupBox">
      <!-- <img style = "width:150px" src = "{{asset('public/src/site_images/gs.webp ')}}">  -->  
        <h3> Order Has Seccessfully Submitted</h3>
        <h4>ThankYou For Shopping</h4>
    </div>
</div>


<div class="l-1vm6o2n">
        <div class="l-1yfombv--1">
            <div class="l-lqxlj7">
                <header class="l-9416li"><a href="{{ url('/home') }}"><img src="{{ asset('public/src/site_images/egify.png') }}" loading="lazy" layout="fixed" height="50"></a>
                    <ol class="l-1w0jv0m--1">
                        <li class="l-1ii8ot7">
                            <p class="l-sf5iph--1--1 eqn5xv80">Payment</p>
                        </li>
                    </ol>
                </header>
            </div>
        </div>
        <div class="l-znurzd">
            <div class="l-7uxhm3">
                <div class="l-tb9nta--1">
                    <div>
                        <div class="l-16tjgcu">
                            <div class="l-phv52b">
                                <h2 class="l-1dr8rbk--1 eqn5xv82">Order details</h2>
                            </div>
                            <div class="l-158u5xj--1">
                                <p class="l-1y9bx72">Payment method:</p>
                                <div class="l-1vt5a71--1--1">Pay With Funded Amount</div>
                            </div>
                            <hr class="l-1j6az3q">
                            <div id="cart_products">
                                <!-- product -->
                                <!-- <div class="l-i9fq99--1"><a href="/buy/bitrefill-giftcard-usd/">
                                        <div class="l-17nvt0x" style="background: rgb(68, 155, 247); border: 1px solid rgb(163, 178, 194);">
                                            <img src="https://www.bitrefill.com/content/cn/b_rgb%3A449bf7%2Cc_pad%2Ch_48%2Cw_48/v1585678956/bitrefill-balance-usd-icon.png" loading="lazy" width="48" height="48" data-fill="true" style="background: rgb(68, 155, 247);"></div>
                                    </a>
                                    <div class="l-yhvrli--1">
                                        <div class="l-1wrjqnn--1">
                                            <p class="l-a4yqo7">Bitrefill Balance Card (USD)</p>
                                            <div class="l-4u69oq"><span>$20</span><span></span></div>
                                        </div>
                                        <div class="l-t3twgy">0.00210000 BTC</div>
                                    </div>
                                </div> -->
                                <!-- product -->


                            </div>
                            <div class="l-xp0d1v"><span>Total:</span><span id="totalInBTC">0 BTC</span></div>
                        </div>
                    </div>
                    <div>
                        <div class="l-16tjgcu">
                            <div class="l-1j5q9vf">
                                <div class="l-1iyoj2o">
                                    <div class="l-vj4kk0">
                                        <h1 class="l-jwtvmq--1--1 eqn5xv81">Payment</h1>
                                    </div>
                                </div>
                                <!-- timer -->
                                <!-- <div class="l-zyef45"><svg width="12" height="12" viewBox="0 0 12 12" class="l-9brbjt--1">
                                        <circle cx="6" cy="6" r="5" stroke-width="2px" class="l-269e4n"></circle>
                                        <circle cx: 6; cy: 6; r: 5; stroke-width: 2px; transform: rotate(-90deg, 6, 6); class="l-wmhbqs" style="stroke-dasharray: 31.4159; stroke-dashoffset: 29.531;"></circle>
                                    </svg><span class="l-hnb606" id="demo"></span>
                                </div> -->
                                <!-- timer -->

                            </div>
                            <div class="l-2geg7c" style = "text-align:center" id = "order">
                                <div style = "text-align:center">
                                    <h1>Final Confirmation</h1>
                                    <textarea name="comment" id="order_comment" style="width: 100%;"  rows="10" placeholder="This is order comment box please type if you have any kind customize / modification for your order example shipping address / request countries / quantity etc. if not just leave empty and can submit order"></textarea>
                                    <p>Please Confirm That the Order Here Is Correct</p>

                                </div>
                                
                                <button class="l-x10c0s--3 e8fmg4a1" onclick="submit_order_instant()">Submit Order</a>
                                

                            </div>
 
                            <table style="width: 100%;margin-top:60px" id = "order_table">
                                        <!-- <tr>
                                            <td style = "width:40%">
                                            <img style = "width:95%" src="{{ asset('product_images/5f15d23b4f387.png') }}" alt="">
                                            </td>
                                            <td>
                                                 <h2>Amazon Card</h2>                                                      
                                                 <h5>$10</h5>
                                                 <a class="l-x10c0s--3 e8fmg4a1" >Redeem</a>
                                                 <input type="text" readonly value="64siidha" style="width: 100%;height:30px;background:#f0f0f0;border:none;text-align:center">   
                                            </td>
                                        </tr> -->
                                </table>
 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- <script>
        // Set the date we're counting down to
        var countDownDate = new Date().getTime() + 15 * 60 * 1000;

        // Update the count down every 1 second
        var x = setInterval(function() {

            // Get today's date and time
            var now = new Date().getTime();

            // Find the distance between now and the count down date
            var distance = countDownDate - now;

            // Time calculations for days, hours, minutes and seconds
            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((distance % (1000 * 60)) / 1000);

            // Output the result in an element with id="demo"
            document.getElementById("demo").innerHTML = minutes + ":" + seconds;

            // If the count down is over, write some text 
            if (distance < 14) {
                clearInterval(x);
                document.getElementById("demo").innerHTML = "EXPIRED";
            }
        }, 1000);
    </script> -->
</body>

</html>
<!-- jQuery 3.1.1 -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js" ></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.15.1/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>

<script src="{{ asset('public/js/checkouts2.js') }}"></script>

<script src="{{ asset('public/js/order.js') }}"></script>