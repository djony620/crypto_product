<!DOCTYPE html>
<?php use App\Http\Controllers\loginBackController; ?>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register</title>
    <link href="{{ asset('public/src/css/landing2.css') }}" rel="stylesheet">
</head>

<body>
    <div class="wrapper" style="background:url('/public/banners/<?php echo loginBackController::loginBack();?>');">
        <div class="container">
            <img src="{{ asset('public/src/site_images/egify.png') }}" alt=""  width="110px" height="110px">
            <h2 style="font-size: 24px;"><span style="font-weight: bold;">Egify</span> Registration </h2>

            <form class="form" method="post" action="{{ url('/register') }}">
                @csrf
                <!-- <input type="text" class="form-control" name="name" value="{{ old('name') }}" placeholder="Full Name">

                @if ($errors->has('name'))
                <div style="background-color: red;width: 60%;margin:0 auto">
                    <p style="color: white;">{{ $errors->first('name') }}</p>
                </div>
                @endif -->

                <br>

                <input type="text" class="form-control" name="username" value="{{ old('username') }}" placeholder="username">
                @if ($errors->has('username'))
                <div style="background-color: red;width: 60%;margin:0 auto">
                    <p style="color: white;">{{ $errors->first('username') }}</p>
                </div>
                @endif


                <br>

                <input type="password" class="form-control" name="password" placeholder="Password">
                @if ($errors->has('password'))
                <div style="background-color: red;width: 60%;margin:0 auto">
                    <p style="color: white;">{{ $errors->first('password') }}</p>
                </div>
                @endif


                <br>

                <input type="password" name="password_confirmation" class="form-control" placeholder="Confirm password">
                @if ($errors->has('password_confirmation'))
                <div style="background-color: red;width: 60%;margin:0 auto">
                    <p style="color: white;">{{ $errors->first('password_confirmation') }}</p>
                </div>
                @endif


                <br>

                <div class="captcha">
                    <span>{!! captcha_img('math') !!}</span>
                    <br>
                </div>
                <input type="text" id="captcha" name="captcha" class="form-control" placeholder="Enter Captcha Calculation">
                @if ($errors->has('captcha'))
                <div style="background-color: red;width: 60%;margin:0 auto">
                    <p style="color: white;">{{ $errors->first('captcha') }}</p>
                </div>
                @endif

                <!-- affiliation code -->
                <input type="hidden" name="affiliation_code" value="{{$code}}">
                <!-- affiliation code -->


                <button type="submit" id="login-button">Register</button>
                <br>
                <a style="color: white;" href="{{ url('/login') }}" class="text-center">I Already Have An Account </a>

            </form>
        </div>

    </div>

    <div id='hint'>
        <p class="text-center text-gray-500 text-xs">
            &copy;2020 Egify. All rights reserved.
        </p>
    </div>
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
</body>

</html>