<!DOCTYPE html>
<?php use App\Http\Controllers\loginBackController; ?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <link href="{{ asset('public/src/css/landing.css') }}" rel="stylesheet">
</head>
<body >
    <div class="wrapper" style="background:url('/public/banners/<?php echo loginBackController::loginBack();?>') ;">
        <h3 style = "color:white;width:100%;text-align:center;margin-top:10px;font-weight:bold"> <?php echo loginBackController::loginBookmark();?> </h3>
   <div class="container">
        <img src="{{ asset('public/src/site_images/egify.png') }}" alt="" width="110px" height="110px">

            <h2 style="font-size: 30px;"><span style="font-weight: bold;">demosite</span> Login</h2>
            
            <form class="form" method="post" action="{{ url('/login') }}">
                @csrf    
                <input type="text" class="form-control" name="username" value="{{ old('username') }}" placeholder="username">
               
                @if ($errors->has('username'))
                <div style="background-color: red;width: 60%;margin:0 auto" >
                    <p style="color: white;">{{ $errors->first('username') }}</p>
                </div>
                @endif
    
                <br>

                <input type="password" class="form-control" placeholder="Password" name="password">
                @if ($errors->has('password'))
                <div style="background-color: red;width: 100%;" >
                    <p style="color: white;">{{ $errors->first('password') }}</p>
                </div>
                @endif
                <br>

                <div class="captcha">
                    <span>{!! captcha_img('math') !!}</span>
                    <br>
                </div>
                <input type="text" id="captcha" name="captcha" class="form-control" placeholder="Enter Captcha Calculation">
                @if ($errors->has('captcha'))
                <div style="background-color: red;width: 60%;margin:0 auto">
                    <p style="color: white;">{{ $errors->first('captcha') }}</p>
                </div>
                @endif


                <br>

                <button type="submit" id="login-button" style="width:40%;background: #3ec127;color: whitesmoke;">Login</button>
                <a style="color: white;" href="{{ url('/register') }}"> 
                           <div class="reg" id="login-button"   style="display: inline-block;" >Register</div>
                 </a>

                 <br>   
               

            </form>
        </div>
        
    </div>
    
 <div id='hint'>  <p class="text-center text-gray-500 text-xs">
    &copy;2020 Egify. All rights reserved.
  </p></div>
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
</body>
</html>