<!DOCTYPE html>
<?php
use App\Http\Controllers\ClientHomeController;
?>
<html>
<head>
    <meta charset="UTF-8">
    <title>{{ config('app.name') }}</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <meta name = "_token" content={{csrf_token()}}>

<script src="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/alertify.min.js"></script>
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/alertify.min.css"/>
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" crossorigin="anonymous">


  <!-- Egify Links   -->
  <!-- <link data-rh="true" rel="manifest" href="/site.webmanifest" /> -->
  <!-- <link data-rh="true" rel="mask-icon" href="/img/safari-pinned-tab.svg" color="#459BF7" /> -->
  
  <!-- <style data-emotion="l"></style> -->
  <style>

/* Chrome, Safari, Edge, Opera */
input::-webkit-outer-spin-button,
input::-webkit-inner-spin-button {
  -webkit-appearance: none;
  margin: 0;
}

/* Firefox */
input[type=number] {
  -moz-appearance: textfield;
}

    .search-bar input,
    .search-btn,
    .search-btn:before,
    .search-btn:after {
        transition: all 0.25s ease-out;
    }
    .search-bar input,
    .search-btn {
        width: 3em;
        height: 3em;
    }
    .search-bar input:invalid:not(:focus),
    .search-btn {
        cursor: pointer;
background-image:url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAMAAABEpIrGAAAAA3NCSVQICAjb4U/gAAAACXBIWXMAAADhAAAA4QFwGBwuAAAAGXRFWHRTb2Z0d2FyZQB3d3cuaW5rc2NhcGUub3Jnm+48GgAAAapQTFRF////gICAv4BgRFVmPVVhOVVjPFdltoZSuIRNuYZQt4RPxIxUPVZiuYlSuIVPnXlTuohPu4hQPFZkxI5TuoZQPFVkwo1Uw49Uxo5VsoZWPFZixZBVyJJWO1ZjPFZjPFZjy5RYPFZjPVZjZoiXu4pRdZqqdZuqZoeWZ4qYdZyqdpyqQ15rRF9sPFZjQl5rRF9sQ15raIqZP1pnQFtpwY1URmJvPFZjQl5qR2NwR2NxlcHRuYhQPFZjRWJvRmJvRmNwR2Rwk8DQlcLRaYuZZ4qZk7/Oy5RYbYaNdZqpk8HQk8HRzJNYdJqodZuplMDQlMHRuolQu4lRu4pRbYiRuolRPFZjjLbFjbbFjbfHzJRXPFZjPVZjRlphSFtiSl9nb2xgb21girTDi7TDlbzIl77JnH9bnoBcoM7doc/fotDgpdTlp9fnqNfnqdjpqdnpqtrrq9zsrNztrN3urd7vruDxr+HysOLysOLzseP0seT0s+b3tOb3tOf4wo1UxZBVxpNbx5BVypJWypNXy5NYzJNXzJVYzJVZzZRYzZVZzpZZz5da0Jda0Jha0Jhbjbx6JwAAAFp0Uk5TAAIIDxUkJjtPUFFSU1ReZWdtc3V2eIaIk56mqKqst77J1tvf4OHh4uLl5enp6urq6+zt7vDx8vLy8vLy8/Pz8/P09fb39/f5+fn5+fr6+/v8/Pz9/f7+/v7+0MbZ8QAAAYlJREFUOMt9kudTwkAQxaMUGzYUCwoIdgW72FCx94YGdbE3FCtixbPXHPF/9mAyIQXzvmQu+5vb3XePojhpDRaby2WzGLRUIqlNwMukltd1jmiFdnfR0a9DJymrjORv/8xKIHjm357uIwejSgSQOt0cQjHdBw+HyTVG0f0ArSOI1+2JtwNA0EXtAHoQCRQ+99LgiE9K5q9BIoWPh8gu/P4AjddiAN0ctQNoOMAAUIekuhgH0HOABWBOBtztAJg5wAZ0SAY8+GmwcoAL7Eiu005wKgJBNw8kbIEC8RZkyFk5sBwfkqxZKwcEaxKjmqRGocveuFFRq6ulwKLA6thjDYjruz5YLxY9d0u3sL63AXCwmiwOTDk/x9WYD2CLYTIkkauvmiR+hCYqG8hh8zmCyxKE1m6PhXZtn2U/MC78N/bZPY9PbwzDlIiSq9GbrU6n1awn+yel5M+/YIyLqP+VuvDFMLhAgcjxvLL4u1SBSPf8MBGcp0Dket7ZyKgCQGUtff5OKQFUWkVb5h/FpOpcw1WrOgAAAABJRU5ErkJggg==') !important;
    background-repeat: no-repeat;
    background-position: 3px;
margin-top: -13px !important;
background-size: 35px 29px;
margin-right: -22px;
    }
    .search-bar,
    .search-bar input:focus,
    .search-bar input:valid {
        width: 100%;
cursor:text;
    }
    .search-bar input:focus,
    .search-bar input:not(:focus) + .search-btn:focus {
        outline: transparent;
    }
    .search-bar {
        margin: auto;
        padding: 0.2px;
        justify-content: center;
        max-width: 30em;
    }
    .search-bar input {
        opacity: 1;
        border-radius: 1.5em;
        transform: translate(0.5em, 0.5em) scale(0.5);
        /* transform-origin: 100% 0; */
        -webkit-appearance: none;
        -moz-appearance: none;
        appearance: none;
            border: white;
    }
    .search-bar input::-webkit-search-decoration {
        -webkit-appearance: none;
    }
    .search-bar input:focus,
    .search-bar input:valid {
         background-image:url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAMAAABEpIrGAAAAA3NCSVQICAjb4U/gAAAACXBIWXMAAADhAAAA4QFwGBwuAAAAGXRFWHRTb2Z0d2FyZQB3d3cuaW5rc2NhcGUub3Jnm+48GgAAAapQTFRF////gICAv4BgRFVmPVVhOVVjPFdltoZSuIRNuYZQt4RPxIxUPVZiuYlSuIVPnXlTuohPu4hQPFZkxI5TuoZQPFVkwo1Uw49Uxo5VsoZWPFZixZBVyJJWO1ZjPFZjPFZjy5RYPFZjPVZjZoiXu4pRdZqqdZuqZoeWZ4qYdZyqdpyqQ15rRF9sPFZjQl5rRF9sQ15raIqZP1pnQFtpwY1URmJvPFZjQl5qR2NwR2NxlcHRuYhQPFZjRWJvRmJvRmNwR2Rwk8DQlcLRaYuZZ4qZk7/Oy5RYbYaNdZqpk8HQk8HRzJNYdJqodZuplMDQlMHRuolQu4lRu4pRbYiRuolRPFZjjLbFjbbFjbfHzJRXPFZjPVZjRlphSFtiSl9nb2xgb21girTDi7TDlbzIl77JnH9bnoBcoM7doc/fotDgpdTlp9fnqNfnqdjpqdnpqtrrq9zsrNztrN3urd7vruDxr+HysOLysOLzseP0seT0s+b3tOb3tOf4wo1UxZBVxpNbx5BVypJWypNXy5NYzJNXzJVYzJVZzZRYzZVZzpZZz5da0Jda0Jha0Jhbjbx6JwAAAFp0Uk5TAAIIDxUkJjtPUFFSU1ReZWdtc3V2eIaIk56mqKqst77J1tvf4OHh4uLl5enp6urq6+zt7vDx8vLy8vLy8/Pz8/P09fb39/f5+fn5+fr6+/v8/Pz9/f7+/v7+0MbZ8QAAAYlJREFUOMt9kudTwkAQxaMUGzYUCwoIdgW72FCx94YGdbE3FCtixbPXHPF/9mAyIQXzvmQu+5vb3XePojhpDRaby2WzGLRUIqlNwMukltd1jmiFdnfR0a9DJymrjORv/8xKIHjm357uIwejSgSQOt0cQjHdBw+HyTVG0f0ArSOI1+2JtwNA0EXtAHoQCRQ+99LgiE9K5q9BIoWPh8gu/P4AjddiAN0ctQNoOMAAUIekuhgH0HOABWBOBtztAJg5wAZ0SAY8+GmwcoAL7Eiu005wKgJBNw8kbIEC8RZkyFk5sBwfkqxZKwcEaxKjmqRGocveuFFRq6ulwKLA6thjDYjruz5YLxY9d0u3sL63AXCwmiwOTDk/x9WYD2CLYTIkkauvmiR+hCYqG8hh8zmCyxKE1m6PhXZtn2U/MC78N/bZPY9PbwzDlIiSq9GbrU6n1awn+yel5M+/YIyLqP+VuvDFMLhAgcjxvLL4u1SBSPf8MBGcp0Dket7ZyKgCQGUtff5OKQFUWkVb5h/FpOpcw1WrOgAAAABJRU5ErkJggg==') !important;
    background-repeat: no-repeat;
    background-position: 3px;
    box-shadow: 0 1px 6px 0 rgba(32,33,36,0.28);
    transform: translate(-2em, 0em) scale(0.6);
    border: rgba(223,225,229,0) !important;
    background-position: 14px;
   margin-top: 2px !important;
   text-align:center;
   width: 150% !important;    font-family: bitrefill, -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol";
}
    .abs:hover, .abs:focus {

    background-color: transparent !important;

}
    
    /* Active state */
    .search-bar input:focus + .search-btn,
    .search-bar input:valid + .search-btn {
        background: #2762f3;
        border-radius: 0 0.375em 0.375em 0;
        transform: scale(1);
    }
    .search-bar input:focus + .search-btn:before,
    .search-bar input:focus + .search-btn:after,
    .search-bar input:valid + .search-btn:before,
    .search-bar input:valid + .search-btn:after {
        opacity: 1;
    }
    .search-bar input:focus + .search-btn:hover,
    .search-bar input:valid + .search-btn:hover,
    .search-bar input:valid:not(:focus) + .search-btn:focus {
        background: #0c48db;
    }
    .search-bar input:focus + .search-btn:active,
    .search-bar input:valid + .search-btn:active {
        transform: translateY(1px);
    }
</style>
  <!-- mainlinks -->
  <link data-rh="true" rel="stylesheet" href="{{ asset('public/src/css/main.css') }}" />
  <script src="{{asset('public/src/js/translations.js')}}"></script>


    <!-- recaptcha -->
    @yield('css')
</head>
<body translate="no">




@if (!Auth::guest())



  <!-- Header -->
  <div class="l-1t7fvih">
    <div class="l-lqxlj7">
      <header class="l-9ho4sz">
        <a class="l-18vfbgj" href="{{ url('/') }}">
          <img src="{{ asset('public/src/site_images/egify.png') }}" loading="lazy" layout="fixed" width="50"/><span style="font-weight: bold;color:#b7a541">DEMOSITE</span></a>
        <nav class="l-klig6j-3">
          <input type="checkbox" name="toggle" id="toggle" class="l-a2tilk-3" /><label for="toggle"
            class="l-bet8hv-3"></label><label for="toggle" class="l-1x2gi9h">
            <div class="l-1uc3g14">
              <span></span><span></span><span></span>
            </div>
            <span class="l-brqscu-3">Menu</span>
          </label>
          <ul class="l-2ck3p5">
            <label for="toggle" class="l-6lfw9q">×</label>
            <div class="l-9qo9k4-3">
              <li class="l-1cyq5nx-3">Account</li>
              <li class="l-1vcr3xk" style = "padding:8px 2px!important;position:relative"><a style = "padding:8px 2px!important" href="{{ url('/purchase_history') }}"> <img style="width: 17px;margin-bottom: -2px;" src="{{ asset('public/src/site_images/profile.png') }}" alt=""> {{ Auth::user()->username }}
                    @if(ClientHomeController::notificationOrderDispatch()>0)
                    <div style="height: 10px;width: 10px;border-radius: 50%;background: red;position:absolute;top:15px;left:15px"></div>
                    @endif
              </li>
              <li class="l-1vcr3xk" style = "padding:8px 2px!important"><a style = "padding:8px 2px!important" href="{{ url('/account') }}"> <span style="font-size: 13px;color:green"> ( Balance : <span id = "user_balance"> {{Auth::user()->balance}} </span> $ ) </span></a> <input type="hidden" id = "user_btc_balance" value="{{Auth::user()->balance}}"></li>
                                   
            </div>
             
            <div class="l-ihvqmn-3">
              <li class="l-1cyq5nx-3">Menu</li>
              <li class="l-1vcr3xk abs">
                
               <form action="" class="search-bar">
	       <input type="search" name="search" pattern=".*\S.*" required style="text-rendering: geometricPrecision;font-size: 16px;" onkeyup="matchProducts(this)">
	
               </form>
             </li>
              <li class="l-1vcr3xk">
                <a href="{{ url('/home') }}">  &nbsp; Browse Products</a>
              </li>
              <li class="l-1vcr3xk">
                <a href="{{ url('/addFund') }}"> <img style="width: 17px;margin-bottom: -2px;" src="{{ asset('public/src/site_images/ddd.png') }}" alt=""> &nbsp; Add Funds</a>
              </li>
              <li class="l-1vcr3xk">
                <a style="position: relative;" href="{{ url('/support') }}"> <img style="width: 17px;margin-bottom: -2px;" src="{{ asset('public/src/site_images/mail.png') }}" alt=""> &nbsp; Support
                    @if(ClientHomeController::notificationUser()>0)
                    <div style="height: 10px;width: 10px;border-radius: 50%;background: red;position:absolute;top:10px;right:5px"></div>
                    @endif
                </a>
              </li>

            </div>
          </ul>
        </nav>
        <li role="button" tabindex="-1" class="l-ks9thb"  >
         <span id = "cart_product_count" style="background:blue;height:20px;width:20px;text-align:center;border-radius:50% ;position:absolute;top:0px;left:30px;z-index:1000000px;color:white;font-size:14px" >0</span>
          <div role="button" tabindex="-1" class="gayab" id="cartb" ></div>
          <div class="l-7vooej" >
            <svg fill="none" height="24" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round"
              stroke-width="2" viewBox="0 0 24 24" width="24">
              <circle cx="9" cy="21" r="1"></circle>
              <circle cx="20" cy="21" r="1"></circle>
              <path d="M1 1h4l2.68 13.39a2 2 0 0 0 2 1.61h9.72a2 2 0 0 0 2-1.61L23 6H6"></path>
            </svg><span class="l-1rydqmw-3">Cart</span>
          </div>
          <div class="gayab" id="carta" style="width:500px!important">
            <div data-amp-bind-hidden="cart.count > 0" class="l-17ye3fu-2" id = "cart_card">
            <table id = "cart_pop_table">
            </table>
              <p class="l-10b1ual">Your cart is empty</p>
              <p class="l-bpj52k">Looks like you haven't added anything to your cart yet</p>
            <!-- cartproduct -->
            <!-- cartproduct -->

            </div>
          </div>
          
        </li>
      </header>
      <div class="l-1euv9up-3"></div>
    </div>
  </div>
<!-- /.header -->
       

    @yield('content')


    <!-- <footer class="l-1jr2f0u-2">
  <div class="l-qf3abr-2">
    <ul class="l-mpvvpp">
      <li class="l-1klnwrd-2">
        <h4 class="l-1lzdnbh-2 eqn5xv85">Color Scheme</h4>
      </li>
      <li class="l-jdm6zm-2">
        <p class="l-j4uldl-2 eqn5xv80">Light</p><label class="l-17u25cp-2"><input type="checkbox" class="l-5e422y-2">
          <div class="l-1df3nkt-2"></div>
        </label>
        <p class="l-j4uldl-2 eqn5xv80">Dark</p>
      </li>
    </ul>
    <div class="l-yxt8nl">
      <ul class="l-mpvvpp">
        <li class="l-1klnwrd-2">
          <h4 class="l-1lzdnbh-2 eqn5xv85">Questions?</h4>
        </li>
        <li class="l-kkfjuq-2"><a href="https://www.bitrefill.com/faq/?hl=en" target="_self" rel="noopener" class="l-jzy0ak-2"><span class="l-lzm8kh-2">FAQ</span></a></li>
        <li class="l-kkfjuq-2"><a href="https://www.bitrefill.com/contact/?hl=en" target="_self" rel="noopener" class="l-jzy0ak-2"><span class="l-lzm8kh-2">Contact Form</span></a></li>
        <li class="l-kkfjuq-2">
          <a href="https://www.bitrefill.com/reviews/?hl=en" target="_self" rel="noopener" class="l-jzy0ak-2"><span class="l-lzm8kh-2">Reviews</span></a></li>
        <li class="l-kkfjuq-2"><a href="https://www.bitrefill.com/videos/?hl=en" target="_self" rel="noopener" class="l-jzy0ak-2"><span class="l-lzm8kh-2">Video Tutorials</span></a></li>
        <li class="l-kkfjuq-2"><a href="mailto:support@bitrefill.com" target="_self" rel="noopener" class="l-jzy0ak-2"><span class="l-lzm8kh-2">support@bitrefill.com</span></a></li>
      </ul>
      <ul class="l-mpvvpp">
        <li class="l-1klnwrd-2">
          <h4 class="l-1lzdnbh-2 eqn5xv85">Social</h4>
        </li>
        <li class="l-kkfjuq-2"><a href="https://blog.bitrefill.com/" target="_self" rel="noopener" class="l-jzy0ak-2"><span class="l-lzm8kh-2">Blog</span></a></li>
        <li class="l-kkfjuq-2"><a href="https://twitter.com/bitrefill" target="_self" rel="noopener" class="l-jzy0ak-2"><span class="l-lzm8kh-2">Twitter</span></a></li>
        <li class="l-kkfjuq-2"><a href="https://t.me/bitrefill" target="_self" rel="noopener" class="l-jzy0ak-2"><span class="l-lzm8kh-2">Telegram</span></a></li>
        <li class="l-kkfjuq-2"><a href="https://facebook.com/bitrefill" target="_self" rel="noopener" class="l-jzy0ak-2"><span class="l-lzm8kh-2">Facebook</span></a></li>
        <li class="l-kkfjuq-2"><a href="https://www.reddit.com/r/Bitrefill/" target="_self" rel="noopener" class="l-jzy0ak-2"><span class="l-lzm8kh-2">Reddit</span></a></li>
        <li class="l-kkfjuq-2"><a href="https://www.instagram.com/bitrefill/" target="_self" rel="noopener" class="l-jzy0ak-2"><span class="l-lzm8kh-2">Instagram</span></a></li>
        <li class="l-kkfjuq-2"><a href="https://www.bitrefill.com/newsletter/?hl=en" target="_self" rel="noopener" class="l-jzy0ak-2"><span class="l-lzm8kh-2">Email Newsletter</span></a></li>
        <li class="l-kkfjuq-2"><a href="https://www.bitrefill.com/press-resources/?hl=en" target="_self" rel="noopener" class="l-jzy0ak-2"><span class="l-lzm8kh-2">Press Resources</span></a></li>
      </ul>
      <ul class="l-mpvvpp">
        <li class="l-1klnwrd-2">
          <h4 class="l-1lzdnbh-2 eqn5xv85">Live on crypto</h4>
        </li>
        <li class="l-kkfjuq-2"><a href="https://www.bitrefill.com/refills/?hl=en" target="_self" rel="noopener" class="l-jzy0ak-2"><span class="l-lzm8kh-2">Refill phones</span></a></li>
        <li class="l-kkfjuq-2"><a href="https://www.bitrefill.com/buy/lightning/?hl=en" target="_self" rel="noopener" class="l-jzy0ak-2"><span class="l-lzm8kh-2">Lightning</span></a></li>
        <li class="l-kkfjuq-2"><a href="https://www.bitrefill.com/gaming/?hl=en" target="_self" rel="noopener" class="l-jzy0ak-2"><span class="l-lzm8kh-2">Gaming</span></a></li>
        <li class="l-kkfjuq-2"><a href="https://www.bitrefill.com/travel/?hl=en" target="_self" rel="noopener" class="l-jzy0ak-2"><span class="l-lzm8kh-2">Travel</span></a></li>
        <li class="l-kkfjuq-2"><a href="https://www.bitrefill.com/entertainment/?hl=en" target="_self" rel="noopener" class="l-jzy0ak-2"><span class="l-lzm8kh-2">Entertainment</span></a></li>
        <li class="l-kkfjuq-2"><a href="https://www.bitrefill.com/retail/?hl=en" target="_self" rel="noopener" class="l-jzy0ak-2"><span class="l-lzm8kh-2">Shopping</span></a></li>
        <li class="l-kkfjuq-2"><a href="https://www.bitrefill.com/rewards/?hl=en" target="_self" rel="noopener" class="l-jzy0ak-2"><span class="l-lzm8kh-2">Rewards</span></a></li>
      </ul>
      <ul class="l-mpvvpp">
        <li class="l-1klnwrd-2">
          <h4 class="l-1lzdnbh-2 eqn5xv85">Partners</h4>
        </li>
        <li class="l-kkfjuq-2"><a href="https://www.bitrefill.com/refer-a-friend/" target="_self" rel="noopener" class="l-jzy0ak-2"><span class="l-lzm8kh-2">Refer-a-Friend Program</span></a></li>
        <li class="l-kkfjuq-2"><a href="https://careers.bitrefill.com/" target="_blank" rel="noopener" class="l-jzy0ak-2"><span class="l-lzm8kh-2">Careers</span></a></li>
      </ul>
      <ul class="l-mpvvpp">
        <li class="l-1klnwrd-2">
          <h4 class="l-1lzdnbh-2 eqn5xv85">Legal</h4>
        </li>
        <li class="l-kkfjuq-2"><a href="https://www.bitrefill.com/terms/?hl=en" target="_self" rel="noopener" class="l-jzy0ak-2"><span class="l-lzm8kh-2">Terms and Conditions</span></a></li>
        <li class="l-kkfjuq-2"><a href="https://www.bitrefill.com/privacy/?hl=en" target="_self" rel="noopener" class="l-jzy0ak-2"><span class="l-lzm8kh-2">Privacy Policy</span></a></li>
      </ul>
      <ul class="l-mpvvpp">
        <li class="l-1klnwrd-2">
          <h4 class="l-1lzdnbh-2 eqn5xv85">Other languages</h4>
        </li>
        <li class="l-kkfjuq-2"><a href="https://www.bitrefill.com/buy/bitrefill-giftcard-usd/?hl=en" target="_self" rel="noopener" class="l-jzy0ak-2"><span class="l-lzm8kh-2">English</span></a></li>
        <li class="l-kkfjuq-2"><a href="https://www.bitrefill.com/buy/bitrefill-giftcard-usd/?hl=ru" target="_self" rel="noopener" class="l-jzy0ak-2"><span class="l-lzm8kh-2">Русский</span></a></li>
        <li class="l-kkfjuq-2"><a href="https://www.bitrefill.com/buy/bitrefill-giftcard-usd/?hl=fr" target="_self" rel="noopener" class="l-jzy0ak-2"><span class="l-lzm8kh-2">Français</span></a></li>
        <li class="l-kkfjuq-2"><a href="https://www.bitrefill.com/buy/bitrefill-giftcard-usd/?hl=es" target="_self" rel="noopener" class="l-jzy0ak-2"><span class="l-lzm8kh-2">Español</span></a></li>
        <li class="l-kkfjuq-2"><a href="https://www.bitrefill.com/buy/bitrefill-giftcard-usd/?hl=de" target="_self" rel="noopener" class="l-jzy0ak-2"><span class="l-lzm8kh-2">Deutsch</span></a></li>
        <li class="l-kkfjuq-2"><a href="https://www.bitrefill.com/buy/bitrefill-giftcard-usd/?hl=pt" target="_self" rel="noopener" class="l-jzy0ak-2"><span class="l-lzm8kh-2">Português</span></a></li>
        <li class="l-kkfjuq-2"><a href="https://www.bitrefill.com/buy/bitrefill-giftcard-usd/?hl=vi" target="_self" rel="noopener" class="l-jzy0ak-2"><span class="l-lzm8kh-2">tiếng Việt</span></a></li>
        <li class="l-kkfjuq-2"><a href="https://www.bitrefill.com/buy/bitrefill-giftcard-usd/?hl=it" target="_self" rel="noopener" class="l-jzy0ak-2"><span class="l-lzm8kh-2">Italiano</span></a></li>
        <li class="l-kkfjuq-2"><a href="https://www.bitrefill.com/buy/bitrefill-giftcard-usd/?hl=ko" target="_self" rel="noopener" class="l-jzy0ak-2"><span class="l-lzm8kh-2">한국어</span></a></li>
        <li class="l-kkfjuq-2"><a href="https://www.bitrefill.com/buy/bitrefill-giftcard-usd/?hl=zh-Hans" target="_self" rel="noopener" class="l-jzy0ak-2"><span class="l-lzm8kh-2">简体中文</span></a></li>
      </ul>
    </div>
  </div>
</footer> -->


    @endif

    <!-- jQuery 3.1.1 -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.15.1/moment.min.js" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js" crossorigin="anonymous"></script>

    <!-- cart js -->
    <script src="{{asset('public/js/cart.js')}}"></script>
    <script src="{{asset('public/js/getCart.js')}}"></script>

    <script>
          var cartActive =false
       $( ".l-7vooej").click(function() {
         if($( "#cartb" ).class = "gayab"){
               $('#cartb').toggleClass("gayab");
               $('#carta').toggleClass("gayab");
               $( "#cartb" ).toggleClass( "l-67sgjt" );
               $( "#carta" ).toggleClass( "l-u33a9r-2" );
            }
            if(cartActive) {
              $(".l-17ye3fu-2").append('<div class="l-1m1xsmm">'+
                                           '<a class="l-x0qlry-2 e8fmg4a0" href="./buy.html">Browse Products</a>' +
                                       '</div>')
                cartActive = false;
                                      }
            
        });

        $( "#cartb").click(function() {
         if($( "#cartb" ).class != "gayab"){
               $('#cartb').toggleClass("gayab");
               $('#carta').toggleClass("gayab");
               $( "#cartb" ).toggleClass( "l-67sgjt" );
               $( "#carta" ).toggleClass( "l-u33a9r-2" );
            }            
        });
        

        
  </script>


    <!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> -->
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script> -->
    <!-- <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script> -->
    <!-- AdminLTE App -->
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/2.4.3/js/adminlte.min.js"></script> -->

    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/icheck.min.js"></script> -->

    @yield('js')

    @stack('scripts')
</body>
</html>