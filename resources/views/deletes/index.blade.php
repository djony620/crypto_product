@extends('layouts.app')

@section('content')
<section class="content-header">
    <h1 class="pull-left">Delete Messages And Orders</h1>
    <h1 class="pull-right">
    </h1>
</section>
<div class="content">
    <div class="clearfix"></div>


    <div class="clearfix"></div>
    <div class="box box-primary">
        <div class="box-body">
            @if($message)
                <div class="alert-success">
                    {{$message}}
                </div>
             @endif   
                <form action="{{ url('/admin/deletes') }}" method="POST" class="text-center">
                @csrf
                    <h4>Do You Want To Delete All Messages And Orders Just Click On Button</h4>
                    <input type="submit" class="btn btn-danger btn-block" value="Delete All Messages And Orders">
                </form>

        </div>
    </div>
    <div class="text-center">

    </div>
</div>
@endsection