<?php
use \App\Http\Controllers\messagesController;
use \App\Http\Controllers\AdminUserController;
?>

<li class="{{ Request::is('categories*') ? 'active' : '' }}">
    <a href="{{ route('categories.index') }}"><i class="fa fa-tag"></i><span>Categories</span></a>
</li>

<li class="{{ Request::is('countries*') ? 'active' : '' }}">
    <a href="{{ route('countries.index') }}"><i class="fa fa-flag"></i><span>Countries</span></a>
</li>

<li class="{{ Request::is('products*') ? 'active' : '' }}">
    <a href="{{ route('products.index') }}"><i class="fa fa-product-hunt"></i><span>Products</span></a>
</li>

<li class="{{ Request::is('orders*') ? 'active' : '' }}">
    <a href="{{ url('/admin/orders') }}"><i class="fa fa-shopping-cart"></i><span>Orders</span>
    @if(AdminUserController::notificationOrder()>0)
    <div style="height: 10px;width: 10px;border-radius: 50%;background: red;position:absolute;top:10px;left:25px"></div>
    @endif

    </a>
</li>


<li class="">
    <a href="{{ url('/admin/transactions') }}"><i class="fa fa-money"></i><span>Transactions</span></a>
</li>

<li class="">
    <a href="{{ url('/admin/refund') }}"><i class="fa fa-money"></i><span>Refund Requests</span>
        @if(AdminUserController::notificationRefund()>0)
    <div style="height: 10px;width: 10px;border-radius: 50%;background: red;position:absolute;top:10px;left:25px"></div>
    @endif

    </a>
</li>



<li class="">
    <a href="{{ url('/admin/users') }}"><i class="fa fa-user"></i><span>Users</span></a>
</li>

<li class="">
    <a href="{{ url('/admin/change_banners') }}"><i class="fa fa-picture-o"></i><span>Change Banners</span></a>
</li>
