@extends('layouts.app')

@section('content')
<section class="content-header">
    <h1>
        Category
    </h1>
</section>
<div class="content">
    @include('adminlte-templates::common.errors')
    <div class="box box-primary">
        <div class="box-body">
            <div class="container">
            <div class="row">

                <form action="{{ url('/admin/changeBanner1') }}" method="POST" enctype="multipart/form-data">
                @csrf
                    <div class="form-group">
                        <label for="">Home Page Banner (1)</label>
                        <input class="form-control" type="file" name="image">

                        <label for="">Banner Heading</label>
                        <input type="text" class="form-control" name = "heading">

                        <label for="">Banner Text</label>
                        <textarea class="form-control" name="text" id="" rows="10"></textarea>
                    </div>

                    <input type="submit" name="submit" value="Cahnge" class="btn btn-primary">
                </form>

                <form action="{{ url('/admin/changeBanner2') }}" method="POST" enctype="multipart/form-data" style="margin-top: 50px;">
                @csrf
                    <div class="form-group">
                        <label for="">Home Page Banner (2)</label>
                        <input class="form-control" type="file" name="image">

                        <label for="">Banner Heading</label>
                        <input type="text" class="form-control" name = "heading">

                        <label for="">Banner Text</label>
                        <textarea class="form-control" name="text" id="" rows="10"></textarea>
                    <input type="submit" name="submit" value="Cahnge" class="btn btn-primary">

                    </div>
                </form>


                <form action="{{ url('/admin/changeBanner3') }}" method="POST" enctype="multipart/form-data" style="margin-top: 50px;">
                @csrf
                    <div class="form-group">
                        <label for="">Login Page Background</label>
                        <input class="form-control" type="file" name="image">
         <label for="">Login Page Book Mark</label>
                        <input type="text" class="form-control" name = "heading">
              
      </div>
                    <input type="submit" name="submit" value="Cahnge" class="btn btn-primary">
              
                </form>


            </div>
            </div>
        </div>
    </div>
</div>
@endsection