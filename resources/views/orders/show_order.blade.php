@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">Show And Update Order</h1>
        <h1 class="pull-right">
        </h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>


        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body" style="padding: 50px;">
<div class = "container-fluid mt-5">
<h1>Order Comment</h1>
<pre style = "width:100%">{{$data['comment']}}</pre>
</div>


                  @csrf
                  <table class="table">
                      
                    @foreach($data['cart'] as $index0=>$product)
                    @if($product['status'] == 0 || $product['status'] == 1)
                        <tr>
                            <td style="width: 30%;"><img style="height: 150px;" src="{{$product['image']}}" alt=""></td>
                            <td>
                                <h4>{{$product['name']}}</h4>
                                <p>{{$product['rate']}} {{$product['currency']}} * {{$product['qty']}}</p>
                            </td>
                            <td>
                                @foreach($product['redeem_code'] as $index1=>$code)
<textarea name="" class="form-control code" style="width: 100%;padding:10px;font-size:15px;" id="" rows="2">{{$code}}</textarea>
                                 <!--       <input type="text" name="" class="form-control code" value="{{$code}}" style = "height:60px!important"> -->
                                @endforeach

                                <input type = "hidden" style="width: 100%;" class="guide" name="" id="" rows="10" placeholder="Guide To Redeem">
  
                             @if($product['status'] == 0)
                                <button class="btn btn-success" invoice = "{{$data['invoice']}}" slug = "{{$index0}}" onclick="update_redeem(this)">Dispatch</button>
                                @else
                              <button class="btn btn-warning" invoice = "{{$data['invoice']}}" slug = "{{$index0}}" onclick="update_redeem(this)">Dispatch Again</button>
                              
                               
                               @endif
                            </td>
                        
                        </tr>
                    @endif
                    @endforeach
                  </table>


            </div>
        </div>
        <div class="text-center">
        
        </div>
    </div>
@endsection

@section('js')
<script>
let update_redeem = (e)=>{
    // let csrf = document.getElementsName('_token')[0];
alert("Please Wait");
    let invoice = e.getAttribute('invoice');
    let slug = e.getAttribute('slug');
    let redeem_div = e.parentNode;
    let codes_field = redeem_div.querySelectorAll('.code');
    let codes = [];
    let guide = redeem_div.querySelectorAll('.guide')[0].value;
    codes_field.forEach((element,index)=>{
        codes[index] = element.value;
    });

    // let alert_danger = redeem_div.querySelectorAll('.alert_danger')[0];
    // let alert_success = redeem_div.querySelectorAll('.alert-success')[0];
    
    $.ajax({
        url:`http://${window.location.hostname}/admin/updateOrder`,
        type:'get',
        data:{
            // '_token':csrf,
            'invoice':invoice,
            'slug':slug,
            'codes':codes,
            'guide':guide  
        },
        success:function(response){
            if(response.status == 200){
                alert("Order Successfully Dispatched");
                window.location = `http://${window.location.hostname}/admin/orders`;
            }else{
                alert(response.message);
            }
            // console.log(response);
        },
        error:function(error){
            console.log(error);
        }   
    });

}

</script>

@endsection