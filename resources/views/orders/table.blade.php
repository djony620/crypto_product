<div class="table-responsive">
    <table class="table" id="orders-table">
        <thead>
            <tr>
        <th>Client</th>
        <th>Date</th>
        <th>Total</th>
        
        <th>Action</th>
        
                <!-- <th colspan="3">Action</th> -->
            </tr>
        </thead>
        <tbody>
        @foreach($orders as $order)
            <tr>
            
            <td>{{ $order->client_name }}</td>
            <td>{{ $order->created_at }}</td>
            <td>{{ $order->total }}</td>
            
            <td>
              @if($order->notify == 0)
              <a href="{{ url('admin/showOrder/'.$order->invoice) }}" class = "btn btn-success"> Details And Dispatch </a>
              @else
              <a href="{{ url('admin/showOrder/'.$order->invoice) }}" class = "btn btn-danger"> Dispatched </a>
              <!-- <a href="{{ url('admin/dispatchOrder/'.$order->id) }}" class = "btn btn-success"> Dispatch </a> -->
            @endif    
            </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
