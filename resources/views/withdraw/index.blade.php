@extends('layouts.app')

@section('content')
<section class="content-header">
    <h1 class="pull-left">WithDraw Request</h1>
    <h1 class="pull-right">
    </h1>
</section>
<div class="content">
    <div class="clearfix"></div>


    <div class="clearfix"></div>
    <div class="box box-primary">
        <div class="box-body">
            <div class="table-responsive">
                <table class="table" id="orders-table">
                    <thead>
                        <tr>
                            <th>Date</th>
                            <th>User</th>
                            <th>Btc Address</th>
                            <th>amount</th>
                            <th>Current Commission Amount</th>
                            <th>Status</th>
                            <th>Action</th>
                            <!-- <th colspan="3">Action</th> -->
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($requests as $request)
                        <tr>

                            <td>{{ $request->created_at }}</td>
                            <td>{{ $request->username }}</td>
                            <td>{{ $request->btc_address }}</td>
                            <td>{{ $request->amount }}</td>
                            <td>{{ $request->commission }}</td>
                            <td>
                                @if($request->status == 0)
                                <p style="color: red;"> pending </p>
                                @elseif($request->status == 1)
                                <p style="color: green;"> paid </p>

                                @endif
                            </td>
                            <td>

                            @if($request->status == 0)
                            <a href="{{ url('admin/sentAmount/'.$request->id) }}" class="btn btn-success"> sent </a>
                            @elseif($request->status == 1)
                            <button type="button" class="btn btn-warning"> Paid </button>

                                @endif

                        </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>


        </div>
    </div>
    <div class="text-center">

    </div>
</div>
@endsection