@extends('layouts.app')

@section('content')
<section class="content-header">
    <h1 class="pull-left">Change User Password</h1>
    <h1 class="pull-right">
    </h1>
</section>
<div class="content">
    <div class="clearfix"></div>


    <div class="clearfix"></div>
    <div class="box box-primary">
        <div class="box-body">
            <div class="table-responsive">
                <form action="{{ url('admin/updatePassword') }}" method="POST">
                    <h3>User Name : {{$user->name}}</h3> 
                    @csrf
                    <input type="hidden" name = "id" value={{$user->id}}>               
                    <div class="form-group">
                        <label for="">Password</label>
                        <input type="password" name="password" class="form-control">
                        <br>
                        <label for="">Confirm Password</label>
                        <input type="password" name="password_confirmation" class="form-control">
                    </div>
                    <input type="submit" class="btn btn-primary" value="submit" name="submit">
                </form>
        </div>


        </div>
    </div>
    <div class="text-center">

    </div>
</div>
@endsection