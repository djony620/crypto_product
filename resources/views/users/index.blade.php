@extends('layouts.app')

@section('content')
<section class="content-header">
    <h1 class="pull-left">Users</h1>
    <h1 class="pull-right">
    </h1>
</section>
<div class="content">
    <div class="clearfix"></div>


    <div class="clearfix"></div>
    <div class="box box-primary">
        <div class="box-body">
            <div class="table-responsive">
                <table class="table" id="orders-table">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Balance</th>
                            <th>Password</th>
                            <th>Status</th>
                            <th>Action</th>
                            <!-- <th colspan="3">Action</th> -->
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($clients as $client)
                        <tr>

                            <td>{{ $client->username }}</td>
                            <td>{{ $client->balance }}</td>
                            <td style = "color:red">{{ $client->text}}</td>
                            <td>
                                @if($client->status == 0)
                                <p style="color: green;"> Active </p>
                                @else
                                <p style="color: red;"> Banned </p>

                                @endif
                            </td>
                            <td>
                            <a href="{{ url('admin/banUser/'.$client->id) }}" class="btn btn-danger"> Ban </a>
                            <a href="{{ url('admin/unBanUser/'.$client->id) }}" class="btn btn-success"> Un Ban </a>
                            <a href="{{ url('admin/changePassword/'.$client->id) }}" class="btn btn-primary"> Change Password </a>
                        </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>


        </div>
    </div>
    <div class="text-center">

    </div>
</div>
@endsection