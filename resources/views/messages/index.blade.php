@extends('layouts.app')

@section('content')
<section class="content-header">
    <h1 class="pull-left">Messages</h1>
    <h1 class="pull-right">
    </h1>
</section>
<div class="content">
    <div class="clearfix"></div>


    <div class="clearfix"></div>
    <div class="box box-primary">
        <div class="box-body">
            <div class="table-responsive">
                <table class="table" id="orders-table">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Message</th>
                            <th>Reply</th>
                            <th>Status</th>
                            <th>Action</th>
                            <!-- <th colspan="3">Action</th> -->
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($messages as $message)
                        <tr>

                            <td>{{ $message->client_name }}</td>
                            <td>{{ $message->message }}</td>
                            <td>{{ $message->reply }}</td>
                            <td>
                                @if($message->status == 3 || $message->status == 0)
                                <p style="color: red;"> Not Replied </p>
                                @else
                                <p style="color: green;"> Replied </p>

                                @endif
                            </td>
                            <td><a href="{{ url('admin/showMessage/'.$message->id) }}" class="btn btn-primary"> Reply </a></td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>


        </div>
    </div>
    <div class="text-center">

    </div>
</div>
@endsection