@extends('layouts.app')

@section('content')
<section class="content-header">
    <h1 class="pull-left">Messages</h1>
    <h1 class="pull-right">
    </h1>
</section>
<div class="content">
    <div class="clearfix"></div>


    <div class="clearfix"></div>
    <div class="box box-primary">
        <div class="box-body">

           <div class="col-md-6">

                @if($data['message'])

                <div style="margin-top: 60px;border:1px solid silver;padding: 10px;" class="messages">
                    <label for="">Message : </label>{{$data['message']->message}}            
                </div>

                <form action="{{ url('admin/updateMessage') }}" method="POST">
                @csrf
                <input type="hidden" name = "id" value={{$data['message']->id}}>
                @if($data['message']->reply == NULL)
                <textarea class="form-control"  name="reply" id=""  rows="10" placeholder="Reply Him / Her"></textarea>
                @else
                <textarea class="form-control" name="reply" id="" rows="10" placeholder="Reply Him / Her">{{$data['message']->reply}}</textarea>

                @endif

                <input type="submit" class="btn btn-primary" value="Submit">
                </form>
                @endif            

           </div> 

            <div class="col-md-6">
            
            <div style="margin-top: 60px;border:1px solid silver;padding: 10px;" class="messages">
                    @if($data['user_messages'])
                    @foreach($data['user_messages'] as $message)
                <!-- message wrap -->
                        <div style="border:1px solid silver;padding:5px;margin-bottom:50px" class="message-wrap">
                            <div class="message">
                                <h4>User Date:{{ $message->created_at }}</h4>
                                {{$message->message}}
                            </div>
                            <hr>
                            <div class="reply">
                                @if($message->reply == NULL)
                                <h4>You Did't Reply To This Message</h4>    
                                @else
                                <h4>You (Reply) Date:8-6-2020</h4>
                                {{$message->reply}}
                                @endif
                            </div>
                        </div>

                <!-- message wrap -->
                    @endforeach
                    @else
                <!-- message wrap -->
                <div style="border:1px solid silver;padding:5px;margin-bottom:50px;text-align:center" class="message-wrap">
                            No Messages Found
                        </div>

                <!-- message wrap -->

                   @endif 


                    </div>



            </div>
        </div>
    </div>
    <div class="text-center">

    </div>
</div>
@endsection