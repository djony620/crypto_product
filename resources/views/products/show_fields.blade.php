<!-- Image Field -->
<div class="form-group">
    <p>Image</p>
    <img src="{{asset('product_images')}}/{{ $product->image }}" alt="">
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{{ $product->name }}</p>
</div>

<!-- Cat Id Field -->
<div class="form-group">
    {!! Form::label('Category', 'Cat Id:') !!}
    <p>{{ $product->category }}</p>
</div>

<!-- Country Id Field -->
<div class="form-group">
    {!! Form::label('country_id', 'Country Id:') !!}
    <p>{{ $product->country }}</p>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'Description:') !!}
    <p>{{ $product->description }}</p>
</div>


<!-- Currency Field -->
<div class="form-group">
    {!! Form::label('currency', 'Currency:') !!}
    <p>{{ $product->currency }}</p>
</div>

<!-- Prices Field -->
<div class="form-group">
    {!! Form::label('prices', 'Prices:') !!}
    @foreach($product->price as $price)
    <p>{{ $price }} {{ $product->currency }}</p>
    @endforeach
    
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $product->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $product->updated_at }}</p>
</div>

