<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Cat Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cat_id', 'Cat Id:') !!}
    {!! Form::select('cat_id', $categoryItems, null, ['class' => 'form-control']) !!}
</div>

<!-- Country Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('country_id', 'Country Id:') !!}
    {!! Form::select('country_id', $countryItems, null, ['class' => 'form-control']) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('description', 'Description:') !!}
    {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
</div>

<div class="clearfix"></div>

<!-- Currency Field -->
<div class="form-group col-sm-6">
    @if($product)
    <input type="hidden" id = "selected_currency" value="{{$product->currency}}">

    @else
    <input type="hidden" id = "selected_currency" value="">

    @endif
    {!! Form::label('currency', 'Currency:') !!}
    <select name="currency" class="form-control" id="currency">

    </select>
    <!-- {!! Form::select('currency', ['USD' => 'USD', 'PKR' => 'PKR'], null, ['class' => 'form-control']) !!} -->
</div>

<!-- Image Field -->
<div class="form-group col-sm-6">
    {!! Form::label('image', 'Image:') !!}
    {!! Form::file('image') !!}
</div>


<div class="form-group col-sm-12">
    <label for="">Redeem Link</label>
    @if($product)
    <input type="text" class="form-control" name= "redeem_link" value="{{$product->redeem_link}}">

    @else
    <input type="text" class="form-control" name= "redeem_link">

    @endif
</div>

<div class="form-group col-sm-12">
    <label for="">Refund Time <span class="text-danger">(In Minutes)</span></label>
    @if($product)
    <input type="text" class="form-control" name= "refund_time" value="{{$product->refund_time}}">

    @else
    <input type="text" class="form-control" name= "refund_time">

    @endif
</div>



<table class="table" id = "table">
    <thead >
        <th>Prices</th>
        <th>Comment</th>
        <th>Uniq Id To Identify Each Price</th>
        <th>Action</th>
    </thead>
    <tbody>
        
        <tr>
            <td>
              <input type="number" class="form-control prices" name="prices[]" 
                value=<?php 
                if ($product) {
                    if($product->price[0] != null){ 
                        echo $product->price[0];
                    }    
                }
                ?>
                
                >
            </td>
            <td>
            <input type="text" class="form-control" name="comments[]" 
                value="<?php 
                if ($product) {
                    if($product->comment[0] != null){ 
                        echo $product->comment[0];
                    }    
                }
                ?>
                "
                >
            
            </td>

            <td>

            <input type="text" class="form-control delivery_status" name="delivery_status[]" 
                value="<?php 
                if ($product) {
                    if($product->delivery_statuss[0] != null){ 
                        echo $product->delivery_statuss[0];
                    }    
                }else{
                  echo uniqid();
                }
                ?>
                "
                readonly
                >
      
            
            </td>

            <td>
            <button type="button" class="btn btn-success" onclick="addNewRedeem(this)">Add Redeem Code</button> 
             <button type="button" class="btn btn-primary" onclick="addNewPrice()">Add More Prices</button> 
            </td>
        </tr>
        @if($product)

        @foreach($product->redeem_codes[0] as $redeem)         
        <tr>
            <td>
                <textarea type="text" class="form-control" placeholder = "Redeem Code" name="redeem[]">{{$redeem->code}}</textarea>
            </td>
            <td>
                <input type="number" class="form-control" name="status[]" value = 0 readonly>
                <input type="hidden" class="form-control" value = {{$product->price[0]}} name="redeem_price[]">
                <input type="hidden" class="form-control" name="price_delivery_status[]" value="{{$redeem->price_delivery_status}}">
             </td>
             
            <td><button type="button" class="btn btn-danger" onclick="removeRow(this)">x</button></td>
        </tr>
        @endforeach  


        @foreach($product->price as $index=>$price)
            @if($index != 0)
            <tr>
            <td>
               <input type="number" class="form-control prices" name="prices[]" value={{$price}}>
            </td>
            <td>
               <input type="text" class="form-control" name="comments[]" value="{{$product->comment[$index]}}">
            </td>

            <td>
            <input type="text" class="form-control delivery_status" name="delivery_status[]" 
                value="<?php 
                if ($product) {
                    if($product->delivery_statuss[$index] != null){ 
                        echo $product->delivery_statuss[$index];
                    }    
                }
                ?>
                "
                readonly
                >
            </td>

             <td>
               <button type="button" class="btn btn-success" onclick="addNewRedeem(this)">Add Redeem Code</button> 
               <button type="button" class="btn btn-danger" onclick="removeRow(this)">x</button> 
             </td>
            </tr>
            
            @foreach($product->redeem_codes[$index] as $redeem)         
        <tr>
            <td>
                <textarea type="text" class="form-control" placeholder = "Redeem Code" name="redeem[]">{{$redeem->code}}</textarea>
            </td>
            <td>
                <input type="number" class="form-control" name="status[]" value = 0 readonly>
                <input type="hidden" class="form-control" value = {{$product->price[$index]}} name="redeem_price[]">
                <input type="hidden" class="form-control" name="price_delivery_status[]" value="{{$redeem->price_delivery_status}}">
             </td>
             
            <td><button type="button" class="btn btn-danger" onclick="removeRow(this)">x</button></td>
        </tr>
        @endforeach  

            
            @endif
        @endforeach
        @endif
    </tbody>
</table>


<!-- <table class="table" id = "table2">
<thead>
<tr>
<th>Redeem Codes</th>
<th>Price</th>
<th>Status <span style="font-size: 10px!important;"> 0 means avaulable 1 means used </span></th>
<td>
<button type="button" class="btn btn-primary" onclick="addNewRedeem()">Add Redeem Codes</button> 
</td>
</tr>
</thead>

<tbody>

</tbody>
</table> -->

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('products.index') }}" class="btn btn-default">Cancel</a>
</div>
