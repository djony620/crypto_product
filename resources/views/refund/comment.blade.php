@extends('layouts.app')

@section('content')
<section class="content-header">
    <h1 class="pull-left">Reasonn To Decline</h1>
    <h1 class="pull-right">
    </h1>
</section>
<div class="content">
    <div class="clearfix"></div>


    <div class="clearfix"></div>
    <div class="box box-primary">
        <div class="box-body">
            @if(Session::has('message'))
                <h4 class="alert-success">{{Session::get('message')}}</h4>
            @endif

            <form action="{{ url('admin/declineRefundAmountUpdate') }}" method="POST">
            @csrf
            <input type="hidden" name = "invoice" value="{{$refund->invoice}}">
            <input type="hidden" name = "id" value="{{$refund->id}}">
            <input type="hidden" name = "index" value="{{$refund->key}}">
            <textarea class="form-control" name="comment" id=""  rows="10" placeholder="Enter Reason To Decline" required></textarea>
            <input type="submit" class="btn btn-primary" value="Decline">
        </form>

        </div>
    </div>
    <div class="text-center">

    </div>
</div>
@endsection