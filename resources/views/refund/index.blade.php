@extends('layouts.app')

@section('content')
<section class="content-header">
    <h1 class="pull-left">Messages</h1>
    <h1 class="pull-right">
    </h1>
</section>
<div class="content">
    <div class="clearfix"></div>


    <div class="clearfix"></div>
    <div class="box box-primary">
        <div class="box-body">
            @if(Session::has('message'))
                <h4 class="alert-success">{{Session::get('message')}}</h4>
            @endif
            <div class="table-responsive">
                <table class="table" id="orders-table">
                    <thead>
                        <tr>
                            <th>Client Name</th>
                            <th>Product</th>
                            <th>Amount</th>
                            <th>Comment</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($refunds as $refund)
                        <tr>

                            <td>{{ $refund->client_name }}</td>
                            <td>{{ $refund->product['name'] }} ({{ $refund->product['currency'] }} {{ $refund->product['rate'] }} * {{ $refund->product['qty'] }})</td>
                            <td>{{ $refund->amount }}</td>
                            <td>{{ $refund->comment }}</td>
                            <td>
                            <a href="{{ url('admin/refundAmount') }}/{{$refund->id}}/{{$refund->invoice}}/{{$refund->key}}" class="btn btn-success"> Refund Amount </a>
                            <a href="{{ url('admin/declineRefundAmount') }}/{{$refund->id}}" class="btn btn-warning"> Decline Request </a>
                            </td>
                            
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>


        </div>
    </div>
    <div class="text-center">

    </div>
</div>
@endsection