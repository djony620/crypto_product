@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Edit Profile
        </h1>
    </section>
    <div class="content">
        <!-- @include('adminlte-templates::common.errors') -->
        <div class="box box-primary">
            <div class="box-body">
                <div class="container">
                <div class="row">
                    <div class="col-md-6">
                    <form method="post" action="{{url('admin/profileUpdate')}}">
                    @csrf
                    <div class="form-froup ">
                          <label>Name</label>
                          <input type="text" class="form-control" name="name" value = "{{$user->name}}">
                      </div>
                    <div class="form-froup">
                          <label>Username</label>
                          <input type="text" class="form-control" name="username" value = "{{$user->username}}">
                      </div>
                      <div class="form-froup">
                          <label>Email</label>
                          <input type="Email " class="form-control" name="email" value = "{{$user->email}}">
                      </div>
                      <br>
                    <input type="submit" class="btn btn-primary" value="Update">
                </form>    
                </div>

                <div class="col-md-6">
                    
                    <form method="post" action="{{url('/admin/profileUpdatePassword')}}">
                    @csrf   
                    <div class="form-froup ">
                          <label>Password</label>
                          <input type="password" class="form-control" name="password" >
                      </div>
                    <div class="form-froup">
                          <label>Password Confirmation</label>
                          <input type="password" class="form-control" name="password_confirmation">
                      </div>
                      <br>
                      @if($errors->has('password'))
                    
                    <span class="alert" style = "background-color:maroon;color:white; margin-bottom:20px">{{$errors->first('password')}}</span>
                @endif

                    <input type="submit" class="btn btn-primary" value="Update">
                </form>    
                </div>



            </div>
               </div>
            </div>
        </div>
    </div>
@endsection
