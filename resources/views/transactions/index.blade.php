@extends('layouts.app')

@section('content')
<section class="content-header">
    <h1 class="pull-left">Messages</h1>
    <h1 class="pull-right">
    </h1>
</section>
<div class="content">
    <div class="clearfix"></div>


    <div class="clearfix"></div>
    <div class="box box-primary">
        <div class="box-body">
            <div class="table-responsive">
                <table class="table" id="orders-table">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Btc Address</th>
                            <th>Amount</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($transactions as $transaction)
                        <tr>

                            <td>{{ $transaction->client_name }}</td>
                            <td>{{ $transaction->btc_address }}</td>
                            <td>{{ $transaction->amount }}</td>
                            <td>
                                @if($transaction->status == 1)
                                <p style="color: red;"> Pending </p>
                                @else
                                <p style="color: green;"> Confirmed </p>

                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>


        </div>
    </div>
    <div class="text-center">

    </div>
</div>
@endsection