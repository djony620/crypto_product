let getCart = ()=>{
     
    let cart_card = document.getElementById('cart_products');
    
    var grand_total = 0;
    let element = "";
    let mycart = JSON.parse(localStorage.getItem('myCartInstant'));    
    let sending_amount = document.getElementById('sending_amount');
    if(mycart!=null){
        let cartProducts = mycart.products;
        cartProducts.forEach(product=>{ 

            grand_total+= parseFloat(product.rate*product.btc_rate*product.qty);
            let total = parseFloat(product.rate*product.btc_rate*product.qty);
           element+=`
           <div class="l-i9fq99--1"><a href="/buy/bitrefill-giftcard-usd/">
           <div class="l-17nvt0x" style="background: rgb(68, 155, 247); border: 1px solid rgb(163, 178, 194);">
                 <h2>${product.currency}</h2>
            </div>
       </a>
       <div class="l-yhvrli--1">
           <div class="l-1wrjqnn--1">
               <p class="l-a4yqo7">${product.name}</p>
               <div class="l-4u69oq"><span>${product.currency}${product.rate} * ${product.qty}</span><span></span></div>
           </div>
           <div class="l-t3twgy">${total.toFixed(10)} BTC</div>
       </div>
   </div>

           `;
                          
                         
        })
        cart_card.innerHTML = element;
        totalInBTC.innerHTML = grand_total.toFixed(10)+" BTC";
        // sending_amount.value = grand_total;
    }else{
        window.location = `http://${window.location.hostname}/`;
    }   
}

let submit_order_instant = ()=>{
let ordersss = document.getElementById('order');
let blur_back = document.getElementById('blur_back');
let h1 = "<h1>Please Be Patient Order Is In Progress</h1>";
 

    var minutes = 1000 * 60;
    var hours = minutes * 60;
    var days = hours * 24;
    var years = days * 365;
    var d = new Date();
    var t = d.getTime();
    
      let invoice_id = t;

 
    let myCart = JSON.parse(localStorage.getItem('myCartInstant'));
 
    let cart = myCart.products;
    //calculating grand total

    let product = cart[0];
    grand_total = parseFloat(product.rate*product.btc_rate*product.qty);
    
    //calculating grand total

    let order_comment = document.getElementById('order_comment').value;
    let csrf = document.getElementsByTagName('meta')[2].getAttribute('content');
     console.log(cart);   

    if(cart !== undefined){
ordersss.innerHTML = h1;
      $.ajax({
       headers:{
        'X-CSRF-TOKEN': csrf
       }, 
       url:`http://${window.location.hostname}/orderSubmit`,
       type:'post',
       data:{
        'invoice':invoice_id,
        'cart':cart,
        'grand_total':grand_total,
        'order_comment':order_comment  
      },
       success:function(response){
           localStorage.removeItem('myCartInstant');
     //      alert(response.message);
        //   console.log(response);
  blur_back.classList.remove("blur_back");
                blur_back.classList.add("blur_back_active");
          
 window.location = `http://${window.location.hostname}/purchase_history`;
       },
       error:function(error){
           console.log(error);
       }
      
   });
  
  }//undefined

}

getCart();


