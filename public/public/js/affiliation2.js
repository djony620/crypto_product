const toggleBtcAddressForm = () => {
    let btcAddressForm = document.getElementById('btcAddressForm');
    let checkClass = btcAddressForm.classList.contains('d-none');
    if (checkClass) {
        btcAddressForm.classList.remove('d-none');
    } else {
        btcAddressForm.classList.add('d-none');
    }
}

//cahnging rates to btc

const exchangeRate = () => {
        let btc_api = JSON.parse(localStorage.getItem('egify_BTC_rate'));
        let btc_rate = 0;


        // get btc rate
        btc_api.forEach(x => {
            if (x.code === "USD") {
                btc_rate = x.rate_float;
                // btc_rate = parseFloat(1 / btc_rate);
            }
        });
        // get btc rate


        let order_commissions = document.getElementsByName('order_commission');
        order_commissions.forEach((order_commission, index) => {
            let value1 = order_commissions[index].innerHTML;
            order_commissions[index].innerHTML = value1 + "=" + parseFloat(value1 * btc_rate).toFixed(2) + "$"

        })

    }
    //cahnging rates to btc

window.onload = exchangeRate();