let addToCart = (e) => {


    let id = e.value;
    // let slug = e.getAttribute('slug');
    let image = document.getElementById('main_image').getAttribute('src');
    let currency = e.getAttribute('currency');
    let name = e.parentNode.querySelectorAll('.main_title')[0].value;
    let redeem_status = parseFloat(document.getElementById('redeem_status').value);
    let qty = document.getElementById('counter_to_show').innerHTML;
    let prices = e.parentNode.parentNode.parentNode.querySelectorAll('.price');;

    let btc_api = JSON.parse(localStorage.getItem('egify_BTC_rate'));
    let btc_rate = 0;


    // get btc rate
    btc_api.forEach(x => {
        if (x.code === currency) {
            btc_rate = x.rate_float;
            btc_rate = parseFloat(1 / btc_rate);
        }
    });
    // get btc rate


    // alert(qty+"\n"+name+"\n"+currency+"\n"+id);
    let rate = 0;
    let count = 0;
    let delivery_status = 0;
    // console.log(prices);
    prices.forEach((price, index) => {
        if (price.checked) {
            rate = parseFloat(price.value);
            delivery_status = price.getAttribute('delivery_status');
            count++;
        }
    });
    let slug = name + "_" + rate + "_" + delivery_status;

    if (count != 0) {

        //setting cart item
        let mycart = JSON.parse(localStorage.getItem('myCart'));
        if (mycart === undefined || mycart === null) {
            settingUpCart(id, name, slug, rate, currency, qty, btc_rate, image, redeem_status, delivery_status);
        } //cart is empty or not
        else {
            var checkCounter = 0;
            let cartProducts = mycart.products;

            cartProducts.forEach(product => {
                if (product.slug === slug) {
                    product.qty = parseFloat(product.qty) + parseFloat(qty);
                    checkCounter++;
                }
            });
            if (checkCounter == 0) {
                settingUpCartUpdate(id, name, slug, rate, currency, qty, btc_rate, image, redeem_status, delivery_status);
            } //checking if product alredy in cart     
            else {
                localStorage.setItem('myCart', JSON.stringify(mycart));
                let cart = JSON.parse(localStorage.getItem('myCart'));
                console.log(cart);
                cartCount();
                getCart();
            } //checking if product alredy in cart
        } //cart is empty or not


    } else {
        alert('Please Select Price')
    } ///counter is 0

}


let settingUpCart = (id, name, slug, rate, currency, qty, btc_rate, image, redeem_status, delivery_status) => {

    let mycart = {};
    let products = [];
    mycart.products = products;
    rate = parseFloat(rate);
    qty = parseFloat(qty);
    mycart.products.push({
            'id': id,
            'name': name,
            'slug': slug,
            'rate': rate,
            'currency': currency,
            'qty': qty,
            'btc_rate': btc_rate,
            'image': image,
            'redeem_link': "",
            'redeem_code': "",
            'redeem_status': redeem_status,
            'delivery_status': delivery_status
        }

    );

    localStorage.setItem('myCart', JSON.stringify(mycart));
    //setting cart item

    mycart = JSON.parse(localStorage.getItem('myCart'));
    cartCount();
    getCart();


}


let settingUpCartUpdate = (id, name, slug, rate, currency, qty, btc_rate, image, redeem_status, delivery_status) => {

    let mycart = JSON.parse(localStorage.getItem('myCart'));
    rate = parseFloat(rate);
    qty = parseFloat(qty);
    mycart.products.push({
            'id': id,
            'name': name,
            'slug': slug,
            'rate': rate,
            'currency': currency,
            'qty': qty,
            'btc_rate': btc_rate,
            'image': image,
            'redeem_link': "",
            'redeem_code': "",
            'redeem_status': redeem_status,
            'delivery_status': delivery_status

        }

    );

    localStorage.setItem('myCart', JSON.stringify(mycart));
    //setting cart item

    mycart = JSON.parse(localStorage.getItem('myCart'));
    console.log(mycart);
    cartCount();
    getCart();


}


let checkProduct = () => {

    mycart = JSON.parse(localStorage.getItem('myCart'));
    cartProducts = mycart.products;
    if (mycart != null) {
        cart_btns = document.getElementsByClassName('cart-btn');
        Array.from(cart_btns).forEach(cart_btn => {
            cartProducts.forEach(cartProduct => {
                if (cart_btn.value === cartProduct.id) {
                    cart_btn.textContent = "Remove";
                    cart_btn.setAttribute('onclick', 'removeFromCart(this)');
                    cart_btn.setAttribute('slug', cartProduct.slug);
                    cart_btn.classList.remove('btn-add-cart');
                    cart_btn.classList.add('btn-rem-cart');

                }
            });
        });
    }
}

let cartCount = () => {

    mycart = JSON.parse(localStorage.getItem('myCart'));
    if (mycart === null) {
        let total_qty = 0;
    } else {
        let cart_count = document.getElementById('cart_product_count');
        let total_qty = 0;
        let cartProducts = mycart.products
        cartProducts.forEach(product => {
            total_qty += product.qty;
        });
        cart_product_count.innerHTML = total_qty;
        // console.log(total_qty);
        // alert(total_qty);
    }

}

let removeFromCart = (e) => {
    let slug = e.getAttribute('slug');
    let mycart = JSON.parse(localStorage.getItem('myCart'));

    if (mycart != null) {
        mycart.products.forEach((product, index) => {
            if (product.slug == slug) {
                mycart.products.splice(index, 1);
                // alert(index);                
            }
        });
    }
    localStorage.setItem('myCart', JSON.stringify(mycart));
    mycart = JSON.parse(localStorage.getItem('myCart'));
    getCart();
    cartCount();
}

let areaCharges = (e) => {
    let charges = parseFloat(e.options[e.selectedIndex].value);
    let net_total = document.getElementById('net_total');
    let id = parseFloat(e.options[e.selectedIndex].getAttribute('a_id'));
    let area_id = document.getElementById('area_id');
    let grand_total = parseFloat(document.getElementById('cart_total').innerHTML);

    if (grand_total > 500) {
        charges = 0;
    }
    let net_amount = grand_total + charges;
    console.log(net_amount);
    area_id.value = id;
    let delivery = document.getElementById('delivery');


    delivery.innerHTML = charges;
    net_total.innerHTML = net_amount;
}

let btcRate = () => {
    let data = "";
    let btc_rate = 0;
    let user_balance = parseFloat(document.getElementById('user_balance').innerHTML);
    let user_balance_span = document.getElementById('user_balance');

    fetch('https://api.coindesk.com/v1/bpi/currentprice.json?fbclid=IwAR22QK7ER4YKxa5Kla7u0bPkiMxC_K2iXWQQGsO4XlON6-wK54gZuE9Z6ec')
        .then(function(response) {
            if (response.status === 200) {
                response.json().then(function(data) {
                    data = Object.values(data.bpi);
                    localStorage.setItem('egify_BTC_rate', JSON.stringify(data));

                    let btc_api = JSON.parse(localStorage.getItem('egify_BTC_rate'));
                    btc_api.forEach(x => {
                        if (x.code == "USD") {
                            btc_rate_usd = x.rate_float;
                            user_balance = user_balance * btc_rate_usd;
                            user_balance_span.innerHTML = user_balance.toFixed(2);
                        } //if
                    }); //foreach

                }); //promiss
            } //status==200 
        }); //promiss


}


cartCount();


let pendingPayments = () => {
    let csrf = document.getElementsByTagName('meta')[2].getAttribute('content');

    $.ajax({
        url: `http://${window.location.hostname}/pendingAddresses`,
        type: 'POST',
        data: {
            '_token': csrf,
        },
        success: function(response) {
            console.log(response);
            response.forEach(x => {
                let address = x.btc_address;
                let payment_code = x.payment_code;
                updatingCheckingTrans(address, payment_code);
            });
            if (transaction_message !== undefined) {
                transaction_message.innerHTML = "Transactions updated Now Refresh Page, Please click on balance twice if transaction on site keep showing pending";
                transaction_message.setAttribute('style', 'background:green;color:white;padding:10px')
            }

        },
        error: function(error) {
            console.log(error)
        }

    });

}


let updatingCheckingTrans = (address, payment_code) => {

    let transaction_message = document.getElementById('transaction_message');

    $.ajax({
        url: `https://api.bitaps.com/btc/v1/payment/address/state/${address}`,
        type: 'GET',
        success: function(response) {
            console.log(response);
            if (response.transaction_count > 0 && response.pending_transaction_count == 0) {
                if (transaction_message !== undefined) {
                    transaction_message.innerHTML = "Some Transactions Detected";
                    transaction_message.setAttribute('style', 'background:green;color:white;padding:10px')
                }
                let amount = response.received * 0.00000001;
                updateTransaction0(address, amount, 2);

            }
        },
        error: function(error) {
            console.log(error)
        }

    });



}


let updateTransaction0 = (address, amount, status) => {
    let csrf = document.getElementsByTagName('meta')[2].getAttribute('content');

    $.ajax({
        url: `http://${window.location.hostname}/btcCallback`,
        type: 'POST',
        data: {
            '_token': csrf,
            'address': address,
            'amount': amount,
            'status': status

        },
        success: function(response) {
            console.log(response);

        },
        error: function(error) {
            console.log(error)
        }

    });



}


pendingPayments();
window.onload = btcRate();

// checkProduct();