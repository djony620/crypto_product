const toggleBtcAddressForm = () => {
    let btcAddressForm = document.getElementById('btcAddressForm');
    let checkClass = btcAddressForm.classList.contains('d-none');
    if (checkClass) {
        btcAddressForm.classList.remove('d-none');
    } else {
        btcAddressForm.classList.add('d-none');
    }
}



const toggleBtcAddressForm2 = () => {
    let btcAddressForm = document.getElementById('autoSetCommission');
    let checkClass = btcAddressForm.classList.contains('d-none');
    if (checkClass) {
        btcAddressForm.classList.remove('d-none');
    } else {
        btcAddressForm.classList.add('d-none');
    }
}

const checkMode = (e) => {
    let value = e.value;
    let btc_address_field = document.getElementById('btc_address_field');
    if (value == 2) {
        btc_address_field.classList.remove('d-none');
    } else {
        btc_address_field.classList.add('d-none');
    }
}

//cahnging rates to btc

const exchangeRate = () => {
        let btc_api = JSON.parse(localStorage.getItem('egify_BTC_rate'));
        let btc_rate = 0;


        // get btc rate
        btc_api.forEach(x => {
            if (x.code === "USD") {
                btc_rate = x.rate_float;
                // btc_rate = parseFloat(1 / btc_rate);
            }
        });
        // get btc rate

        let usd_btc = document.getElementById('usd_btc');
        usd_btc.innerHTML = (btc_rate * 0.0020).toFixed(2) + "$";

        let total_earned = parseFloat(document.getElementById('total_earned').innerHTML);
        let total_earned_usd = document.getElementById('total_earned_usd');
        total_earned_usd.innerHTML = (total_earned * btc_rate).toFixed(2) + "$";

        let total_commission = parseFloat(document.getElementById('total_commission').innerHTML);
        let total_commission_usd = document.getElementById('total_commission_usd');
        total_commission_usd.innerHTML = (total_commission * btc_rate).toFixed(2) + "$";


        let order_values = document.getElementsByName('order_value');
        let order_commissions = document.getElementsByName('order_commission');
        order_values.forEach((order_value, index) => {
            let value = order_values[index].innerHTML;
            order_values[index].innerHTML = value + "=" + parseFloat(value * btc_rate).toFixed(2) + "$"
            let value1 = order_commissions[index].innerHTML;
            order_commissions[index].innerHTML = value1 + "=" + parseFloat(value1 * btc_rate).toFixed(2) + "$"

        })

    }
    //cahnging rates to btc

window.onload = exchangeRate();