let submit_order = () => {
    let ordersss = document.getElementById('order');
    let blur_back = document.getElementById('blur_back');
    let h1 = "<h1>Please Be Patient Order Is In Progress</h1>";

    let invoice_id = document.getElementById('invoice_id').value;
    let grand_total = document.getElementById('grand_total').value;
    let order_div = document.getElementById('order');
    let myCart = JSON.parse(localStorage.getItem('myCart'));
    let cart = myCart.products;
    let csrf = document.getElementsByTagName('meta')[2].getAttribute('content');
    let order_table = document.getElementById('order_table');
    let order_comment = document.getElementById('order_comment').value;
    let tr = "";

    if (cart !== undefined) {
        ordersss.innerHTML = h1;
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': csrf
            },
            url: `${window.location.protocol}//${window.location.hostname}/orderSubmit`,
            type: 'post',
            data: {
                'invoice': invoice_id,
                'cart': cart,
                'grand_total': grand_total,
                'order_comment': order_comment
            },
            success: function(response) {
                localStorage.removeItem('myCart');
                blur_back.classList.remove("blur_back");
                blur_back.classList.add("blur_back_active");
                // alertify.alert('<h1 style="font-weight:500">Thanks For Ordering</h1>', "<p style='text-align:center;font-weight:500'>Hope you enjoyed</p>" , function(){ alertify.success('Ordered Successfully'); }).set({transition:'slide'});
                // alert(response.message);
                window.location = `${window.location.protocol}//${window.location.hostname}/purchase_history`;

            },
            error: function(error) {
                console.log(error);
            }

        });

    } else {
        alert('cart is empty');
    }


}



//   <img src = "http://ginibrxyrr4dq6q4.onion/public/src/site_images/gs.png">