let filter1 = (e) => {
    let country_id = e.options[e.selectedIndex].value;
    let cat_id = e.options[e.selectedIndex].getAttribute('cat_id');
    let sorting_id = document.getElementById('sorting_id').value;

    let country_field = document.getElementById('country_id');
    country_field.value = country_id;

    let products_section = document.getElementById('products_section');
    let elements = '';
    fetch(`http://${window.location.hostname}/public/getSortedProducts/${cat_id}/${country_id}/${sorting_id}`).then(function(response) {
        if (response.status === 200) {
            response.json().then(function(data) {
                //foreach for displaying data
                data = Object.values(data);
                data.forEach(product => {
                    elements += `
                    <a class="l-13vjo0e-3-3" href="http://${window.location.hostname}/product/${product.id}" style="color: rgb(29, 35, 43);">
                    <div class="l-d6m5lc" style="background-color: rgb(255, 255, 255); color: black;">
                      <div data-fill="true" class="l-xyawq0-3-3">
                          <img src="http://${window.location.hostname}/public/product_images/${product.image}"
                          loading="lazy" class="l-1uv64rf-3" width="350" height="212" alt="${product.name}" layout="responsive"
                          decoding="async" style="background: rgb(255, 255, 255);">
                       </div>
                       <!-- badge -->
                      <!-- <div class="l-7f0b15-3-3">Featured</div> -->
                       <!-- badge -->
      
                    </div>
                    <p class="l-nfokyf-3" style = "margin-bottom:5px">${product.name}</p>
                    <h5 class="" style="color: red;margin-top:0px;font-weight:bold">${product.min_price} ${product.currency}</h5>

                  </a>
                          
                    `;
                });
                //foreach for displaying data
                products_section.innerHTML = elements;
                // console.log(data);
            }).catch(function(error) {
                console.log(error);
            })
        }
    }); //fetch api
    // alert(country_id+"\n"+cat_id+"\n"+sorting_id);
}

let filter2 = (e) => {
    let sorting_id = e.options[e.selectedIndex].value;
    let cat_id = e.options[e.selectedIndex].getAttribute('cat_id');
    let country_id = document.getElementById('country_id').value;

    let sorting_field = document.getElementById('sorting_id');
    sorting_field.value = sorting_id;
    // alert(country_id+"\n"+cat_id+"\n"+sorting_id);
    let products_section = document.getElementById('products_section');
    let elements = '';
    fetch(`/getSortedProducts/${cat_id}/${country_id}/${sorting_id}`).then(function(response) {
        if (response.status === 200) {
            response.json().then(function(data) {
                data = Object.values(data);
                console.log(data);

                //foreach for displaying data
                data.forEach(product => {
                    elements += `
                    <a class="l-13vjo0e-3-3" href="http://${window.location.hostname}/product/${product.id}" style="color: rgb(29, 35, 43);">
                    <div class="l-d6m5lc" style="background-color: rgb(255, 255, 255); color: black;">
                      <div data-fill="true" class="l-xyawq0-3-3">
                          <img src="http://${window.location.hostname}/public/product_images/${product.image}"
                          loading="lazy" class="l-1uv64rf-3" width="350" height="212" alt="${product.name}" layout="responsive"
                          decoding="async" style="background: rgb(255, 255, 255);">
                       </div>
                       <!-- badge -->
                      <!-- <div class="l-7f0b15-3-3">Featured</div> -->
                       <!-- badge -->
      
                    </div>
                    <p class="l-nfokyf-3" style = "margin-bottom:5px">${product.name}</p>
                    <h5 class="" style="color: red;margin-top:0px;font-weight:bold">${product.min_price} ${product.currency}</h5>

                  </a>
                          
                    `;
                });
                //foreach for displaying data
                products_section.innerHTML = elements;
            }).catch(function(error) {
                console.log(error);
            });
        }
    }); //fetch api
}

let matchProducts = (e) => {
    let value = e.value;

    let elements = '';
    fetch(`matchProducts/${value}`).then(function(response) {
        if (response.status === 200) {
            response.json().then(function(data) {
                data = Object.values(data);
                console.log(data);

                //foreach for displaying data
                data.forEach(product => {
                    elements += `
                <a class="l-13vjo0e-3-3" href="http://${window.location.hostname}/product/${product.id}" style="color: rgb(29, 35, 43);">
                <div class="l-d6m5lc" style="background-color: rgb(255, 255, 255); color: black;">
                  <div data-fill="true" class="l-xyawq0-3-3">
                      <img src="http://${window.location.hostname}/public/product_images/${product.image}"
                      loading="lazy" class="l-1uv64rf-3" width="350" height="212" alt="${product.name}" layout="responsive"
                      decoding="async" style="background: rgb(255, 255, 255);">
                   </div>
                   <!-- badge -->
                  <!-- <div class="l-7f0b15-3-3">Featured</div> -->
                   <!-- badge -->
  
                </div>
                    <p class="l-nfokyf-3" style = "margin-bottom:5px">${product.name}</p>
                    <h5 class="" style="color: red;margin-top:0px;font-weight:bold">${product.min_price} ${product.currency}</h5>

              </a>
                      
                `;
                });
                //foreach for displaying data
                products_section.innerHTML = elements;
            }).catch(function(error) {
                console.log(error);
            });
        }
    }); //fetch api


}