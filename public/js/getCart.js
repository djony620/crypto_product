 let domain = window.location.hostname;

 let getCart = () => {


     let cart_card = document.getElementById('cart_card');
     //  let cart_total = document.getElementById('cart_total');
     //  let delivery_charges = parseFloat(document.getElementById('delivery').innerHTML);
     //  let net_total = document.getElementById('net_total');
     let csrf = document.getElementsByTagName('meta')[2].getAttribute('content');
     var grand_total = 0;
     let tr = "";
     let mycart = JSON.parse(localStorage.getItem('myCart'));
     let elements = `
     <!-- cart_product -->
     <div class="l-gi0cnj--3"><a href="/buy/Egify-giftcard-usd/">
             <div class="l-17nvt0x" style="background: rgb(68, 155, 247); border: 1px solid rgb(163, 178, 194);">
                 <img src="" loading="lazy" width="48" height="48" data-fill="true" style="background: rgb(68, 155, 247);"></div>
         </a>
         <div class="l-1yguqne--3">
             <div class="l-1dpe8gk--3">
                 <p class="l-a4yqo7">Egify Balance Card (USD)</p>
                 <div class="l-4u69oq"><span>$20 * 2</span><span></span></div>
             </div>
             <div class="l-t3twgy">0.00209900 BTC</div>
         </div>
     </div>
     <!-- cart_product -->

     `;

     if (mycart != null && mycart.products.length !== 0) {
         let cartProducts = mycart.products;
         cartProducts.forEach(product => {
             grand_total += parseFloat(product.rate * product.btc_rate * product.qty);
             let total = parseFloat(product.rate * product.btc_rate * product.qty);
             tr += `
						      <tr>

                              <td class="image-prod">
                              <div style = "height:50px;width:50px;border-radius:10px;display:flex;justify-content:center;align-items:center;background:skyblue;color:white">
                                ${product.currency}
                              </div>
                              
                          </td>
                          
								<td class="name-prod">
                                    <h5>${product.name}<br>
                                    ${product.currency}${product.rate}<br>
                                    ${total.toFixed(10)} BTC    
                                    </h5>
                                    
								</td>
						        
						        <td class="price"></td>
						        
						        <td class="quantity">`;



             tr += `<div class="input-group mb-3">
			                            <input type = "hidden" class = "slug" value = "${product.slug}">     	
					          	    </div>`;

             // let subtotal = product.weight*parseFloat(product.rate);
             // grand_total+=subtotal;
             // <td class="total">${subtotal}</td>
             tr += `</td>
                                <td>
                                <button style = "width:30px!important;border:none;margin-right:-5px;height:24px" value = "${product.slug}" onclick = "updateCartMinus(this)">-</button>
                                <input style = "width:40px;text-align:center;height:22px;border:1px solid silver;border-radius:none" class = "qty" type = "number" value = "${product.qty}" readonly/>
                                <button style = "width:30px!important;border:none;margin-left:-4px;height:24px" value = "${product.slug}" onclick = "updateCartPlus(this)">+</button>

                                </td>
						         
                                <td style = "text-align:center">
                                    <button class = "bg-danger pointer" style = "color:black!important;margin-left:20px;padding:10px 0px!important;line-height:5px;width:25px;border:none" slug = "${product.slug}" onclick = "removeFromCart(this)">
                                        <strong>x</strong>
                                    </button>
                                </td>
						      </tr>
            
            `;
         });
         tr += `<tr>
            <td colspan = "6">Grand Total : ${grand_total.toFixed(10)} BTC</td>
         </tr>`;
         let Element = `<table style = "width:100%">${tr}</table>
         
            <div style = "padding-top:10px;border-top:1px solid silver;padding-bottom:10px!important">
            <form action = "http://${window.location.hostname}/" method = "GET">
                <input type = "submit" style = " background:transparent;margin-bottom:10px;padding:10px 20px;float:left ;color:skyblue;border:1px solid skyblue" value = "Keep Shopping"/>
            </form>


            <form action = "http://${window.location.hostname}/checkout/payment" method = "POST">
            
                <input type = "hidden" name = "_token" value = "${csrf}"/>  
                <input type = "hidden" name = "grand_total" value = "${grand_total.toFixed(10)}"/>  
                <input type = "submit" style = "background:transparent;margin-bottom:10px;padding:10px 20px;float:right ;color:skyblue;border:1px solid skyblue" value = "Checkout"/>
            </form>
            </div>
         `
         cart_card.innerHTML = Element;
         //  cart_total.innerHTML = grand_total;
         //  net_total.innerHTML = parseFloat(grand_total + delivery_charges);

     } else {
         let element = ` <p class="l-10b1ual">Your cart is empty</p>
        <p class="l-bpj52k">Looks like you haven't added anything to your cart yet</p>
`;
         cart_card.innerHTML = element;
     }
     cartCount();
 }

 // let removeFromCart2 = (e)=>{
 // removeFromCart(e);
 // getCart();    
 // }


 let updateCartPlus = (e) => {
     let mycart = JSON.parse(localStorage.getItem('myCart'));
     let slug = e.value;
     // increasing value
     let qty = e.parentNode.querySelectorAll('.qty')[0];
     let qty_val = qty.value;
     // alert(qty_val)
     qty.value = ++qty_val;
     // increasing value

     let currentQty = e.parentNode.querySelectorAll('.qty')[0].value;



     let cartProducts = mycart.products;

     cartProducts.forEach(product => {
         if (product.slug === slug) {
             product.qty = parseFloat(currentQty);
         }
     });
     localStorage.setItem('myCart', JSON.stringify(mycart));
     // mycart = JSON.parse(localStorage.getItem('myCart'));
     // console.log(mycart);
     getCart();

     // getCart();
 }


 let updateCartMinus = (e) => {
     let mycart = JSON.parse(localStorage.getItem('myCart'));
     let slug = e.value;
     // increasing value
     let qty = e.parentNode.querySelectorAll('.qty')[0];
     let qty_val = qty.value;
     // alert(qty_val)
     qty_val = --qty_val;
     if (qty_val === 0) {
         qty_val = 1
     }
     qty.value = qty_val;
     // increasing value

     let currentQty = e.parentNode.querySelectorAll('.qty')[0].value;



     let cartProducts = mycart.products;

     cartProducts.forEach(product => {
         if (product.slug === slug) {
             product.qty = parseFloat(currentQty);
         }
     });
     localStorage.setItem('myCart', JSON.stringify(mycart));
     // mycart = JSON.parse(localStorage.getItem('myCart'));
     // console.log(mycart);

     getCart();
 }


 getCart();