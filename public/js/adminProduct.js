let addNewPrice = ()=>{
    var minutes = 1000 * 60;
    var hours = minutes * 60;
    var days = hours * 24;
    var years = days * 365;
    var d = new Date();
    var t = d.getTime();
    
    let uniqid = t;

    let table_body = document.getElementById('table').getElementsByTagName('tbody')[0];
    let newRow = table_body.insertRow();
    let newCell1 = newRow.insertCell(0);
    let newCell2 = newRow.insertCell(1);
    let newCell3 = newRow.insertCell(2);
    let newCell4 = newRow.insertCell(3);

    let redeem_button = ``;
    let cencelButton = `
    <button type="button" class="btn btn-success" onclick="addNewRedeem(this)">Add Redeem Code</button>
    <button type="button" class="btn btn-danger" onclick="removeRow(this)">x</button>`; 
    let priceField =  `<input type="number" class="form-control prices" name="prices[]">`;
    let commentField =  `<input type="text" class="form-control" name="comments[]">`;
    let delivery_status =  `
    <input type="text" class="form-control delivery_status" name="delivery_status[]" value = "${uniqid}" readonly>
    `;
    newCell1.innerHTML = priceField;
    newCell2.innerHTML = commentField;
    newCell3.innerHTML = delivery_status;
    newCell4.innerHTML = cencelButton;

}

let removeRow = (e)=>{
    let tr = e.parentNode.parentNode;
    let table_body = e.parentNode.parentNode.parentNode;
    table_body.removeChild(tr);
}

let addNewRedeem = (e)=>{
    let trIndex = e.parentNode.parentNode.rowIndex;
    let tr = e.parentNode.parentNode
    let price = tr.querySelectorAll('.prices')[0].value;
    let delivery_status = tr.querySelectorAll('.delivery_status')[0].value;
    
    // delivery_status = delivery_status.options[delivery_status.selectedIndex].value;
    if(price == "" || price == null || price == undefined){
        alert("Please Enter Price First");
    }else{

        let table_body = document.getElementById('table').getElementsByTagName('tbody')[0];
        let newRow = table_body.insertRow(trIndex);
        let newCell1 = newRow.insertCell(0);
        let newCell2 = newRow.insertCell(1);
        let newCell3 = newRow.insertCell(2);
        
        let cencelButton = `<button type="button" class="btn btn-danger" onclick="removeRow(this)">x</button>`; 
        let priceField =  `<textarea type="text" class="form-control" placeholder = "Redeem Code" name="redeem[]"></textarea>`;
        let commentField =  `<input type="number" class="form-control" name="status[]" value = 0 readonly>
        <input type="hidden" class="form-control" value = ${price} name="redeem_price[]">
        <input type="hidden" class="form-control" value = ${delivery_status} name="price_delivery_status[]">
        `;
        
        newCell1.innerHTML = priceField;
        newCell2.innerHTML = commentField;
        newCell3.innerHTML = cencelButton;
    

    }
    
}

