let pendingPayments1 = () => {

    let csrf = document.getElementsByTagName('meta')[2].getAttribute('content');
    let transaction_message = document.getElementById('transaction_message1');

    $.ajax({
        url: `http://${window.location.hostname}/pendingAddresses`,
        type: 'POST',
        data: {
            '_token': csrf,
        },
        success: function(response) {
            console.log(response);
            response.forEach(x => {
                let address = x.btc_address;
                let payment_code = x.payment_code;
                updatingCheckingTrans1(address, payment_code);                
            });

            setTimeout(function(){window.location = `http://${window.location.hostname}/account`;},20000);

            // if (transaction_message !== undefined) {
            //     transaction_message.innerHTML = "Transactions updated Now Refresh Page, Please click on balance twice if transaction on site keep showing pending";
            //     transaction_message.setAttribute('style', 'background:green;color:white;padding:10px')
            // }

        },
        error: function(error) {
            console.log(error)
        }

    });

}


let updatingCheckingTrans1 = (address, payment_code) => {

    // let transaction_message = document.getElementById('transaction_message1');

    $.ajax({
        url: `https://api.bitaps.com/btc/v1/payment/address/state/${address}`,
        type: 'GET',
        success: function(response) {
            console.log(response);
            if (response.transaction_count > 0 && response.pending_transaction_count == 0) {
                // if (transaction_message !== undefined) {
                //     transaction_message.innerHTML = "Some Transactions Detected";
                //     transaction_message.setAttribute('style', 'background:green;color:white;padding:10px')
                // }
                let amount = response.received * 0.00000001;
                updateTransaction01(address, amount, 2);

            }
        },
        error: function(error) {
            console.log(error)
        }

    });



}


let updateTransaction01 = (address, amount, status) => {
    let csrf = document.getElementsByTagName('meta')[2].getAttribute('content');

    $.ajax({
        url: `http://${window.location.hostname}/btcCallback`,
        type: 'POST',
        data: {
            '_token': csrf,
            'address': address,
            'amount': amount,
            'status': status

        },
        success: function(response) {
            console.log(response);
        },
        error: function(error) {
            console.log(error)
        }

    });
}

let refresh_countdown_active = ()=>{
    setInterval(() => {
        let refresh_countdown = document.getElementById('refresh_countdown');
        let time_out = parseInt(refresh_countdown.innerHTML)
        time_out--;
        if(time_out>=0){
            refresh_countdown.innerHTML = time_out;        
        }else{
            refresh_countdown.innerHTML = "Refreshing";        
        }
    }, 1000);
}
///////////////////////////////////////////////////////////////////////////////
document.getElementById('pending_trans_button').addEventListener('click',()=>{
    let blur_back = document.getElementById('blur_back');
    let refresh_countdown = document.getElementById('refresh_countdown');
    refresh_countdown.innerHTML = "20";
    blur_back.classList.remove('blur_back');
    blur_back.classList.add('blur_back_active');
    refresh_countdown_active();
    pendingPayments1();
})
///////////////////////////////////////////////////////////////////////////////
document.getElementById('pending_trans_button2').addEventListener('click',()=>{
    let blur_back = document.getElementById('blur_back');
    let refresh_countdown = document.getElementById('refresh_countdown');
    refresh_countdown.innerHTML = "20";
    blur_back.classList.remove('blur_back');
    blur_back.classList.add('blur_back_active');
    refresh_countdown_active();
    pendingPayments1();
})
///////////////////////////////////////////////////////////////////////////////

