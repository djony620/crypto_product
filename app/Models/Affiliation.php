<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Affiliation extends Model
{
    public $table = "affiliation";

    public $fillable = [
        'user_id',
        'code',
        'order_value',
        'commission',
    ];

    
}
