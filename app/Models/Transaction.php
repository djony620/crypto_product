<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    public $table = "transactions";

    public $fillable = [
        'user_id',
        'invoice',
        'btc_address',
        'payment_code',
        'amount',
        'status'
    ];

    public $rules = [
        'user_id'=>'required',
        'btc_address'=>'required',
        'trans_hash'=>'required',
        'amount'=>'required'
    ];
}
