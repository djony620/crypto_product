<?php

namespace App\models;

use GuzzleHttp\Psr7\Request;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    public $table = "messages";

    public $fillable = [
        'user_id',
        'message',
        'reply',
        'status'
    ];

    protected $rules = [
        'user_id'=>'required',
        'message'=>'required'
    ];

   
}
