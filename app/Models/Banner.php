<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    public $table = "banners";

    public $fillable = [
        'image',
        'heading',
        'text'
    ];
}
