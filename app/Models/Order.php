<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    public $table = "orders";

    public $fillable = [
        'invoice',
        'client_id',
        'total',
        'cart',
        'comment',
        'notification'
    ];


    protected $casts = [
        'id' => 'integer',
        'invoice' => 'string',
        'client_id' => 'integer',
        'total' => 'double',
        'cart' => 'text',
    ];



    public static $rules = [
        'invoice' => 'required',
        'client_id' => 'required',
        'total' => 'required',
        'cart' => 'required',

    ];


}
