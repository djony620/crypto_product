<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Product
 * @package App\Models
 * @version July 20, 2020, 1:56 pm UTC
 *
 * @property string $name
 * @property integer $cat_id
 * @property integer $country_id
 * @property string $description
 * @property string $image
 * @property string $currency
 * @property string $prices
 */
class Product extends Model
{
    use SoftDeletes;

    public $table = 'products';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'cat_id',
        'country_id',
        'description',
        'image',
        'currency',
        'prices',
        'redeem_code',
        'redeem_link',
        'comments',
        'refund_time',
        'delivery_status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'cat_id' => 'integer',
        'country_id' => 'integer',
        'description' => 'string',
        'image' => 'string',
        'currency' => 'string',
        'prices' => 'string',
        'refund_time'=>'float'

    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'cat_id' => 'required',
        'country_id' => 'required',
        'description' => 'required',
        // 'image' => 'required',
        'currency' => 'required',
        'prices' => 'required'
    ];

    
}
