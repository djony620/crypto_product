<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Withdrawrequest extends Model
{
    public $table = 'withdrawrequests';
    public $fillable = [
        'user_id',
        'btc_address',
        'amount',
        'status'
    ];
}
