<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Refund_request extends Model
{
    public $table = "refund_requests";

    public $fillable = [
        'user_id',
        'invoice',
        'key',
        'product',
        'amount',
        'status',
        'comment',
        'comment_deny',
        'notification'
    ];
}
