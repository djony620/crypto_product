<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Redeem_code extends Model
{
    public $table = "redeem_codes";
    public $fillable = [
        'product_id',
        'code',
        'status',
        'price',
        'price_delivery_status'
    ];


    protected $casts = [
        'product_id'=>'integer',
        'code'=>'string',
        'status'=>'integer'
        
    ];


    public $rules = [
        'product_id'=>'required',
        'code'=>'required',
        'status'=>'required',
        'price'=>'required'
        
    ];
}
