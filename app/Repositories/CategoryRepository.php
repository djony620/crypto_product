<?php

namespace App\Repositories;

use App\Models\Category;
use App\Repositories\BaseRepository;
use Illuminate\Http\Request;

/**
 * Class CategoryRepository
 * @package App\Repositories
 * @version July 18, 2020, 3:22 pm UTC
*/

class CategoryRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'icon_url'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Category::class;
    }

    public function createCategory(Request $request){
    
        if($request->file('icon_url')){
            $file = $request->file('icon_url');
            $fileName = $file->getClientOriginalName();
            $fileExt = $file->getClientOriginalExtension();
            $nameToStore = uniqid().'.'.$fileExt; 
            $file->move(public_path('uploads'),$nameToStore);
    
            $input = $request->all();
            $input['icon_url'] = $nameToStore;
    
            return $this->create($input);
    
        }else{
            $input = $request->all();
            unset($input['icon_url']);
            return $this->create($input);
    
        }
    }

    public function updateCategory(Request $request,$id){
    
        if($request->icon_url){
            $file = $request->file('icon_url');
            $fileExt = $file->getClientOriginalExtension();
            $fileNameToStore = uniqid().'.'.$fileExt;
            $file->move(public_path('uploads'),$fileNameToStore);

            $input = $request->all();
            $input['icon_url'] = $fileNameToStore;
    
            return $this->update($input,$id);

        }else{
            $input = $request->all();
            unset($input['icon_url']);
            return $this->update($input,$id);

        }
    }

}
