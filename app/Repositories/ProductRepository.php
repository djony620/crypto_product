<?php

namespace App\Repositories;

use App\Models\Product;
use App\Repositories\BaseRepository;
use Illuminate\Http\Request;
use App\Models\Redeem_code;

/**
 * Class ProductRepository
 * @package App\Repositories
 * @version July 20, 2020, 1:56 pm UTC
*/

class ProductRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'cat_id',
        'country_id',
        'description',
        'image',
        'currency',
        'prices',
        'comments'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Product::class;
    }

    public function insert_product(Request $request){
        
        if($request->image){
            $file = $request->file('image');
            $fileExt = $file->getClientOriginalExtension();
            $fileNameToStore = uniqid().'.'.$fileExt;
            $file->move(public_path('product_images'),$fileNameToStore);
            $input = $request->all();
            $input['image'] = $fileNameToStore;
            $input['prices'] = serialize($input['prices']);
            $input['comments'] = serialize($input['comments']);
            $input['delivery_status'] = serialize($input['delivery_status']);
            $input['refund_time'] = $input['refund_time']?$input['refund_time']:0;
            $product = $this->create($input);
            if($request->redeem){
                foreach ($request->redeem as $index => $redeem) {
                    $redeem_data = [
                        'product_id'=>$product->id,
                        'code'=> $redeem,
                        'status'=>$request->status[$index],
                        'price'=>$request->redeem_price[$index], 
                        'price_delivery_status'=>$request->price_delivery_status[$index]
                    ];
                    Redeem_code::create($redeem_data);
                }//foreach 
             }
            return $product;  
        }else{
            $input = $request->all();
            
            $input['prices'] = serialize($input['prices']);
            $input['comments'] = serialize($input['comments']);
            $input['delivery_status'] = serialize($input['delivery_status']);
            $input['refund_time'] = $input['refund_time']?$input['refund_time']:0;  
          $product = $this->create($input);
            if($request->redeem){
                foreach ($request->redeem as $index => $redeem) {
                    $redeem_data = [
                        'product_id'=>$product->id,
                        'code'=> $redeem,
                        'status'=>$request->status[$index],
                        'price'=>$request->redeem_price[$index],
                        'price_delivery_status'=>$request->price_delivery_status[$index]   
                    ];
                    Redeem_code::create($redeem_data);
                }//foreach 
             }
            return $product;  

        }
    }

    public function updateProduct(Request $request, $id)
    {
        
        if($request->image){
            $file = $request->file('image');
            $fileExt = $file->getClientOriginalExtension();
            $fileNameToStore = uniqid().'.'.$fileExt;
            $file->move(public_path('product_images'),$fileNameToStore);
            $input = $request->all();
            $input['image'] = $fileNameToStore;
            $input['prices'] = serialize($input['prices']);
            $input['comments'] = serialize($input['comments']);
            $input['delivery_status'] = serialize($input['delivery_status']);
            $input['refund_time'] = $input['refund_time']?$input['refund_time']:0;
            if($request->redeem){
                Redeem_code::where('product_id',$id)->delete();
                foreach ($request->redeem as $index => $redeem) {
                    $redeem_data = [
                        'product_id'=>$id,
                        'code'=> $redeem,
                        'status'=>$request->status[$index],
                        'price'=>$request->redeem_price[$index],
                        'price_delivery_status'=>$request->price_delivery_status[$index]   
                    ];
                    Redeem_code::create($redeem_data);
                }//foreach 
             }

            return $this->update($input,$id);
        }//if getting image
        else{
            $input = $request->all();
            unset($input['image']);
            $input['prices'] = serialize($input['prices']);
            $input['comments'] = serialize($input['comments']);
            $input['delivery_status'] = serialize($input['delivery_status']);
            $input['refund_time'] = $input['refund_time']?$input['refund_time']:0;
            if($request->redeem){
                Redeem_code::where('product_id',$id)->delete();
                foreach ($request->redeem as $index => $redeem) {
                    $redeem_data = [
                        'product_id'=>$id,
                        'code'=> $redeem,
                        'status'=>$request->status[$index],
                        'price'=>$request->redeem_price[$index],
                        'price_delivery_status'=>$request->price_delivery_status[$index]   
                    ];
                    Redeem_code::create($redeem_data);
                }//foreach 
             }

            return $this->update($input,$id);

        }
    }
}
