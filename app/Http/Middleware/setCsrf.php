<?php

namespace App\Http\Middleware;

use Closure;

class setCsrf
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response =  $next($request);

        $response->headers->set('x-csrf-token', csrf_token());

        return $response;
    }
}
