<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Product;
use App\Models\Country;
use App\Models\Order;
use App\Client;
use App\Models\Banner;
use App\Models\Message;
use App\Models\Redeem_code;
use App\Models\Refund_request;
use App\Models\Transaction;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Laracasts\Flash\Flash;

//affiliation uses
use App\Models\Affiliation;
use App\Models\Withdrawrequest;
//affiliation uses


// use Symfony\Component\HttpFoundation;
class ClientHomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:client');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    private function checkBanned(){
        $user_id = Auth::id();      
        $status = Client::find($user_id)->status;
        if($status == 1){
            echo "<h1 style = 'color:red'>You Are Temporarily Banned</h1>";
            die();
        }

    }


    public function index()
    {

        $this->checkBanned();
        $products = Product::inRandomOrder()->limit(20)->get();;
        $countries = Country::all();
        $categories = Category::all();
  
          foreach($products as $key=>$product){
            $prices = unserialize($product->prices);
            $min_price = min($prices);
            $products[$key]->min_price = $min_price;
        }

           
        $data = [
           'products'=>$products,
           'countries'=>$countries,
           'categories'=>$categories, 
           'cat_name'=>"",
           'cat_id'=>0,
           'country_id'=>0,
           'sorting_id'=>2
        ];
        return view('Client/home',compact('data'));
    }


    public function homeCategory($id)
    {
        $this->checkBanned();

        $products = Product::where('cat_id',$id)->orderby('id','DESC')->get();
        $countries = Country::all();
        $categories = Category::all();
        $category_name = Category::where('id',$id)->get();

        foreach($products as $key=>$product){
            $prices = unserialize($product->prices);
            $min_price = min($prices);
            $products[$key]->min_price = $min_price;
        }


           
        $data = [
           'products'=>$products,
           'countries'=>$countries,
           'categories'=>$categories, 
           'cat_name'=>$category_name[0]->name,
           'cat_id'=>$category_name[0]->id,
           'country_id'=>0,
           'sorting_id'=>2

        ];
        return view('Client/home',compact('data'));
    }



    public function showProduct($id){
        $this->checkBanned();

        $product = Product::find($id);
        $price  = unserialize($product->prices);
        $product->price = $price;

        $comments = unserialize($product->comments);
        $product->comment = $comments;

        $delivery_status = unserialize($product->delivery_status);
        $product->delivery_statuss = $delivery_status;

        $redeems = Redeem_code::where(['product_id'=>$product->id,'status'=>0])->get();
        $redeem_length = sizeof($redeems);
        //getting products that whis has no redeem registered
        $actual_redeems = Redeem_code::where(['product_id'=>$product->id])->get();
        $actual_redeem_length = sizeof($actual_redeems);
        //getting products that whis has no redeem registered


        //getting related products
        $category_id = $product->cat_id;
        $products = Product::where('cat_id',$category_id)->orderby('id','DESC')->limit(6)->get();
        //getting category name
        $cat_name = Category::where('id',$category_id)->get();
        $cat_name = $cat_name[0]->name;
        $data = [
            'product'=>$product,
            'products'=>$products,
            'cat_name'=>$cat_name,
            'redeem_length'=>$redeem_length,
            'actual_redeems'=>$actual_redeem_length
        ];  
        return view('Client/product',compact('data'));

    }

    // public function checkOutContact(){
    //     return view('Client/checkout');
    // }

    // //check step 1
    // public function checkOutPayMethod(Request $request){
    //     $rules = [
    //         'email'=>'required|email'
    //     ];
    //     $validate = Validator::make($request->all(),$rules);
    //     if($validate->fails()){
    //         return redirect()->back();
    //     }else{
    //        $email = $request->email; 
    //     return view('Client/checkout2',compact('email'));
    //     }
    // }

    // public function checkOutPayMethod(){
    //     return view('Client/checkout2');
    // }

    public function checkOutPayment(Request $request){
        $this->checkBanned();


        $client_id = Auth::id();
        $client = Client::find($client_id);
        $balance = $client->balance;
        $rules = [
            'grand_total'=>'required'
        ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return redirect()->back();
        }else{
         
        $invoice_id = time().'_'.uniqid();
        $grand_total = $request->grand_total;
        $data = [
            'invoice_id'=>$invoice_id,
            'grand_total'=>$grand_total,
        ]; 

        if($balance>=(double)$grand_total && $balance > 0){
            return view('Client/checkout3',compact('data'));

        }else{
            return view('Client/checkout');
        }
        }


        // return view('');
    }


    public function checkOutPaymentInstant(){
        $this->checkBanned();

            return view('Client/checkout2');

    }


    public function orderComment($invoice){
        if($invoice){
            return view('Client/checkout2',compact('invoice'));
        }else{
            return redirect()->back();
        }
    }

    public function orderCommentUpdate(Request $request){
        $invoice = $request->invoice;
        $user_id = Auth::id();
        $check = Order::where([
            'client_id'=>$user_id,
            'invoice'=>$invoice
        ])->get();
        $check=sizeof($check);
        if($check == 1){
            $orderUpdate = Order::where('invoice',$invoice)->first()->update(['comment'=>$request->comment]);
            echo '<scrip>alert("Message Has Been Submitted")</script>';
            return redirect('/purchase_history');
        }else{
            echo '<scrip>alert("There Is No Record Againt This User Or Invoice")</script>';
            return redirect('/purchase_history');
            
        }
    }

public function matchProducts($value){
    $this->checkBanned();

    if($value != ""){
        $product = Product::where('name','LIKE','%'.$value.'%')->get();

    }else{
        $product = Product::where('name','!=','')->get();

    }
    
        foreach($product as $key=>$produc){
            $prices = unserialize($produc->prices);
            $min_price = min($prices);
            $product[$key]->min_price = $min_price;
        }

    return response()->json($product,200);
}

    public function sortProducts($cat_id,$country_id,$sorting_id){
        if($country_id != 0 && $sorting_id != 2 && $cat_id !=0 ){
            $products = Product::where([
                'country_id'=>$country_id,
                'cat_id'=>$cat_id
                ])->orderby('id','ASC')->get();
                
                        foreach($products as $key=>$product){
            $prices = unserialize($product->prices);
            $min_price = min($prices);
            $products[$key]->min_price = $min_price;
        }

                
                return response()->json($products,200);
        }elseif($country_id != 0 && $sorting_id == 2 && $cat_id ==0){
            $products = Product::where([
                'country_id'=>$country_id,
                ])->orderby('id','DESC')->get();

        foreach($products as $key=>$product){
            $prices = unserialize($product->prices);
            $min_price = min($prices);
            $products[$key]->min_price = $min_price;
        }

                return response()->json($products,200);

        }elseif($country_id != 0 && $sorting_id != 2 && $cat_id ==0){
            $products = Product::where([
                'country_id'=>$country_id,
                ])->orderby('id','ASC')->get();

        foreach($products as $key=>$product){
            $prices = unserialize($product->prices);
            $min_price = min($prices);
            $products[$key]->min_price = $min_price;
        }

                return response()->json($products,200);
        }elseif($country_id != 0 && $sorting_id == 2 && $cat_id !=0){
            $products = Product::where([
                'country_id'=>$country_id,
                'cat_id'=>$cat_id
                ])->orderby('id','DESC')->get();

        foreach($products as $key=>$product){
            $prices = unserialize($product->prices);
            $min_price = min($prices);
            $products[$key]->min_price = $min_price;
        }

                return response()->json($products,200);
        }elseif($sorting_id == 2 && $country_id == 0 && $cat_id ==0){
            $products = Product::where('cat_id','!=',0)->orderby('id','DESC')->get();
        foreach($products as $key=>$product){
            $prices = unserialize($product->prices);
            $min_price = min($prices);
            $products[$key]->min_price = $min_price;
        }

                return response()->json($products,200);

        }elseif($sorting_id != 2 && $country_id == 0 && $cat_id ==0){
            $products = Product::where('cat_id','!=',0)->orderby('id','ASC')->get();
        foreach($products as $key=>$product){
            $prices = unserialize($product->prices);
            $min_price = min($prices);
            $products[$key]->min_price = $min_price;
        }

            return response()->json($products,200);

        }elseif($sorting_id == 2 && $country_id == 0 && $cat_id !=0){
            $products = Product::where('cat_id','!=',0)->where('cat_id',$cat_id)->orderby('id','DESC')->get();
        foreach($products as $key=>$product){
            $prices = unserialize($product->prices);
            $min_price = min($prices);
            $products[$key]->min_price = $min_price;
        }

            return response()->json($products,200);

        }elseif($sorting_id != 2 && $country_id == 0 && $cat_id !=0){
            $products = Product::where('cat_id','!=',0)->whereRaw('cat_id',$cat_id)->orderby('id','ASC')->get();
        foreach($products as $key=>$product){
            $prices = unserialize($product->prices);
            $min_price = min($prices);
            $products[$key]->min_price = $min_price;
        }

            return response()->json($products,200);

        }    

    }



    public function orderSubmit(Request $request){   
        $this->checkBanned();
       
        date_default_timezone_set('Asia/Karachi');
        $client_id = Auth::id();
        $client = Client::find($client_id);
        $client_balanece = $client->balance;
        $invoice = $request->invoice;
        $total = (double)$request->grand_total;
        $order_comment = $request->order_comment?$request->order_comment:"No Comment";
        // $cart = serialize($request->cart);
        $cart_to_show = $request->cart;
        $non_auto_deliveries = array();
        foreach($cart_to_show as $index=>$product){
            $redeemInProgress = array();
            $redeem_code = array();
            $id = (int)$product['id'];
            $rate = (int)$product['rate'];
            $delivery_status = (int)$product['delivery_status'];
            $qty = (int)$product['qty'];
        //getting redeem codes
        $code = Redeem_code::where([
            'product_id'=>$id,
            'price'=>$rate,
            'status'=>0,
            'price_delivery_status'=>$delivery_status
        ])->orderby('id','ASC')->get();
        
        $code_length = sizeof($code);
        if($qty>$code_length && $code_length != 0){
            $product['qty'] = $code_length;
            $redeem_lengths = $product['qty'];//auto_delivery
            $remain_qty = (int)$qty-$redeem_lengths;

            for ($i=0; $i < $redeem_lengths; $i++) { 
                $code = Redeem_code::where([
                    'product_id'=>$id,
                    'price'=>$rate,
                    'status'=>0,
                    'price_delivery_status'=>$delivery_status
                ])->orderby('id','ASC')->limit(1)->get();
                $redeem_code[$i] = $code[0]->code;
                Redeem_code::find($code[0]->id)->update(['status'=>1]);
           
           }
        
        //getting redeem codes
            $productdb = Product::find($id);
            $redeem_link = $productdb->redeem_link;
        
            $cart_to_show[$index]['redeem_link'] = $redeem_link;
            $cart_to_show[$index]['redeem_code'] = $redeem_code; 
            $cart_to_show[$index]['qty'] = $code_length;
        //checkking whetehr or will auto dispatch or not
        //status (1) = order will dispatch automtically
        //status (0) = order will not dispatch automtically
            $cart_to_show[$index]["status"] = 1;
            $cart_to_show[$index]['guide'] = null;

            for ($i=0; $i < $remain_qty; $i++) {
                $non_auto_deliveries_index = sizeof($non_auto_deliveries); 
                $non_auto_deliveries[$non_auto_deliveries_index] = $cart_to_show[$index];
                $non_auto_deliveries[$non_auto_deliveries_index]['qty'] = 1;
                $non_auto_deliveries[$non_auto_deliveries_index]["status"] = 0;
                $non_auto_deliveries[$non_auto_deliveries_index]['redeem_code'] = ["In Progress"];
            }

        }//if $qty>$code_length && $code_length != 0
        elseif($qty<=$code_length){

            $redeem_lengths = $qty;//auto delivery
         
            for($i=0; $i < $redeem_lengths; $i++) { 
                $code = Redeem_code::where([
                    'product_id'=>$id,
                    'price'=>$rate,
                    'status'=>0,
                    'price_delivery_status'=>$delivery_status
                ])->orderby('id','ASC')->limit(1)->get();
                $redeem_code[$i] = $code[0]->code;
                Redeem_code::find($code[0]->id)->update(['status'=>1]);
           
           }
        
        //getting redeem codes
            $productdb = Product::find($id);
            $redeem_link = $productdb->redeem_link;
        
            $cart_to_show[$index]['redeem_link'] = $redeem_link;
            $cart_to_show[$index]['redeem_code'] = $redeem_code; 
    
        //checkking whetehr or will auto dispatch or not
        //status (1) = order will dispatch automtically
        //status (0) = order will not dispatch automtically
            $cart_to_show[$index]["status"] = 1;
            $cart_to_show[$index]['guide'] = null;
        }//else if ($qty<$code_length)
        elseif($qty>$code_length && $code_length == 0){
            for ($i=0; $i < $qty; $i++) {
                $non_auto_deliveries_index = sizeof($non_auto_deliveries); 
                $non_auto_deliveries[$non_auto_deliveries_index] = $cart_to_show[$index];
                $non_auto_deliveries[$non_auto_deliveries_index]['qty'] = 1;
                $non_auto_deliveries[$non_auto_deliveries_index]["status"] = 0;
                $non_auto_deliveries[$non_auto_deliveries_index]['redeem_code'] = ["In Progress"];
            }
            $cart_to_show[$index]['status'] = 5; 
            $cart_to_show[$index]['redeem_code'] = [];
            $cart_to_show[$index]['guide'] = null; 
            // unset();
        }////else if ($qty>$code_length && $code_length == 0)

        }//main foreach       

        $non_auto_deliveries_length = sizeof($non_auto_deliveries);
        //setting non auto delivery products
        for ($i=0; $i < $non_auto_deliveries_length; $i++) { 
            $showcart_length = sizeof($cart_to_show);
            $cart_to_show[$showcart_length] = $non_auto_deliveries[$i];                
        }
        
        //setting non auto delivery products




        $cart = serialize($cart_to_show);
    
        $data =[
            'invoice'=>$invoice,
            'client_id'=>$client_id,
            'total'=>$total,
            'cart'=>$cart,
            'comment'=>$order_comment,
        ]; 

        //checkking whetehr or will auto dispatch or not

        $order = new Order;
        if($order->create($data)){
            $client_balanece = $client_balanece-$total;
            $client->update([
                'balance'=>$client_balanece
            ]);

            //now checking affiliation
            $affiliation = Affiliation::where(['user_id'=>$client->id,'order_value'=>0,'commission'=>0])->get();
            $check_length = count($affiliation);
            if($check_length>0){
                $affiliation_code = $affiliation[0]->code;
                $commission = $total*0.1;
                $order_value = $total;

                $updateAffiliation = Affiliation::where(['user_id'=>$client->id,'order_value'=>0,'commission'=>0])->first();
                if($updateAffiliation->update(['commission'=>$commission,'order_value'=>$order_value])){
                    //getting client with code
                        $client_with_code = Client::where('affiliated_link',$affiliation_code)->get();
                        $client_commission = $client_with_code[0]->commission;
                        $client_commission+=$commission;

                        //now updating client commission
                        $aff_client = Client::where('affiliated_link',$affiliation_code)->first();
                        $aff_client->update(['commission'=>$client_commission]);
                        //now updating client commission

                        //now checking auto trnasfer or withdraw
                        $this->autoTransferCommission($aff_client->id);
                        //now checking auto trnasfer or withdraw

                    //getting client with code
                }//if update affiliation

            }//if user reached from affiliation link
            //now checking affiliation

            return response()->json(['status'=>200 ,'message'=>'Order Has Been Completed']);    
        }
        
     
    }

    public function orderCencel($invoice,$slug,$index){
        $this->checkBanned();
        
        $user_id = Auth::id();
    

        $order = Order::where(['client_id'=>$user_id, 'invoice'=>$invoice])->whereRaw('created_at < now() - interval 24 hour')->get();
        // $order = Order::where(['client_id'=>$user_id, 'invoice'=>$invoice])->where('created_at', '<', Carbon::now()->subMinutes(120)->toDateTimeString())->get();
        // dd($order);
        if(sizeof($order) > 0){
        $order = $order[0];
        $products = unserialize($order->cart);
        $amount_to_return =  $products[$index]['rate']*$products[$index]['btc_rate']*$products[$index]['qty'];
        unset($products[$index]);

        $cart = serialize($products);   

        Order::where([
            'client_id'=>$user_id,
            'invoice'=>$invoice,
        ])->first()->update(['cart'=>$cart]);
        $user = Client::find($user_id);
        $balance = $user->balance;
        $balance_after_return = $balance+$amount_to_return;
        $user->update(['balance'=>$balance_after_return]);

        return redirect()->back();
        }else{ 
        return redirect()->back()->withErrors(['error'=>'You Can not Delete it till 24 hours']);
        }
        // foreach($products as $key=>$product){

        // }

    }


    public function refundRequest($invoice,$slug,$index){
        $this->checkBanned();

        date_default_timezone_set('Asia/Karachi');
        $user_id = Auth::id();
        $order = Order::where([
            'client_id' => $user_id,
            'invoice'=>$invoice,
        ])->get();
        $products = unserialize($order[0]->cart);
        $product = $products[$index];
        $product_total = $product['rate']*$product['btc_rate']*$product['qty'];
        $product_id = $product['id'];

        //now calculating time
        // $order_time = $order[0]->created_at;
        // $current_time = Carbon::now();
        // $refund_time = Product::find($product_id)->refund_time;
        // $total_duration = $current_time->floatDiffInHours($order_time);
        // $total_duration = $total_duration/60;

        // if($total_duration > $refund_time){
            // dd([$total_duration,$refund_time]);
                    
        // return redirect()->back()->withErrors(['error'=>'You Can not Delete It Now As Refund Request Time Has Passed']);
            
        // }else{
            $product = serialize($product);
            $data = [
                'user_id'=>$user_id,
                'invoice'=>$invoice,
                'key'=>$index,
                'product'=>$product,
                'amount'=>$product_total
        
            ];
            $check_refund = Refund_request::where(
                [
                    'user_id'=>$user_id,
                    'invoice'=>$invoice,
                    'key'=>$index
    
                ]
            )->get();
            if(sizeof($check_refund)>0){
                // return redirect()->back()->withErrors(['error'=>'You Already Requested Refund On That Product']);
                return response()->json('also good');

            }
            if(Refund_request::create($data)){
                // return redirect()->back()->with(['msg'=>'Refund Request Has Been Sent']);
                return response()->json('good');
            }
            
        // }

    }


    public function refundRequestUpdate(Request $request){
        $this->checkBanned();

        date_default_timezone_set('Asia/Karachi');
        $user_id = Auth::id();
        $order = Order::where([
            'client_id' => $user_id,
            'invoice' => $request->invoice,
        ])->get();
        $products = unserialize($order[0]->cart);
        $product = $products[$request->index];
        $product_total = $product['rate']*$product['btc_rate']*$product['qty'];
        $product_id = $product['id'];

            $product = serialize($product);
            $data = [
                'user_id'=>$user_id,
                'invoice'=>$request->invoice,
                'key'=>$request->index,
                'product'=>$product,
                'amount'=>$product_total
        
            ];
            $check_refund = Refund_request::where(
                [
                    'user_id'=>$user_id,
                    'invoice'=>$request->invoice,
                    'key'=>$request->index
    
                ]
            )->get();
            if(sizeof($check_refund)>0){
                Refund_request::find($check_refund[0]->id)->update(['comment'=>$request->comment,'status'=>1]);
                return redirect()->back()->with(['msg'=>'Refund Request Has Been Sent']);
            }else{
                return redirect()->back()->withErrors(['error'=>'No Record Found']);
            }
            

    }



    //account functions
    public function account(){

        $this->checkBanned();


        $id = Auth::id();
        $balance = Client::find($id)->balance;
        $transactions = Transaction::whereRaw('user_id = '.$id.' AND status != 0')->get();
        $data = [
            'balance'=>$balance,
            'transactions'=>$transactions
        ];
        return view('Client/account',compact('data'));
    }

    public function addFund(){
        $this->checkBanned();
        return view('Client/addFund');
    }

    public function createAddress(Request $request){
        $this->checkBanned();
        $user_id = Auth::id();
        $data = [
            'user_id'=>$user_id,
            'invoice'=>$request->invoice,
            'btc_address'=>$request->address,
            'payment_code'=>$request->payment_code,            
        ];
        $transaction = Transaction::create($data);
        return response()->json($transaction,200);
    }

    public function btcCallback(Request $request){
        $this->checkBanned();
        //status = 1 means pending payment
        //status = 2 means confirmed payment
        $user_id = Auth::id();


        if($request->status == 1){
            $transaction = Transaction::where([
                'user_id'=>$user_id,
                'btc_address'=>$request->address,
                
            ])->first()->update([
                'status'=>$request->status,
                'amount'=>$request->amount
            ]);
    
        }

        if ($request->status == 2) {
            $transactionCheck = Transaction::where([
                'user_id'=>$user_id,
                'btc_address'=>$request->address,
                'status'=>2
                
            ])->get();
            $length = sizeof($transactionCheck);    

            if($length == 0){

                $transaction = Transaction::where([
                    'user_id'=>$user_id,
                    'btc_address'=>$request->address,
                    
                ])->first()->update([
                    'status'=>$request->status,
                    'amount'=>$request->amount
                ]);
    
                //updating client balance
                $client = Client::find($user_id);
                $balance = (double)($client->balance+$request->amount);
                $client->update(['balance'=>$balance]);
                //updating client balance

              }
                

          
        }
        
    }

    public function pendingAddresses(Request $request){
        $this->checkBanned();

        $user_id = Auth::id();
        $transaction = Transaction::where([
            'user_id'=>$user_id
        ])->whereRaw('(status=0 OR status = 1)')->get();

        $length = sizeof($transaction);
        if($length > 0){
            return response()->json($transaction,200);
        }else{
            $transaction = array();
            return response()->json($transaction,200);

        }
        
    }

    public function myproducts(){
        $this->checkBanned();

        $id = Auth::id();
        $orders = Order::where('client_id',$id)->orderby('id','DESC')->get();
        foreach($orders as $index=>$order){
            $orders[$index]->cart = unserialize($orders[$index]->cart);
        }

        return view('Client/myproducts',compact('orders'));
    }

    public function purchase_history(){
        $this->checkBanned();
        date_default_timezone_set('Asia/Karachi');
        $id = Auth::id();
$orders_update = Order::where(['client_id'=>$id,'notification'=>2])->update(['notification'=>3]);
        $orders = Order::where('client_id',$id)->orderby('id','DESC')->get();
        foreach($orders as $index=>$order){
            $orders[$index]->cart = unserialize($orders[$index]->cart);
            $times = array();
            $refund_status = array();
            $refund_comment =array();
            $see_order_status = array();
            // $refund_status = 0 means he didnt see order
            // $refund_status = 1 means he has seen order
            // $refund_status = 2 means he has request refund 
            // $refund_status = 3 means request refund time expired  Or Refund time = 0 
            // $refund_status = 4 means request refund accepted 
            // $refund_status = 5 means request refund decline  

            // //setting that
            foreach($orders[$index]->cart as $key=>$cart){
                // $product_id = Product::find($cart['id'])->id;
                
                $invoice = $order->invoice;
                $refund_request = Refund_request::where([
                    'invoice'=>$invoice,
                    'key'=>$key,
                ])->get();
                if(sizeof($refund_request)>0){
                    $status_refund = $refund_request[0]->status;
                    $see_order_status[$key] = 1;
                     if($status_refund == 0){
                           
                        $created_time = $refund_request[0]->created_at;
                        $current_time = Carbon::now();
                        $refund_time = Product::find($cart['id'])->refund_time;
                        $total_duration = $current_time->floatDiffInMinutes($created_time);
                        if($total_duration>$refund_time){
                         $times[$key] = 0;
                         $refund_status[$key] = 5;
                         // means refund time has passed
 
                        }//second level if
                        else{
 
                         $times[$key] = $refund_time-$total_duration;
                         $refund_status[$key] = 1;
 
                        }//second level else
 


                    }elseif($status_refund == 1){
                        $times[$key] = 0;
                        $refund_status[$key] = 2;//measn requestted refund;
                        $refund_comment[$key] = $refund_request[0]->comment;
                    }elseif($status_refund == 2){
                        $times[$key] = 0;
                        $refund_status[$key] = 3;//meana requestted refund accepted;
                    }elseif($status_refund == 3){
                        $times[$key] = 0;
                        $refund_status[$key] = 4;//meana requestted refund decline;
                        $refund_comment[$key] = $refund_request[0]->comment_deny;
                    }
                }//sizeof($refund_request)>0
                else{
                    $see_order_status[$key] = 0;
                    $refund_time = Product::find($cart['id'])->refund_time;
                    $times[$key] = $refund_time;
                    if($refund_time == 0){
                        $refund_status[$key] = 5;//refund time = 0
                    }//refintome 0 if
                    else{
                        $refund_status[$key] = 0;
                    }
         
                }//else//sizeof($refund_request)>0
            }//inner foreach

            $orders[$index]->invoice_total = 0;
            $orders[$index]->times = $times;
            $orders[$index]->refund_status = $refund_status;
            $orders[$index]->refund_comment = $refund_comment;
$orders[$index]->see_order_status = $see_order_status ;
          
 // dd($orders[$index]);
        }//order foreach (outer)
        // dd($orders);

        return view('Client/purchase_history',compact('orders'));
    }

    //account functions


    public function support(){
        $this->checkBanned();

        $user_id = Auth::id();
        $update_status = Message::where([
            'user_id'=>$user_id,
            'status'=>1
        ])->update(['status'=>4]);
        $messages = Message::where('user_id',$user_id)->orderby('id','DESC')->get();
        return view('Client/support',compact('messages'));
    }

    public function sendMessage(Request $request){
        $this->checkBanned();

        $user_id = Auth::id();
        $data = [
            'user_id'=>$user_id,
            'message'=>$request->message
        ];
        if(Message::create($data)){
            return redirect('/support')->with('success','Message Has Been Submitted');
        }


    }


//static functions
    public static function notificationUser(){

        $user_id = Auth::id();

        $messages = Message::where('user_id',$user_id)->whereRaw('status = 1')->get();
        $length = sizeof($messages);
        return $length;
    }

    public static function notificationOrderDispatch(){

        $user_id = Auth::id();

        $dispatch = Order::where(['client_id'=>$user_id,'notification'=>2])->get();
        $length = sizeof($dispatch);
        return $length;
    }



    public static function homeBanners(){

        $banner1 = Banner::find(1);
        $banner2 = Banner::find(2);

        $data = [
            'banner1'=>$banner1,
            'banner2'=>$banner2
        ];

        return $data;
    }
//static functions



//*********************affiliation start***********************//
    public function affiliate(){
        $this->checkBanned();

        $user_id = Auth::id();
        $user = Client::find($user_id);
        $check_link = $user->affiliated_link;
        $mode = $user->auto_request_mode;
        if($check_link == NULL){

            $data = [
                'check_link'=>NULL,
                'affiliation_data'=>[],
                'users_referred'=>0,
                'total_earned'=>0,
                'current_amount'=>0,
                'mode'=>0
                    
            ];

            
            return view('Client/affiliate',compact('data'));
        }else{
            $affiliation = Affiliation::where('code',$check_link);
            $withdrawrequest = Withdrawrequest::where('user_id',$user_id);
            $affiliation_data = $affiliation->get();
            $users_referred = count($affiliation_data);
            $total_earned = $affiliation->sum('commission');
            $current_amount = $user->commission;
            
            $data = [
                'check_link'=>$check_link,
                'affiliation_data'=>$affiliation_data,
                'users_referred'=>$users_referred,
                'total_earned'=>$total_earned,
                'current_amount'=>$current_amount,
                'mode'=>$mode
            ];

            // dd($data);
            return view('Client/affiliate',compact('data'));
        }

        
    }


    public function withdrawRequests(){
        $this->checkBanned();

        $user_id = Auth::id();
        $user = Client::find($user_id);
        $check_link = $user->affiliated_link;
        
        if($check_link == NULL){

            $data = [
                'check_link'=>NULL,
                'affiliation_data'=>[],
                'withdrawrequest_data'=>[],
                'users_referred'=>0,
                'total_earned'=>0,
                'current_amount'=>0
                    
            ];

            
            return view('Client/withdrawRequests',compact('data'));
        }else{
            $affiliation = Affiliation::where('code',$check_link);
            $withdrawrequest = Withdrawrequest::where('user_id',$user_id);
            $affiliation_data = $affiliation->get();
            $withdrawrequest_data = $withdrawrequest->get();
            $users_referred = count($affiliation_data);
            $total_earned = $affiliation->sum('commission');
            $current_amount = $user->commission;
            
            $data = [
                'check_link'=>$check_link,
                'affiliation_data'=>$affiliation_data,
                'users_referred'=>$users_referred,
                'total_earned'=>$total_earned,
                'current_amount'=>$current_amount,
                'withdrawrequest_data'=>$withdrawrequest_data,
            ];

            // dd($data);
            return view('Client/withdrawRequests',compact('data'));
        }

        
    }


    public function generateLink(){
        $this->checkBanned();

        $user_id = Auth::id();
        $affiliate_id = uniqid();
        
        $check_link = Client::find($user_id)->affiliated_link;
        if($check_link == NULL){
            $user = Client::where('id',$user_id)->first()->update(['affiliated_link'=>$affiliate_id]);
            return redirect('/affiliate');
        }else{
            return redirect('/affiliate');
        }

    }


    public function transferCommission(){
        $this->checkBanned();
        $user_id = Auth::id();
        $user = Client::find($user_id);
        $user_balance = $user->balance;
        $user_commission = $user->commission;

        if($user_commission<=0.0020){
            return redirect('/affiliate')->with('error','You Dont Have Enough Balance To Proceed Minimum amount To proceed is 0.0020 BTC');    
        }
        if($user_commission)
        $user_balance += $user_commission;
        $user_commission-=$user_commission;
        Withdrawrequest::where('user_id',$user_id)->delete();
        if($user->update(['balance'=>$user_balance,'commission'=>$user_commission])){
            return redirect('/affiliate')->with('success','Commission Transferred Into Balance Successfully');
        }else{
            return redirect('/affiliate')->with('Error','Oops Commission Din not Transferred Into Balance');

        }
        

    }


    public function withdrawCommission(Request $request){
        $this->checkBanned();
        $user_id = Auth::id();
        $user = Client::find($user_id);
        $rules = [
            'btc_address'=>'required',
            'amount'=>'required'
        ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return redirect('/affiliate')->with('error','All The Fields Are Required');
        }

        if($request->amount<=0.0020){
            return redirect('/affiliate')->with('error','You Dont Have Enough Balance To Proceed Minimum amount To proceed is 0.0020 BTC');    
        }

        $data = [
            'user_id'=>$user_id,
            'btc_address'=>$request->btc_address,
            'amount'=>$request->amount,
            'status'=>0
        ];
        Withdrawrequest::where('user_id',$user_id)->delete();
        if(Withdrawrequest::create($data)){
            return redirect('/affiliate')->with('success','Request Has Success Fully Been Sent');
        }else{
            return redirect('/affiliate')->with('error','Request Abandond');

        }
    }

    public function autoSetCommission(Request $request){
        $this->checkBanned();
        $user_id = Auth::id();
        $user = Client::find($user_id);
        
        if($request->auto_request_mode == 1){
            $rules = [
                'auto_commission_amount'=>'required',
                'auto_request_mode'=>'required'
            ];
    
        }elseif($request->auto_request_mode == 2){
            $rules = [
                'auto_commission_amount'=>'required',
                'auto_request_mode'=>'required',
                'btc_address'=>'required'
            ];
        }

        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return redirect('/affiliate')->with('error','All The Fields Are Required');
        }

        if($request->auto_commission_amount<0.0020){
            return redirect('/affiliate')->with('error','Set Your Minimum Amount Greater Than 0.0020 BTC');    
        }

        if($request->auto_request_mode == 1){
            $data = [
                'auto_commission_amount'=>$request->auto_commission_amount,
                'auto_request_mode'=>$request->auto_request_mode,
            ];
    
        }elseif($request->auto_request_mode == 2){
            $data = [
                'auto_commission_amount'=>$request->auto_commission_amount,
                'auto_request_mode'=>$request->auto_request_mode,
                'btc_address'=>$request->btc_address,
            ];

        }

        if($user->update($data)){
            return redirect('/affiliate')->with('success','Auto Transfer Option Is Set');
        }else{
            return redirect('/affiliate')->with('error','Request Abandond');

        }
    }


    public function autoTransferCommission($user_id){
        
        $user = Client::find($user_id);
        $user_balance = $user->balance;
        $user_commission = $user->commission;
        $auto_request_mode = $user->auto_request_mode;
        $auto_commission_amount = $user->auto_commission_amount;
        $btc_address = $user->btc_address;
        //main if else check wheteher with draw request or transfer amount
        if($auto_request_mode == 1){
            if($user_commission>=0.0020 && $user_commission>=$auto_commission_amount){

                $user_balance += $user_commission;
                $user_commission-=$user_commission;
                Withdrawrequest::where('user_id',$user_id)->delete();
                $user->update(['balance'=>$user_balance,'commission'=>$user_commission]);
                
            }//if user commission>0.0020    
        }elseif($auto_request_mode == 2){
            if($user_commission>=0.0020 && $user_commission>=$auto_commission_amount){

                $data = [
                    'user_id'=>$user_id,
                    'btc_address'=>$btc_address,
                    'amount'=>$user_commission,
                    'status'=>0
                ];
                Withdrawrequest::where('user_id',$user_id)->delete();
                Withdrawrequest::create($data);        
            }//if user commission>0.0020    

        }
        //main if else check wheteher with draw request or transfer amount
        

    }//public function



    //*********************affiliation end***********************//



}
