<?php

namespace App\Http\Controllers;

use App\Models\Banner;
use Illuminate\Http\Request;

class BannerController extends Controller
{
    public function banner1(Request $request){
        $heading = $request->heading?$request->heading:"";
        $text = $request->text?$request->text:"";
        if($request->image){
            $file = $request->file('image');
            $ext = $file->getClientOriginalExtension();
            $fileNameToStore = uniqid()."_".$ext;
            $file->move(public_path('banners'),$fileNameToStore);
            $banner = Banner::find(1)->update([
                'image'=>$fileNameToStore,
                'heading'=>$heading,
                'text'=>$text
            ]);
          return redirect()->back()->with('message','Succesfully Updated');
        }else{
            $banner = Banner::find(1)->update([
                'heading'=>$heading,
                'text'=>$text
            ]);
            return redirect()->back()->with('message','Succesfully Updated');
        }

    }



    public function banner2(Request $request){
        $heading = $request->heading?$request->heading:"";
        $text = $request->text?$request->text:"";
        if($request->image){
            $file = $request->file('image');
            $ext = $file->getClientOriginalExtension();
            $fileNameToStore = uniqid()."_".$ext;
            $file->move(public_path('banners'),$fileNameToStore);
            $banner = Banner::find(2)->update([
                'image'=>$fileNameToStore,
                'heading'=>$heading,
                'text'=>$text
            ]);
          return redirect()->back()->with('message','Succesfully Updated');
        }else{
            $banner = Banner::find(2)->update([
                'heading'=>$heading,
                'text'=>$text
            ]);
            return redirect()->back()->with('message','Succesfully Updated');
        }

    }




    public function banner3(Request $request){
        $heading = $request->heading?$request->heading:"";
        if($request->image){
            $file = $request->file('image');
            $ext = $file->getClientOriginalExtension();
            $fileNameToStore = uniqid()."_".$ext;
            $file->move(public_path('banners'),$fileNameToStore);
            $banner = Banner::find(3)->update([
                'image'=>$fileNameToStore,
                'heading'=>$heading
            ]);
          return redirect()->back()->with('message','Succesfully Updated');
        }else{
            $banner = Banner::find(3)->update([
                'heading'=>$heading
            ]);
            return redirect()->back()->with('message','Succesfully Updated');
        }

    }
   

}
