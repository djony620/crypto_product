<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class btcRateController extends Controller
{
    public function btcRate(){
        $api = url("https://api.coindesk.com/v1/bpi/currentprice.json?fbclid=IwAR22QK7ER4YKxa5Kla7u0bPkiMxC_K2iXWQQGsO4XlON6-wK54gZuE9Z6ec");
        return response()->json($api,200);
    }
}
