<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateProductRequest;
use App\Http\Requests\UpdateProductRequest;
use App\Repositories\ProductRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Country;
use App\Models\Redeem_code;


use Flash;
use Response;

class ProductController extends AppBaseController
{
    /** @var  ProductRepository */
    private $productRepository;

    public function __construct(ProductRepository $productRepo)
    {
        $this->productRepository = $productRepo;
    }

    /**
     * Display a listing of the Product.
     *
     * @param Request $request
     *
     * @return Response
     */
      public function index(Request $request)
    {
        $products = $this->productRepository->all();
        foreach($products as $index=>$product){
         $codes = Redeem_code::where(['product_id' => $product->id,'status' => 0])->get();
$products[$index]->codes = $codes;
      }

        return view('products.index')
            ->with('products', $products);
    }

    /**
     * Show the form for creating a new Product.
     *
     * @return Response
     */
    public function create()
    {
        $product = [];
        return view('products.create',compact('product'));
    }

    /**
     * Store a newly created Product in storage.
     *
     * @param CreateProductRequest $request
     *
     * @return Response
     */
    public function store(CreateProductRequest $request)
    {
        // $input = $request->all();

        $product = $this->productRepository->insert_product($request);

        Flash::success('Product saved successfully.');

        return redirect(route('products.index'));
    }

    /**
     * Display the specified Product.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $product = $this->productRepository->find($id);

        $country = Country::find($product->country_id)->name;
        $category = Category::find($product->cat_id)->name;
        $prices = unserialize($product->prices);
        $product->price = $prices;
        $comments = unserialize($product->comments);
        $product->comment = $comments;

        $product->country = $country;
        $product->category = $category;
        if (empty($product)) {
            Flash::error('Product not found');

            return redirect(route('products.index'));
        }

        return view('products.show')->with('product', $product);
    }

    /**
     * Show the form for editing the specified Product.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $product = $this->productRepository->find($id);
        $redeem_codes = array();
        $prices = $product->prices;
        $prices = unserialize($prices);        
        $product->price = $prices;

        $comments = unserialize($product->comments);
        $product->comment = $comments;

        $delivery_status = unserialize($product->delivery_status);
        $product->delivery_statuss = $delivery_status;

        foreach($prices as $index=>$price){
        $redeems = Redeem_code::where([
            'product_id'=>$product->id,
            'price'=>$prices[$index],
            'status'=>0,
            'price_delivery_status'=>$delivery_status[$index]
        ])->get();
        $redeem_codes[$index] = $redeems;
        }

        $product->redeem_codes = $redeem_codes;

        // dd($product->redeem_codes);
        // $redeem_codes = Redeem_code::where('product_id',$product->id)->get();
        // dd($product->prices);
        if (empty($product)) {
            Flash::error('Product not found');

            return redirect(route('products.index'));
        }

        return view('products.edit',compact('product'));
    }

    /**
     * Update the specified Product in storage.
     *
     * @param int $id
     * @param UpdateProductRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateProductRequest $request)
    {
        $product = $this->productRepository->find($id);

        if (empty($product)) {
            Flash::error('Product not found');

            return redirect(route('products.index'));
        }

        $product = $this->productRepository->updateProduct($request, $id);

        Flash::success('Product updated successfully.');

        return redirect(route('products.index'));
    }

    /**
     * Remove the specified Product from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $product = $this->productRepository->find($id);

        if (empty($product)) {
            Flash::error('Product not found');

            return redirect(route('products.index'));
        }

        $this->productRepository->delete($id);

        Flash::success('Product deleted successfully.');

        return redirect(route('products.index'));
    }
}
