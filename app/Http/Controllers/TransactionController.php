<?php

namespace App\Http\Controllers;

use App\Client;
use App\Models\Transaction;
use Illuminate\Http\Request;

class TransactionController extends Controller
{
    public function index(){
        $transactions = Transaction::where('status','!=',0)->get();

        foreach($transactions as $index=>$transaction){
            $client_name = "N/A";
            if (!empty($client_name = Client::find($transaction->user_id)))
            $client_name =$client_name->username;
            $transactions[$index]->client_name = $client_name;

        }
        return view('transactions.index',compact('transactions'));
    }
}
