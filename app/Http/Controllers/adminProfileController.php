<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class adminProfileController extends Controller
{

    public function index($id){
        
        $user = User::find($id);
        return view('profile',compact('user'));
    }


    public function profileUpdate(Request $request){
        $id = Auth::id();
        $user = User::find($id);
        $rules = [
            'name'=>'required',
            'username'=>'required',
            'email'=>'required',
        ];
        $validate = Validator::make($request->all(),$rules);
        
        if(!$validate->fails()){
            if($user->update($request->all())){
                return redirect('/admin');
            }//query check
        }else{
            $error = "Some thing Went Wrong";
            return redirect('/admin/profileSettings/'.$id);

        }//validation check

    }

    public function profileUpdatePassword(Request $request){
        $id = Auth::id();
        $user = User::find($id);

        $rules = [
            'password'=>'required|confirmed|min:6'
        ];

        $validate = Validator::make($request->all(),$rules);
        if(!$validate->fails()){
            $password = Hash::make($request->password);
            if($user->update(['password'=>$password])){
                return redirect('/admin');
            }    
        }else{
            return redirect('/admin/profileSettings/'.$id)->withErrors($validate);
        }

    }


}
