<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Message;
use App\Client;
class messagesController extends Controller
{
    public function index(){
        Message::whereRaw('id != 0 AND status != 1 AND status != 4')->update(['status'=>3]);
        $messages = Message::all()->sortByDesc('id');
        foreach($messages as $index=>$message){
            $user_id =  $message->user_id;
            $user_name = Client::find($user_id)->username;
            $messages[$index]->client_name = $user_name;
        }

        return view('messages.index',compact('messages'));
    }


    public function showMessage($id){
        $message = Message::find($id);
        $user_id = $message->user_id;
        $user_messages = Message::where('user_id',$user_id)->orderby('id','DESC')->get();
        $data = [
            'message'=>$message,
            'user_messages'=>$user_messages
        ];

        return view('messages.showMessage',compact('data'));
    }


    public function updateMessage(Request $request){
        $message = Message::find($request->id);
        $message->update([
            'reply'=>$request->reply,
            'status'=>1
        ]);
        return redirect('/admin/messages');
    }

    public static function notificationAdmin(){
        $messages = Message::where('status',0)->get();
        $length = sizeof($messages);
        return $length;
    }


    
}
