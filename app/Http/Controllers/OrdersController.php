<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Order;
use App\Client;
class OrdersController extends Controller
{
    public function index(){
        $orders = Order::all()->sortByDesc('id');

$orders_update = Order::where('notification',0)->update(['notification'=>1]);
        foreach ($orders as $index => $order) {
            $counter = 0;
            $carts = unserialize($order->cart);
                // dd($carts);
                foreach($carts as $key=>$cart){
                    if($cart['status']!=5 && ($cart['redeem_code'][0] == 'In Progress' || $cart['redeem_code'][0] == '')){
                        $counter++;            
                    }
                }
            
            $client_id = $order->client_id;
            $client_name = Client::find($client_id)->username;
            $orders[$index]->client_name = $client_name;
            if($counter>0){
                $orders[$index]->notify = 0;
            }else{
                $orders[$index]->notify = 1;
            }
        }
        return view('orders.index',compact('orders'));
    }



    public function showOrder($invoice){
        $order = Order::where('invoice',$invoice)->get();
        $order[0]->cart = unserialize($order[0]->cart);    
        $cart = $order[0]->cart;
        $data = [
            'cart'=>$cart,
'comment'=>$order[0]->comment,
            'invoice'=>$order[0]->invoice
        ];
        return view('orders.show_order',compact('data'));
    }



    public function updateOrder(Request $request){
        $order = Order::where('invoice',$request->invoice)->get();
        $order = Order::find($order[0]->id);
        $cart = unserialize($order->cart);
        $codes = $request->codes;
      
  // now slug is index
        $cart[$request->slug]['redeem_code'] = $codes;
        $cart[$request->slug]['guide'] = $request->guide;
        $cart[$request->slug]['status'] = 1;

        $cart_to_store = serialize($cart);
        if($order->update(['cart'=>$cart_to_store,'notification'=>2])){
            return response()->json([
                'status'=>200,
                'message'=>'Successfully Assigned'
            ]);

        }else{
            return response()->json([
                'status'=>500,
                'message'=>'Some Thing Wrong'
            ]);

        }

    }


  public function dispatchOrder($id){
    $order = Order::find($id);
    if($order->update(['status'=>1])){
        return redirect()->back()->withErrors('Order Delivered');
    }else{
        return redirect()->back()->withErrors('Some Thing Went Wrong');

    }
      
  }  
}
