<?php

namespace App\Http\Controllers;

use App\Models\Banner;
use Illuminate\Http\Request;

class loginBackController extends Controller
{
    public static function loginBack(){
        $banner = Banner::find(3);
        $image = $banner->image;
        return $image;
    }

    public static function loginBookmark(){
        $banner = Banner::find(3);
        $heading = $banner->heading;
        return $heading;
    }


}
