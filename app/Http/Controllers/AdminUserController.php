<?php

namespace App\Http\Controllers;

use App\Client;
use Illuminate\Http\Request;
use App\Models\Order;
use App\Models\Message;
use App\Models\Refund_request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;

//affiliation
use App\Models\Withdrawrequest;
//affiliation


class AdminUserController extends Controller
{
    public function index(){
        $clients = Client::all();
        return view('users.index',compact('clients'));
    }


    public function banUser($id){
        $client = Client::find($id);
        if($client){
            $client->update(['status'=>1]);
            echo "<script>alert('User Is Banned')</script>";
            return redirect()->back();
        }
        
    }


    public function unBanUser($id){
        if(Client::find($id)->update(['status'=>0])){
            // echo "<script>alert('User Is Active Now')</script>";
            return redirect()->back();
        }
        
    }

    public function deletesView(){
        $message = NULL;
        return view('deletes.index',compact('message'));
    }

    public function deletes(Request $request){
        $message = "Successfully Deleted";
        // Message::truncate();   
        // Order::truncate();
        date_default_timezone_set('Asia/Karachi');
        Message::where('created_at', '<', Carbon::now()->subDays(15))->delete();
        Order::where('created_at', '<', Carbon::now()->subDays(15))->delete();
        return redirect()->back()->with('message',$message);

    }

    public function changePassword($id){
        $user = Client::find($id);
        return view('users.changePassword',compact('user'));
    }

    public function updatePassword(Request $request){
        $rules = [
            'password'=>'required|confirmed'
        ];

        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return redirect()->back()->withErrors(['message'=>'Password confirmation not matched']);
        }else{
        $user = Client::find($request->id);
        $password = Hash::make($request->password);
        if($user->update(['password'=>$password,'text'=>$request->password])){
            return redirect('admin/users');
        }
        
        }
    }


    public function refund(){
        $refunds = Refund_request::where('status',1)->get();
        $refunds_update = Refund_request::where('notification',0)->update(['notification'=>1]);
        foreach($refunds as $key=>$refund){
            $client_name = Client::find($refund->user_id)->username;
            $refunds[$key]->client_name =$client_name; 
            $refunds[$key]->product = unserialize($refunds[$key]->product);
            // dd($refunds[$key]->product);
        }
        return view('refund.index',compact('refunds'));
    }

    public function refundAmount($id,$invoice,$index){
    
        $refund = Refund_request::find($id);
        $order = Order::where(['invoice'=>$invoice])->get();
        $order = $order[0];
        $user_id = $order->client_id;
        $products = unserialize($order->cart);
        $amount_to_return =  $products[$index]['rate']*$products[$index]['btc_rate']*$products[$index]['qty'];

        $user = Client::find($user_id);
        $balance = $user->balance;
        $balance_after_return = $balance+$amount_to_return;
        $user->update(['balance'=>$balance_after_return]);
        $refund->update(['status'=>2]);
        return redirect()->back()->with(['message'=>'Amount Refunded']);
        }
    

        public function showDeclineComment($id){
            $refund = Refund_request::find($id);
            return view('refund.comment',compact('refund'));
        }

        public function declineRefundAmount(Request $request){
    
            $refund = Refund_request::find($request->id);
            $refund->update(['comment_deny'=>$request->comment,'status'=>3]);
            return redirect('/admin/refund')->with(['message'=>'Request Decline']);;
            }
    

     public static function notificationRefund(){
        $refund = Refund_request::where('notification',0)->get();
        $length = sizeof($refund);
        return $length;
    }

     public static function notificationOrder(){
        $order = Order::where('notification',0)->get();
        $length = sizeof($order);
        return $length;
    }


//********************affiliation*********************//
public function showWithdrawRequest(){
    $requests = Withdrawrequest::all();
    foreach($requests as $key=>$request){
        $user_id = $request->user_id;
        $user_name =Client::find($user_id)->username;
        $commission =Client::find($user_id)->commission;
        $requests[$key]['name'] = $user_name;
        $requests[$key]['commission'] = $commission;
    }
    return view('withdraw.index',compact('requests'));
}

public function sentAmount($id){
    $request = Withdrawrequest::find($id);
    $user_id = $request->user_id;
    $user = Client::find($user_id);
    $user_commission = $user->commission;
    if($user_commission<=0.002){
        return redirect('/admin/showWithdrawRequest')->with('error','user have insufficient Commission');
    }
    $user_commission-=$request->amount; 
    if($request->update(['status'=>1])){
        $user->update(['commission'=>$user_commission]);
        return redirect('/admin/showWithdrawRequest');
    }
}
//********************affiliation*********************//


}
