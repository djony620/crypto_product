<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'ClientHomeController@index');

//client Authentication
 Route::get('/login','Client\Auth\LoginController@showLoginForm')->name('login');
 Route::post('/login','Client\Auth\LoginController@login');
 Route::post('/logout','Client\Auth\LoginController@logout');
 Route::post('/password/confirm','Client\Auth\ConfirmPasswordController@confirm');
 Route::get('/password/confirm','Client\Auth\ConfirmPasswordController@showConfirmForm');
 Route::post('/password/email','Client\Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
 Route::post('/password/reset','Client\Auth\ResetPasswordController@reset')->name('password.reset');
 Route::get('/password/reset','Client\Auth\ForgotPasswordController@showLinkRequestForm');
 Route::get('/password/reset/{token}','Client\Auth\ResetPasswordController@showResetForm');
 Route::get('/register','Client\Auth\RegisterController@showRegistrationForm');
 Route::post('/register','Client\Auth\RegisterController@register');


 //Admin Authentication

 Route::prefix('admin')->group(function(){
    Route::get('/',function(){
        return view('home');
    })->middleware('auth');
    Route::get('/login','Auth\LoginController@showLoginForm')->name('adminLogin');
    Route::post('/login','Auth\LoginController@login');
    Route::post('/logout','Auth\LoginController@logout')->name('admin/logout');

    Route::get('/register','Auth\RegisterController@showRegistrationForm');
    Route::post('/register','Auth\RegisterController@register');

    Route::get('/profileSettings/{id}','adminProfileController@index')->middleware('auth');
    Route::post('/profileUpdate','adminProfileController@profileUpdate')->middleware('auth');;
    Route::post('/profileUpdatePassword','adminProfileController@profileUpdatePassword')->middleware('auth');;

    Route::resource('/categories', 'CategoryController')->middleware('auth');;

    Route::resource('/cities', 'CityController')->middleware('auth');;

    Route::resource('/countries', 'CountryController')->middleware('auth');
    Route::resource('/products', 'ProductController')->middleware('auth');
    Route::get('/orders','OrdersController@index')->middleware('auth');
    Route::get('/showOrder/{invoice}','OrdersController@showOrder')->middleware('auth');
    Route::get('/updateOrder','OrdersController@updateOrder')->middleware('auth');
    Route::get('/dispatchOrder/{id}','OrdersController@dispatchOrder')->middleware('auth');

    //messages

    Route::get('/messages','messagesController@index')->middleware('auth');
    Route::get('/showMessage/{id}','messagesController@showMessage')->middleware('auth');
    Route::post('/updateMessage','messagesController@updateMessage')->middleware('auth');

    Route::get('/change_banners',function(){
        return view('banners.index');
    })->middleware('auth');


    Route::post('/changeBanner1','BannerController@banner1')->middleware('auth');
    Route::post('/changeBanner2','BannerController@banner2')->middleware('auth');
    Route::post('/changeBanner3','BannerController@banner3')->middleware('auth');

    Route::get('/users','AdminUserController@index')->middleware('auth');
    Route::get('/changePassword/{id}','AdminUserController@changePassword')->middleware('auth');
    Route::post('/updatePassword','AdminUserController@updatePassword')->middleware('auth');


    Route::get('/banUser/{id}','AdminUserController@banUser')->middleware('auth');
    Route::get('/unBanUser/{id}','AdminUserController@unBanUser')->middleware('auth');
    Route::get('/deletesView','AdminUserController@deletesView')->middleware('auth');
    Route::post('/deletes','AdminUserController@deletes')->middleware('auth');
    Route::get('/transactions','TransactionController@index')->middleware('auth');

    Route::get('/refund','AdminUserController@refund')->middleware('auth');
    Route::get('/refundAmount/{id}/{invoice}/{index}','AdminUserController@refundAmount')->middleware('auth');


    //affiliation
    Route::get('/showWithdrawRequest','AdminUserController@showWithdrawRequest')->middleware('auth');
    Route::get('/sentAmount/{id}','AdminUserController@sentAmount')->middleware('auth');
    //affiliation


    //decline refund
    Route::get('/declineRefundAmount/{id}','AdminUserController@showDeclineComment')->middleware('auth');
    Route::post('/declineRefundAmountUpdate','AdminUserController@declineRefundAmount')->middleware('auth');
    //decline refund

});






//Client Side Views
Route::get('/home','ClientHomeController@index');
Route::get('/home/category/{id}','ClientHomeController@homeCategory');
Route::get('/product/{id}','ClientHomeController@showProduct');
Route::post('/btcCallback','ClientHomeController@btcCallback');
Route::post('/createAddress','ClientHomeController@createAddress');
Route::post('/pendingAddresses','ClientHomeController@pendingAddresses');

// Route::get('/transactionConfirmation/{address}','ClientHomeController@transactionConfirmation');
//checkouts
// Route::get('/checkout/contact','ClientHomeController@checkOutContact');
// Route::post('/checkout/paymentMethod','ClientHomeController@checkOutPayMethod');
Route::post('/checkout/payment','ClientHomeController@checkOutPayment');
Route::get('/checkout/Instant','ClientHomeController@checkOutPaymentInstant');
Route::get('/orderComment/{invoice}','ClientHomeController@orderComment');
Route::post('/orderCommentUpdate','ClientHomeController@orderCommentUpdate');
Route::get('/orderCencel/{invoice}/{slug}/{index}','ClientHomeController@orderCencel');

Route::get('/refundRequest/{invoice}/{slug}/{index}','ClientHomeController@refundRequest');
Route::post('/refundRequestUpdate','ClientHomeController@refundRequestUpdate');

Route::get('/getSortedProducts/{cat_id}/{country_id}/{sorting_id}','ClientHomeController@sortProducts');
Route::get('/matchProducts/{value}','ClientHomeController@matchProducts');
Route::post('/orderSubmit','ClientHomeController@orderSubmit');


//api rate exchange
Route::get('/btcRate','btcRateController@btcRate');


//accounts routes
Route::get('/account','ClientHomeController@account');
Route::get('/addFund','ClientHomeController@addFund');
Route::get('/myproducts','ClientHomeController@myproducts');
Route::get('/support','ClientHomeController@support');
Route::get('/purchase_history','ClientHomeController@purchase_history');


//support
Route::post('/sendMessage','ClientHomeController@sendMessage');


//affiliation
Route::get('/affiliate','ClientHomeController@affiliate');
Route::post('/generateLink','ClientHomeController@generateLink');
Route::get('/registerAff/{code}','Client\Auth\RegisterController@showRegistrationForm');
Route::get('/transferCommission','ClientHomeController@transferCommission');
Route::post('/withdrawCommission','ClientHomeController@withdrawCommission');
Route::get('/withdrawRequests','ClientHomeController@withdrawRequests');
Route::post('/autoSetCommission','ClientHomeController@autoSetCommission');

